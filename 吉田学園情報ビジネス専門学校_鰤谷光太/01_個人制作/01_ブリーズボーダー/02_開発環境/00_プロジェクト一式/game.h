//==================================================
// game.h
// Author: Buriya Kota
//==================================================
#ifndef _GAME_H_
#define _GAME_H_

//**************************************************
// インクルード
//**************************************************
#include "game_mode.h"
#include "model_data.h"
#include "item.h"
#include "bg_model.h"
#include "obstacle.h"

//**************************************************
// 名前付け
//**************************************************
namespace nl = nlohmann;

//**************************************************
// マクロ定義
//**************************************************

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CScore;
class CLockOnUIManager;
class CPlayer3D;
class CPause;
class CMeshField;
class CGimmick;
class CGoal;
class CTimer;
class CObjectX;
class CCoin;
class CItem;
class CBgModel;

//**************************************************
// クラス
//**************************************************
class CGame : public CGameMode
{
public:
	CGame();
	~CGame() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE /*drawMode*/) override {}

	// プレイヤーの情報の取得
	static CGame* GetGame() { return m_pGame; }
	CPlayer3D* GetPlayer3D() { return m_pPlayer3D; }
	CScore* GetScore() { return m_pScore; }
	CPause* GetPause() { return m_pPause; }
	CMeshField* GetMeshField(int num) { return m_pMeshField[num]; }
	CItem* GetItem(int num) { return m_pItem[num]; }
	CGoal* GetGoal() { return m_pGoal; }
	CObstacle* GetObstacle(int num) { return m_pObstacle[num]; }
	CGimmick* GetGimmick() { return m_pGimmick; }
	CTimer* GetTimer() { return m_pTimer; }
	CCoin* GetCoin() { return m_pCoin; }

	// フレームの設定
	int GetFrame() { return m_time; }

	static CGame *Create();

	// 読み込み
	void LoadMesh(const char* pFileName);
	int GetMeshIndex() { return m_nIndex; }

	void LoadModel(const char* pFileName);
	void LoadItem(const char* pFileName);
	void LoadObstacle(const char* pFileName);

private:
	void ParticleSnow_();

private:
	int m_time;		// ゲーム開始からの時間
	static CGame* m_pGame;
	CPlayer3D *m_pPlayer3D;
	CScore *m_pScore;
	CPause *m_pPause;
	std::vector<CMeshField*> m_pMeshField;
	std::vector<CBgModel*> m_pBgModel;
	std::vector<CItem*> m_pItem;
	std::vector<CObstacle*> m_pObstacle;
	CCoin *m_pCoin;
	CGimmick *m_pGimmick;
	CGoal *m_pGoal;
	CTimer *m_pTimer;

	// リストの生成
	nl::json m_JMesh;
	// メッシュの総数
	int m_nIndex;

	// リストの生成
	nl::json m_JModel;
	int m_nNumModel;
	CBgModel::BG_MODEL_TYPE m_nSelectBgModel;

	// リストの生成
	nl::json m_JItem;
	int m_nNumItem;
	CItem::ITEM_TYPE m_nSelectItem;

	// リストの生成
	nl::json m_JObstacle;
	int m_nNumObstacle;
	CObstacle::OBSTACLE_MODEL_TYPE m_nSelectObstacle;
};

#endif	// _GAME_H_