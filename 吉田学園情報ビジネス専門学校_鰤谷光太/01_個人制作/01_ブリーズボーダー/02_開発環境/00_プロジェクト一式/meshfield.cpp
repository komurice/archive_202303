//==================================================
// meshfilde.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "input.h"

#include "game.h"
#include "utility.h"
#include "debug_proc.h"

#include "meshfield.h"
#include "player3D.h"

//**************************************************
// マクロ定義
//**************************************************
#define SIN_SIZE				(-500.0f)
#define SIN_ROT					(3.5f)

#define ACCEL_FACTOR			(9.8f / 4.0f)

//**************************************************
// 定数定義
//**************************************************
// 頂点フォーマット
const DWORD FVF_VERTEX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);		// 座標・法線・カラー・テクスチャ

//**************************************************
// 構造体定義
//**************************************************
// 頂点の情報[3D]の構造体を定義
struct VERTEX_3D
{
	D3DXVECTOR3 pos;	// 頂点座標	
	D3DXVECTOR3 nor;	// 法線ベクトル
	D3DCOLOR col;		// 頂点カラー	
	D3DXVECTOR2 tex;	// テクスチャの座標
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMeshField::CMeshField(int nPriority /* =1 */) : CObject(nPriority)
{
	// タイプ設定
	SetType(TYPE_ROAD);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMeshField::~CMeshField()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMeshField::Init()
{
	{// 初期化
	 // ワールドマトリックス
		D3DXMatrixIdentity(&m_mtxWorld);
		// 頂点バッファへのポインタ
		m_pVtxBuff = nullptr;
		// インデックスバッファへのポインタ
		m_pIdxBuff = nullptr;
		// 位置
		m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 移動量
		m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 回転
		m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// サイズ
		m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 高さ
		m_height = 0.0f;
		// 回転角度
		m_rotationAngle = 0.0f;
		// 目的の回転角度
		m_rotationAngleDest = 0.0f;
		// 姿勢回転軸ベクトル
		m_postureVec = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 目的の姿勢回転軸ベクトル
		m_postureVecDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 一辺のの面数
		m_nMeshOneSideNumber = 0;
		// メッシュの頂点数
		m_nMeshVtx = 0;
		// メッシュのインデックス数
		m_nMeshIdx = 0;
		// メッシュのポリゴン数
		m_nMeshPrimitive = 0;
		// メッシュの大きさX
		m_fMeshSizeX = 0.0f;
		// メッシュの大きさZ
		m_fMeshSizeZ = 0.0f;
	}

	SetTexture(CTexture::TEXTURE_SNOW_GROUND);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMeshField::Uninit()
{
	if (m_pVtxBuff != nullptr)
	{// 頂点バッファの破棄
		m_pVtxBuff->Release();
		m_pVtxBuff = nullptr;
	}

	if (m_pIdxBuff != nullptr)
	{// インデックスバッファの破棄
		m_pIdxBuff->Release();
		m_pIdxBuff = nullptr;
	}

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMeshField::Update()
{
	NorCalculation_();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMeshField::Draw(DRAW_MODE /*drawMode*/)
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();
	CTexture* pTexture = CManager::GetManager()->GetTexture();

	//計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;

	// カリングを両面
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映								↓rotの情報を使って回転行列を作る
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);		//行列掛け算関数		第二引数 * 第三引数 を　第一引数に格納

	// 位置を反映								↓posの情報を使って移動行列を作る
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// インデックスバッファをデータストリームに設定
	pDevice->SetIndices(m_pIdxBuff);

	// 頂点フォーマットの設定VERTEX_3D
	pDevice->SetFVF(FVF_VERTEX_3D);

	// テクスチャの設定
	pDevice->SetTexture(0, pTexture->GetTexture(m_texture));

	// ポリゴンの描画
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_nMeshVtx, 0, m_nMeshPrimitive);

	// テクスチャの解除
	pDevice->SetTexture(0, NULL);

	// カリングを両面
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CMeshField* CMeshField::Create()
{
	CMeshField *pMeshField;
	pMeshField = new CMeshField;

	if (pMeshField != nullptr)
	{
		pMeshField->Init();
	}
	else
	{
		assert(false);
	}

	return pMeshField;
}

//--------------------------------------------------
// 当たり判定
//--------------------------------------------------
bool CMeshField::CollisionMesh(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, float *pAngle, float *pAngleXY, D3DXVECTOR3 *pMeshNor ,float *height)
{
	bool IsLanding = false;

	int primitive = m_nMeshPrimitive;
	VERTEX_3D* pVtx = NULL;			// 頂点情報へのポインタ
	WORD* pIdx;
	const int nTri = 3;

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	// インデックスバッファをロック
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int nCnt = 0; nCnt < primitive; nCnt++)
	{
		D3DXVECTOR3 posPoly[nTri];

		// 頂点座標の取得
		posPoly[0] = pVtx[pIdx[nCnt + 0]].pos;
		posPoly[1] = pVtx[pIdx[nCnt + 1]].pos;
		posPoly[2] = pVtx[pIdx[nCnt + 2]].pos;

		if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
			(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
			(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
		{// 縮退ポリゴンを飛ばす
			continue;
		}

		// ローカルからワールドに変換
		D3DXVec3TransformCoord(&posPoly[0], &posPoly[0], &m_mtxWorld);
		D3DXVec3TransformCoord(&posPoly[1], &posPoly[1], &m_mtxWorld);
		D3DXVec3TransformCoord(&posPoly[2], &posPoly[2], &m_mtxWorld);

		D3DXVECTOR3 vecLine[nTri];

		// 頂点座標の取得
		vecLine[0] = posPoly[1] - posPoly[0];
		vecLine[1] = posPoly[2] - posPoly[1];
		vecLine[2] = posPoly[0] - posPoly[2];

		D3DXVECTOR3 vecPlayer[nTri];

		// 頂点座標の取得
		vecPlayer[0] = *pPos - posPoly[0];
		vecPlayer[1] = *pPos - posPoly[1];
		vecPlayer[2] = *pPos - posPoly[2];

		float InOut[nTri];

		InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
		InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
		InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);

		if ((InOut[0] >= 0 && InOut[1] >= 0 && InOut[2] >= 0)
			|| (InOut[0] <= 0 && InOut[1] <= 0 && InOut[2] <= 0))
		{
			D3DXVECTOR3 V1 = posPoly[1] - posPoly[0];
			D3DXVECTOR3 V2 = posPoly[2] - posPoly[0];

			// 結果の箱
			D3DXVECTOR3 vecNormal;
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V1, &V2);
			// 大きさを１にする
			D3DXVec3Normalize(&vecNormal, &vecNormal);

			if (nCnt % 2 == 1)
			{// 法線ベクトルの向きを正す
				vecNormal *= -1;
			}

			// 上方向のベクトル
			D3DXVECTOR3 upVec = D3DXVECTOR3(0.0f, 1.0f, 0.0f);		

			// 姿勢回転軸ベクトル
			D3DXVec3Cross(&m_postureVecDest, &upVec, &vecNormal);

			// 現在の角度に少しずつ足していく
			m_postureVec += (m_postureVecDest - m_postureVec) * 0.1f;

			// 内積
			float dot = D3DXVec3Dot(&upVec, &vecNormal);

			// ベクトルの大きさを取る
			float upVecSize = D3DXVec3Length(&upVec);
			float vecNormalSize = D3DXVec3Length(&vecNormal);

			// 目的の回転角度の設定
			m_rotationAngleDest = acos(dot / (upVecSize * vecNormalSize));

			// 現在の角度に少しずつ足していく
			m_rotationAngle += (m_rotationAngleDest - m_rotationAngle) * 0.1f;

			// 回転クォータニオンを作成
			D3DXQUATERNION rotationQuaternion(0, 0, 0, 1);
			D3DXQuaternionRotationAxis(&rotationQuaternion, &m_postureVec, m_rotationAngle);

			// プレイヤーにクォータニオンを設定
			CGame::GetGame()->GetPlayer3D()->SetQuaternion(rotationQuaternion);

			// 当たったオブジェクトの位置を設定
			float meshHeight = posPoly[0].y - (vecNormal.x * (pPos->x - posPoly[0].x) + vecNormal.z * (pPos->z - posPoly[0].z)) / vecNormal.y;
			*height = meshHeight;

			if (pPos->y < meshHeight)
			{// メッシュの高さよりプレイヤーの高さのほうが下のとき
				pPos->y = meshHeight;
			}
			else
			{
				return IsLanding;
			}

			// プレイヤーの情報を取得
			//D3DXVECTOR3 moveVec = pPlayer->GetMove();
			//float moveSize = D3DXVec3Length(&moveVec);

			// 進行方向の計算
			D3DXVECTOR3 pos = D3DXVECTOR3(pPos->x, 0.0f, pPos->z);
			D3DXVECTOR3 posOld = D3DXVECTOR3(pPosOld->x, 0.0f, pPosOld->z);

			D3DXVECTOR3 move = pos - posOld;

			float moveLength = D3DXVec3Length(&move);

			// 高さ
			float heightSize = pPos->y - pPosOld->y;
			// メッシュの傾き
			float angle = atan2f(heightSize, moveLength);

			// 角度を渡す
			*pAngle = angle;

			// vtxから横の長さを求める
			float vtxWidth[3];
			vtxWidth[0] = posPoly[0].x - posPoly[1].x;
			vtxWidth[1] = posPoly[1].x - posPoly[2].x;
			vtxWidth[2] = posPoly[2].x - posPoly[0].x;

			// vtxから高さを求める
			float vtxHeight[3];
			vtxHeight[0] = posPoly[0].y - posPoly[1].x;
			vtxHeight[1] = posPoly[1].y - posPoly[2].x;
			vtxHeight[2] = posPoly[2].y - posPoly[0].x;

			float angleXY[3];

			for (int nCntNumAngle = 0; nCntNumAngle < 3; nCntNumAngle++)
			{// 当たっているメッシュのそれぞれの角度を計算
				angleXY[nCntNumAngle] = atan2f(vtxHeight[nCntNumAngle], vtxWidth[nCntNumAngle]);
			}

			float maxAngle = 0.0f;
			for (int nCntAngle = 0; nCntAngle < 3 - 1; nCntAngle++)
			{// メッシュの傾き
				for (int nCntAngle1 = nCntAngle + 1; nCntAngle1 < 3; nCntAngle1++)
				{// 角度の比較（バブルソート）
					if (angleXY[nCntAngle] < angleXY[nCntAngle1])
					{
						maxAngle = angleXY[nCntAngle];
						angleXY[nCntAngle] = angleXY[nCntAngle1];
						angleXY[nCntAngle1] = maxAngle;
					}
				}
			}

			*pAngleXY = angleXY[0];

#ifdef _DEBUG
			// デバッグ表示
			CDebugProc::Print("メッシュの角度 : %f\n\n", angle);
#endif // DEBUG

			if (angle < 0.0f)
			{
				angle *= -1.0f;
			}

			// 加速度
			float accel = sinf(angle);
			accel *= ACCEL_FACTOR;

			// メッシュの法線を渡す
			*pMeshNor = vecNormal;

			CPlayer3D *pPlayer = CGame::GetGame()->GetPlayer3D();

			// プレイヤーの移動量に足す
			pPlayer->AddAccel(accel);

#ifdef _DEBUG
			// デバッグ表示
			CDebugProc::Print("加速度 : %f\n\n", accel);
#endif // DEBUG

			IsLanding = true;
		}
	}

	// インデックスバッファのアンロック
	m_pVtxBuff->Unlock();
	// 頂点バッファをアンロックする
	m_pIdxBuff->Unlock();

	return IsLanding;
}

//--------------------------------------------------
// 影の高さ設定
//--------------------------------------------------
bool CMeshField::CollisionShadow(D3DXVECTOR3 *pPos, float *height)
{
	bool bIsLanding = false;

	int primitive = m_nMeshPrimitive;
	VERTEX_3D* pVtx = NULL;			// 頂点情報へのポインタ
	WORD* pIdx;
	const int nTri = 3;

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	// インデックスバッファをロック
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int nCnt = 0; nCnt < primitive; nCnt++)
	{
		D3DXVECTOR3 posPoly[nTri];

		// 頂点座標の取得
		posPoly[0] = pVtx[pIdx[nCnt + 0]].pos;
		posPoly[1] = pVtx[pIdx[nCnt + 1]].pos;
		posPoly[2] = pVtx[pIdx[nCnt + 2]].pos;

		if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
			(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
			(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
		{// 縮退ポリゴンを飛ばす
			continue;
		}

		// ローカルからワールドに変換
		D3DXVec3TransformCoord(&posPoly[0], &posPoly[0], &m_mtxWorld);
		D3DXVec3TransformCoord(&posPoly[1], &posPoly[1], &m_mtxWorld);
		D3DXVec3TransformCoord(&posPoly[2], &posPoly[2], &m_mtxWorld);

		D3DXVECTOR3 vecLine[nTri];

		// 頂点座標の取得
		vecLine[0] = posPoly[1] - posPoly[0];
		vecLine[1] = posPoly[2] - posPoly[1];
		vecLine[2] = posPoly[0] - posPoly[2];

		D3DXVECTOR3 vecPlayer[nTri];

		// 頂点座標の取得
		vecPlayer[0] = *pPos - posPoly[0];
		vecPlayer[1] = *pPos - posPoly[1];
		vecPlayer[2] = *pPos - posPoly[2];

		float InOut[nTri];

		InOut[0] = Vec2Cross(&vecLine[0], &vecPlayer[0]);
		InOut[1] = Vec2Cross(&vecLine[1], &vecPlayer[1]);
		InOut[2] = Vec2Cross(&vecLine[2], &vecPlayer[2]);

		if ((InOut[0] >= 0 && InOut[1] >= 0 && InOut[2] >= 0)
			|| (InOut[0] <= 0 && InOut[1] <= 0 && InOut[2] <= 0))
		{
			D3DXVECTOR3 V1 = posPoly[1] - posPoly[0];
			D3DXVECTOR3 V2 = posPoly[2] - posPoly[0];

			// 結果の箱
			D3DXVECTOR3 vecNormal;
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V1, &V2);
			// 大きさを１にする
			D3DXVec3Normalize(&vecNormal, &vecNormal);

			if (nCnt % 2 == 1)
			{// 法線ベクトルの向きを正す
				vecNormal *= -1;
			}

			// 当たったオブジェクトの位置を設定
			float meshHeight = posPoly[0].y - (vecNormal.x * (pPos->x - posPoly[0].x) + vecNormal.z * (pPos->z - posPoly[0].z)) / vecNormal.y;
			*height = meshHeight;

			bIsLanding = true;
		}
	}

	// インデックスバッファのアンロック
	m_pVtxBuff->Unlock();
	// 頂点バッファをアンロックする
	m_pIdxBuff->Unlock();

	return bIsLanding;
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CMeshField::Load(const char *pFileName)
{
	// ファイルオープン
	std::ifstream ifs(pFileName);

	if (ifs)
	{// 開けたら
		ifs >> m_JMesh;

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);

		// 位置の読み込み
		m_pos = D3DXVECTOR3(m_JMesh["WORLD_POS"]["POS"]["X"], m_JMesh["WORLD_POS"]["POS"]["Y"], m_JMesh["WORLD_POS"]["POS"]["Z"]);

		// 角度の読み込み
		m_rot = D3DXVECTOR3(m_JMesh["WORLD_ROT"]["ROT"]["X"], m_JMesh["WORLD_ROT"]["ROT"]["Y"], m_JMesh["WORLD_ROT"]["ROT"]["Z"]);

		// 一辺の面数の読み込み
		m_nMeshOneSideNumber = m_JMesh["MESH_ONESIDE_NUMBER"];

		// 面のサイズ X Z
		m_fMeshSizeX = m_JMesh["MESH_SIZEX"];
		m_fMeshSizeZ = m_JMesh["MESH_SIZEZ"];

		MeshVtxAndIdxCalculation_();

		VERTEX_3D* pVtx = NULL;
		// 頂点座標をロック
		m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

		for (int nCnt = 0; nCnt < m_nMeshVtx; nCnt++)
		{
			// JSONファイルタグ設定
			std::string name = "VTX";
			std::string Number = std::to_string(nCnt);
			name += Number;

			// 位置の読み込み
			pos = D3DXVECTOR3(m_JMesh[name]["POS"]["X"], m_JMesh[name]["POS"]["Y"], m_JMesh[name]["POS"]["Z"]);

			// 座標の設定
			pVtx[nCnt].pos = D3DXVECTOR3(pos.x, pos.y, pos.z);

			// 各頂点の法線の設定(※ベクトルの大きさは1にする必要がある)
			pVtx[nCnt].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			// 頂点カラーの設定
			pVtx[nCnt].col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
		}

		// 頂点座標をアンロック
		m_pVtxBuff->Unlock();
	}

	//計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映								↓rotの情報を使って回転行列を作る
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);		//行列掛け算関数		第二引数 * 第三引数 を　第一引数に格納

	// 位置を反映								↓posの情報を使って移動行列を作る
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// 法線の計算
	NorCalculation_();
}

//--------------------------------------------------
// メッシュの頂点とインデックスの計算
//--------------------------------------------------
void CMeshField::MeshVtxAndIdxCalculation_()
{
	// メッシュの頂点数
	m_nMeshVtx = (((m_nMeshOneSideNumber)+1) * ((m_nMeshOneSideNumber)+1));
	// メッシュのインデックス数
	m_nMeshIdx = ((m_nMeshOneSideNumber + 1) * 2 * m_nMeshOneSideNumber + (m_nMeshOneSideNumber - 1) * 2);
	// メッシュのポリゴン数 
	m_nMeshPrimitive = ((m_nMeshOneSideNumber) * (m_nMeshOneSideNumber) * 2 + 4 * ((m_nMeshOneSideNumber)-1));

	// デバイスのポインタ
	LPDIRECT3DDEVICE9 pDevice = CManager::GetManager()->GetRenderer()->GetDevice();

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * m_nMeshVtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	//インデックスバッファの生成
	pDevice->CreateIndexBuffer(sizeof(WORD) * m_nMeshIdx,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&m_pIdxBuff,
		NULL);

	// 頂点情報へのポインタ
	VERTEX_3D* pVtx = NULL;

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int z = 0; z < (m_nMeshOneSideNumber + 1); z++)
	{
		for (int x = 0; x < (m_nMeshOneSideNumber + 1); x++)
		{
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].pos = D3DXVECTOR3(x * m_fMeshSizeX, 0.0f, -z * m_fMeshSizeZ);
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].pos.x -= m_nMeshOneSideNumber * m_fMeshSizeX * 0.5f;
			//pVtx[x + z * (m_nMeshX + 1)].pos.y = sinf(x * D3DXToRadian(SIN_ROT) + m_height) * SIN_SIZE;
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].pos.z += m_nMeshOneSideNumber * m_fMeshSizeZ * 0.5f;
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].col = D3DXCOLOR(1.0f, 1.0f, 0.7f, 1.0f);
			pVtx[x + z * (m_nMeshOneSideNumber + 1)].tex = D3DXVECTOR2((float)10.0f / m_nMeshOneSideNumber * x, (float)10.0f / m_nMeshOneSideNumber * z);
		}
	}

	WORD* pIdx;

	// インデックスバッファをロック
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int z = 0; z < m_nMeshOneSideNumber; z++)
	{// zの方向
		int nLineTop = z * ((m_nMeshOneSideNumber + 1) * 2 + 2);
		for (int x = 0; x < m_nMeshOneSideNumber + 1; x++)
		{// xの方向
		 // 偶数番目
			pIdx[(x * 2) + nLineTop] = (WORD)((m_nMeshOneSideNumber + 1) + x + z * (m_nMeshOneSideNumber + 1));
			// 奇数番目
			pIdx[(x * 2 + 1) + nLineTop] = (WORD)(pIdx[(x * 2) + nLineTop] - (m_nMeshOneSideNumber + 1));
		}
		// 縮退ポリゴン(数が連続する時)
		if (z < m_nMeshOneSideNumber - 1)
		{
			pIdx[(m_nMeshOneSideNumber + 1) * 2 + nLineTop] = (WORD)(m_nMeshOneSideNumber + (m_nMeshOneSideNumber + 1) * z);
			pIdx[(m_nMeshOneSideNumber + 1) * 2 + 1 + nLineTop] = (WORD)((m_nMeshOneSideNumber + 1) * (2 + z));
		}
	}

	// 頂点バッファをアンロックする
	m_pVtxBuff->Unlock();
	// インデックスバッファのアンロック
	m_pIdxBuff->Unlock();
}

//--------------------------------------------------
// 法線の計算
//--------------------------------------------------
void CMeshField::NorCalculation_()
{
	// 三角形の頂点数
	const int nTri = 3;
	// 名前思いつかない
	D3DXVECTOR3 posCorner[nTri];
	// primitiveを保存
	int primitive = m_nMeshPrimitive;

	// 頂点情報へのポインタ
	VERTEX_3D* pVtx = NULL;
	WORD* pIdx;

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	// インデックスバッファをロック
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int i = 0; i < m_nMeshVtx; i++)
	{// 頂点数分初期化
		pVtx[i].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}
	for (int nCnt = 0; nCnt < primitive; nCnt++)
	{// 各頂点の座標を取得
		posCorner[0] = pVtx[pIdx[nCnt + 0]].pos;
		posCorner[1] = pVtx[pIdx[nCnt + 1]].pos;
		posCorner[2] = pVtx[pIdx[nCnt + 2]].pos;

		if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
			(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
			(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
		{// 縮退ポリゴンをとばす
			continue;
		}

		// ポリドンから二つのベクトルを取る
		D3DXVECTOR3 V1 = posCorner[1] - posCorner[0];
		D3DXVECTOR3 V2 = posCorner[2] - posCorner[0];

		// 求めた法線を格納する箱
		D3DXVECTOR3 vecNormal;

		if (nCnt % 2 == 0)
		{
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V1, &V2);
		}
		else
		{
			// メッシュの法線を求める
			D3DXVec3Cross(&vecNormal, &V2, &V1);
		}

		// 大きさを１にする
		D3DXVec3Normalize(&vecNormal, &vecNormal);

		for (int i = 0; i < nTri; i++)
		{// 求めた法線を各頂点に設定
			pVtx[pIdx[nCnt + i]].nor += vecNormal;
		}
	}

	for (int nCnt = 0; nCnt < m_nMeshVtx; nCnt++)
	{// すべての法線を正規化する
		D3DXVec3Normalize(&pVtx[nCnt].nor, &pVtx[nCnt].nor);
	}

	// 頂点バッファをアンロックする
	m_pVtxBuff->Unlock();
	// インデックスバッファのアンロック
	m_pIdxBuff->Unlock();
}
