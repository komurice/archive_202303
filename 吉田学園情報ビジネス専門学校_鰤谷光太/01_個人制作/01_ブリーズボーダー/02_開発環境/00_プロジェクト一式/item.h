//==================================================
// item.h
// Author: Buriya Kota
//==================================================
#ifndef _ITEM_H_
#define _ITEM_H_

//**************************************************
// インクルード
//**************************************************
#include "gimmick.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CItem : public CGimmick
{
public:
	// 画面(モード)の種類
	enum ITEM_TYPE
	{
		ITEM_TYPE_COIN = 0,				// コイン
		ITEM_TYPE_MAX,					// 最大数
		ITEM_TYPE_NONE,					// 使用しない
	};

	explicit CItem(int nPriority = PRIORITY_OBJECT);
	~CItem();

	HRESULT Init() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	ITEM_TYPE GetItemType() { return m_itemType; }
	void SetItemType(ITEM_TYPE type) { m_itemType = type; }

	static CItem *Create(ITEM_TYPE type);

private:
	static ITEM_TYPE m_itemType;
};

#endif	// _ITEM_H_