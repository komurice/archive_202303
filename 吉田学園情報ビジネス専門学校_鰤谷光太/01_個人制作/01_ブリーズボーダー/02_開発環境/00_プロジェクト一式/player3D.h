//==================================================
// player3D.h
// Author: Buriya Kota
//==================================================
#ifndef _PLAYER3D_H_
#define _PLAYER3D_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CShadow;
class CModel;

//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(9)
#define MAX_KEY			(2)
#define MAX_MOTION		(1)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CPlayer3D : public CObject
{
public:
	struct KEY
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 rot;
	};

	struct KEY_SET
	{// std::vector map を勉強したら動的に配列の値を変えれる
		int nFrame;
		KEY aKey[MAX_PARTS];
	};

	//modelデータの構造体//
	struct MODELDATA
	{
		int  LOOP;		// ループするかどうか[0:ループしない / 1 : ループする]
		int NUM_KEY;  	// キー数
		KEY_SET KeySet[MAX_KEY];
	};

public:
	static const float PLAYER_SPEED;

public:
	explicit CPlayer3D(int nPriority = PRIORITY_PLAYER);
	~CPlayer3D();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(DRAW_MODE drawMode) override;

	static CPlayer3D *Create(D3DXVECTOR3 pos);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; }
	void SetPosOld(const D3DXVECTOR3& posOld) { m_posOld = posOld; }
	void AddAccel(const float& accel);
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtx = mtx; }
	void SetQuaternion(D3DXQUATERNION quaternion) { m_myQuaternion = quaternion; }

	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetPosOld() const { return m_posOld; }
	const D3DXVECTOR3& GetMove() const { return m_move; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXVECTOR3& GetSize() const { return m_size; }

	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_pos += move; }

private:
	void LoadSetFile(char *Filename);
	void LoadKeySetFile(FILE *pFile);
	void Control_();
	void Motion_();
	void CollisionItem_();
	bool CollisionObstacle_();
	void CollisionMesh_();
	void CollisionGoal_();
private:
	// モデルの情報
	CModel *m_pModel[MAX_PARTS];
	D3DXMATRIX m_mtx;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_move;
	D3DXVECTOR3 m_size;
	D3DXVECTOR3 m_rot;
	D3DXVECTOR3 m_rotDest;
	CShadow *m_pShadow;
	// キーの総数
	int m_nNumKey;
	// 現在のキー
	int m_nCurrentKey;
	// モーションカウンター
	int m_nCountMotion;
	//
	MODELDATA m_ModelData[MAX_MOTION];
	//
	int m_nSetModel;
	int m_nSetCurrentMotion;
	// クォータニオン
	D3DXQUATERNION m_myQuaternion;
	// 加速度
	float m_accel;
	// ゴールした時のカウント
	int m_nGoalTimer;
	// ゴールしたかどうか
	bool m_bIslandingGoal;
	// メッシュに当たっているかどうか
	bool m_bIsMesh;
	// メッシュの角度保存
	float m_fMeahAngle;
	// メッシュの最大角度保存
	float m_fMeshMaxAngle;
	// プレイヤーが障害物にぶつかっているとき
	bool m_bIsHitObstacle;
	// 障害物に当たった時の状態を戻すときのフレーム
	int m_nStatusRecoveryTime;
	// メッシュの高さ
	float m_meshHeigh;
};

#endif	// _PLAYER3D_H_