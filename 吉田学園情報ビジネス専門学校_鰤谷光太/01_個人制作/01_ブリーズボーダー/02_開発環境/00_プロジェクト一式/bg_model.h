//==================================================
// background_model.h
// Author: Buriya Kota
//==================================================
#ifndef _BACKGROUND_MODEL_H_
#define _BACKGROUND_MODEL_H_

//**************************************************
// インクルード
//**************************************************
#include "gimmick.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CBgModel : public CGimmick
{
public:
	// 画面(モード)の種類
	enum BG_MODEL_TYPE
	{
		BG_MODEL_TYPE_TREE = 0,				// 木
		BG_MODEL_TYPE_MAX,					// 最大数
		BG_MODEL_TYPE_NONE,					// 使用しない
	};

	explicit CBgModel(int nPriority = PRIORITY_OBJECT);
	~CBgModel();

	HRESULT Init() override;
	void Update() override;

	static CBgModel *Create(BG_MODEL_TYPE type);

	BG_MODEL_TYPE GetBgModelType() { return m_bgModelType; }
	void SetBgModelType(BG_MODEL_TYPE type) { m_bgModelType = type; }

private:
	static BG_MODEL_TYPE m_bgModelType;
};

#endif	// _BACKGROUND_MODEL_H_