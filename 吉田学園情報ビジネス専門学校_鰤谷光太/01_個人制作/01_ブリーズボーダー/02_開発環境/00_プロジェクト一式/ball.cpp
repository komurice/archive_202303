//==================================================
// ball.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "debug_proc.h"

#include "game.h"
#include "manager.h"
#include "input.h"

#include "ball.h"
#include "meshfield.h"
#include "model_data.h"

//**************************************************
// マクロ定義
//**************************************************
#define SPEED	(15.0f)

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBall::CBall(int nPriority) : CObjectX(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBall::~CBall()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CBall::Init()
{
	CObjectX::Init();

	// モデルの読み込み
	SetModelData(CModelData::MODEL_BALL);
	SetQuat(true);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBall::Update()
{
	D3DXVECTOR3 posOld = GetPos();
	SetPosOld(posOld);

	Control_();

	CModelData* modelData = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = modelData->GetModel(GetModelData()).size;
	D3DXVECTOR3 rot = GetRot();

	float angle = 0.0f;
	float maxAngle = 0.0f;
	D3DXVECTOR3 meshNor;
	float height;

	int maxMesh = CGame::GetGame()->GetMeshIndex();

	for (int nCnt = 0; nCnt < maxMesh; nCnt++)
	{// メッシュとの当たり判定
		CMeshField *pMeshField = CGame::GetGame()->GetMeshField(nCnt);
		// 今いるメッシュの高さより下にいる場合
		pMeshField->CMeshField::CollisionMesh(&pos, &posOld, &angle, &maxAngle, &meshNor, &height);
	}

	pos.y += size.y * 0.5f;

	SetPos(pos);

#ifdef _DEBUG
	// デバッグ表示
	CDebugProc::Print("ボールの現在の位置 : \n x : %f , y : %f , z : %f \n", pos.x, pos.y, pos.z);
	CDebugProc::Print("ボールの現在の角度 : \n x : %f , y : %f , z : %f \n\n", rot.x, rot.y, rot.z);
#endif // DEBUG
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBall *CBall::Create(D3DXVECTOR3 pos)
{
	CBall *pBall;
	pBall = new CBall;

	if (pBall != nullptr)
	{
		pBall->Init();
		pBall->SetPos(pos);
	}
	else
	{
		assert(false);
	}

	return pBall;
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CBall::Control_()
{
	// インプット
	CInput *pInput = CInput::GetKey();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	// ジョイパッドの入力
	vec.x = pInput->VectorMoveJoyStick().x;
	vec.z = -pInput->VectorMoveJoyStick().y;

	D3DXVECTOR3 rot = GetRot();

	if ((vec.x == 0.0f) && (vec.z == 0.0f))
	{
		if (pInput->Press(DIK_LEFT))
		{// 左
			vec.x += -1.0f;
		}
		if (pInput->Press(DIK_RIGHT))
		{// 右
			vec.x += 1.0f;
		}
		if (pInput->Press(DIK_UP))
		{// 上
			vec.z += 1.0f;
		}
		if (pInput->Press(DIK_DOWN))
		{// 下
			vec.z += -1.0f;
		}
	}

	CModelData* modelData = CManager::GetManager()->GetModelData();

	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = modelData->GetModel(GetModelData()).size;
	D3DXVECTOR3 rotate;

	//if (vec.x != 0.0f || vec.z != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		D3DXVECTOR3 move;

		// 移動量
		move = vec * SPEED;
		pos += move;

		// 移動量に対しての回転量を求める（z軸）
		rotate.z = vec.x * SPEED / size.x;
		rot.z += -rotate.z;

		// 移動量に対しての回転量を求める（x軸）
		rotate.x = vec.z * SPEED / size.z;
		rot.x += rotate.x;

		rot = RotNormalization(rot);

		// 回転軸
		D3DXVECTOR3 vecAxis(0.0f, 0.0f, 0.0f);
		// 回転角
		float fValueRot = 0.0f;

		// 回転軸の設定
		vecAxis = D3DXVECTOR3(move.z, 0.0f, -move.x);
		// 回転角の設定
		float length = D3DXVec3Length(&move);
		fValueRot = length / size.z;

		SetPos(pos);
		SetRot(rot);
		SetVecAxis(vecAxis);
		SetValueRot(fValueRot);
	}
}
