//==================================================
// ball.h
// Author: Buriya Kota
//==================================================
#ifndef _BALL_H_
#define _BALL_H_

//**************************************************
// インクルード
//**************************************************
#include "objectX.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CShadow;
class CModel;

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CBall : public CObjectX
{
public:
	explicit CBall(int nPriority = PRIORITY_PLAYER);
	~CBall();

	HRESULT Init() override;
	void Update() override;

	static CBall *Create(D3DXVECTOR3 pos);

private:
	void Control_();

private:

};

#endif	// _BALL_H_