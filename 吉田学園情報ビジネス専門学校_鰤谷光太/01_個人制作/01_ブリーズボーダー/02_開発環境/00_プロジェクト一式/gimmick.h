//==================================================
// gimmick.h
// Author: Buriya Kota
//==================================================
#ifndef _GIMMICK_H_
#define _GIMMICK_H_

//**************************************************
// インクルード
//**************************************************
#include "objectX.h"
#include "model.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CGimmick : public CObjectX
{
public:
	enum MODEL_TYPE
	{
		MODEL_TYPE_NONE = -1,
		MODEL_TYPE_GOAL,
		MODEL_TYPE_MAX
	};

	explicit CGimmick(int nPriority = PRIORITY_OBJECT);
	~CGimmick();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override {}
	void Draw(DRAW_MODE drawMode) override;

	static CGimmick *Create();

	void SetStencil(bool isStencil) { m_isStencil = isStencil; }

	bool CollisionGimmick(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize);

private:
	bool m_isStencil;
};

#endif	// _GIMMICK_H_