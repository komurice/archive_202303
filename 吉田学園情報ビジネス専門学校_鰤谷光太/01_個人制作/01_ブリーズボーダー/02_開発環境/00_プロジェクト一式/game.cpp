//==================================================
// game.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "camera.h"
#include "input.h"
#include "sound.h"
#include "pause.h"
#include "game.h"
#include "ranking.h"
#include "utility.h"

#include "fade.h"
#include "score.h"
#include "timer.h"
#include "silhouette.h"
#include "object3D.h"

#include "model.h"
#include "player3D.h"
#include "ball.h"
#include "meshfield.h"
#include "gimmick.h"
#include "goal.h"
#include "item.h"
#include "item_coin.h"
#include "mesh_sky.h"
#include "bg_model.h"
#include "particle.h"

//**************************************************
// マクロ定義
//**************************************************
#define MAX_GOAL	(2)

//**************************************************
// 静的メンバ変数
//**************************************************
CGame *CGame::m_pGame = nullptr;

//**************************************************
// マクロ定義
//**************************************************

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGame::CGame()
{
	m_pPlayer3D = nullptr;
	m_pScore = nullptr;
	m_pPause = nullptr;
	m_pCoin = nullptr;
	m_pGimmick = nullptr;
	m_pGoal = nullptr;
	m_pTimer = nullptr;

	m_nIndex = 0;
	m_nNumModel = 0;
	m_nNumItem = 0;
	m_nNumObstacle = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGame::~CGame()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGame::Init()
{
	CManager::GetManager()->GetSound()->Play(CSound::LABEL_BGM_GAME);

	CManager::GetManager()->GetCamera()->SetPosV(D3DXVECTOR3(0.0f, 200.0f, -400.0f));
	CManager::GetManager()->GetCamera()->SetPosR(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_time = 0;
	m_nIndex = 0;
	m_nNumModel = 0;
	m_nNumItem = 0;
	m_nNumObstacle = 0;

	m_pTimer = CTimer::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, 50.0f, 0.0f), D3DXVECTOR3(50.0f, 100.0f, 0.0f));
	m_pTimer->SetTimer(100);

	m_pScore = CScore::Create(D3DXVECTOR3(60.0f, 50.0f, 0.0f), D3DXVECTOR3(40.0f, 100.0f, 0.0f));
	m_pScore->SetScore(0);

	m_pPause = CPause::Create();

	CSilhoette::Create(
		D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),
		D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));

	m_pPlayer3D = CPlayer3D::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f));

	m_pGoal = CGoal::Create();
	m_pGoal->SetPos(D3DXVECTOR3(-5700.0f, -13150.0f, 69544.0f));

	CObject3D *obj = CObject3D::Create(D3DXVECTOR3(0.0f, -14150.0f, 10000.0f), D3DXVECTOR3(100000.0f, 0.0f, 100000.0f));
	obj->SetTexture(CTexture::TEXTURE_SNOW_GROUND);

	CMeshSky::Create();

	// メッシュの読み込み
	LoadMesh("data/TEXT/stage.json");
	// モデルの読み込み
	LoadModel("data/TEXT/model.json");
	// アイテムの読み込み
	LoadItem("data/TEXT/item.json");
	// 障害物の読み込み
	LoadObstacle("data/TEXT/obstacle.json");

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGame::Uninit()
{
	// スコアの設定
	CManager::SetNowScore(m_pScore->GetScore());

	m_pPlayer3D = nullptr;
	m_pTimer = nullptr;
	m_pPause = nullptr;
	m_pGoal = nullptr;

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGame::Update()
{
	ParticleSnow_();

	m_time++;
	bool isGoal = m_pGoal->IsGoal();
	if (isGoal)
	{
		m_pTimer->StopTimer(true);
	}

#ifdef _DEBUG
	CInput *pInput = CInput::GetKey();

	if (pInput->Trigger(DIK_RETURN) || pInput->Trigger(JOYPAD_B, 0))
	{
		// 遷移
		CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
	}
#endif // _DEBUG
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGame* CGame::Create()
{
	m_pGame = new CGame;

	if (m_pGame != nullptr)
	{
		m_pGame->Init();
	}
	else
	{
		assert(false);
	}

	return m_pGame;
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadMesh(const char* pFileName)
{
	m_pMeshField.clear();

	// ファイルオープン
	std::ifstream ifs(pFileName);

	if (ifs)
	{// 開けたら
		ifs >> m_JMesh;

		m_nIndex = m_JMesh["INDEX"];

		for (int nCnt = 0; nCnt < m_nIndex; nCnt++)
		{
			std::string name = "MESH_PATH";
			// 数型を文字型に変える
			std::string Number = std::to_string(nCnt);
			name += Number;

			std::string path = m_JMesh[name];

			CMeshField *pMesh = CMeshField::Create();

			m_pMeshField.push_back(pMesh);
			pMesh->Load(path.c_str());
		}
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadModel(const char* pFileName)
{
	m_pBgModel.clear();

	// ファイルオープン
	std::ifstream ifs(pFileName);

	if (ifs)
	{// 開けたら
		ifs >> m_JModel;

		m_nNumModel = m_JModel["INDEX"];

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);

		for (int nCnt = 0; nCnt < m_nNumModel; nCnt++)
		{
			std::string name = "BG_MODEL";
			// 数型を文字型に変える
			std::string Number = std::to_string(nCnt);
			name += Number;

			// 位置の読み込み
			pos = D3DXVECTOR3(m_JModel[name]["POS"]["X"], m_JModel[name]["POS"]["Y"], m_JModel[name]["POS"]["Z"]);

			// 角度の読み込み
			rot = D3DXVECTOR3(m_JModel[name]["ROT"]["X"], m_JModel[name]["ROT"]["Y"], m_JModel[name]["ROT"]["Z"]);

			m_nSelectBgModel = m_JModel[name]["SELECT_BG_MODEL"];

			CBgModel* pBgModel = CBgModel::Create((CBgModel::BG_MODEL_TYPE)m_nSelectBgModel);

			m_pBgModel.push_back(pBgModel);
			pBgModel->SetPos(D3DXVECTOR3(pos));
			pBgModel->SetRot(D3DXVECTOR3(rot));
		}
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadItem(const char* pFileName)
{
	m_pItem.clear();

	// ファイルオープン
	std::ifstream ifs(pFileName);

	if (ifs)
	{// 開けたら
		ifs >> m_JItem;

		m_nNumItem = m_JItem["INDEX"];

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);

		for (int nCnt = 0; nCnt < m_nNumItem; nCnt++)
		{
			std::string name = "ITEM";
			// 数型を文字型に変える
			std::string Number = std::to_string(nCnt);
			name += Number;

			// 位置の読み込み
			pos = D3DXVECTOR3(m_JItem[name]["POS"]["X"], m_JItem[name]["POS"]["Y"], m_JItem[name]["POS"]["Z"]);

			// 角度の読み込み
			rot = D3DXVECTOR3(m_JItem[name]["ROT"]["X"], m_JItem[name]["ROT"]["Y"], m_JItem[name]["ROT"]["Z"]);

			m_nSelectItem = m_JItem[name]["SELECT_ITEM"];

			CItem* pItem = CItem::Create((CItem::ITEM_TYPE)m_nSelectItem);

			m_pItem.push_back(pItem);
			pItem->SetPos(D3DXVECTOR3(pos));
			pItem->SetRot(D3DXVECTOR3(rot));
		}
	}
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CGame::LoadObstacle(const char * pFileName)
{
	m_pObstacle.clear();

	// ファイルオープン
	std::ifstream ifs(pFileName);

	if (ifs)
	{// 開けたら
		ifs >> m_JObstacle;

		m_nNumObstacle = m_JObstacle["INDEX"];

		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);

		for (int nCnt = 0; nCnt < m_nNumObstacle; nCnt++)
		{
			std::string name = "OBSTACLE";
			// 数型を文字型に変える
			std::string Number = std::to_string(nCnt);
			name += Number;

			// 位置の読み込み
			pos = D3DXVECTOR3(m_JObstacle[name]["POS"]["X"], m_JObstacle[name]["POS"]["Y"], m_JObstacle[name]["POS"]["Z"]);

			// 角度の読み込み
			rot = D3DXVECTOR3(m_JObstacle[name]["ROT"]["X"], m_JObstacle[name]["ROT"]["Y"], m_JObstacle[name]["ROT"]["Z"]);

			m_nSelectObstacle = m_JObstacle[name]["SELECT_OBSTACLE"];

			CObstacle* pObstacle = CObstacle::Create((CObstacle::OBSTACLE_MODEL_TYPE)m_nSelectObstacle);

			m_pObstacle.push_back(pObstacle);
			pObstacle->SetPos(D3DXVECTOR3(pos));
			pObstacle->SetRot(D3DXVECTOR3(rot));
		}
	}

}

//--------------------------------------------------
// ゲーム中の雪
//--------------------------------------------------
void CGame::ParticleSnow_()
{
	D3DXVECTOR3 playerPos = m_pPlayer3D->GetPos();
	for (int i = 0; i < 5; i++)
	{
		CParticle* particle = CParticle::Create(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(10.0f, 10.0f, 0.0f), 1000);
		particle->SetPos(D3DXVECTOR3(
			FloatRandam(playerPos.x + 500.0f, playerPos.x - 500.0f),
			FloatRandam(playerPos.y + 300.0f,playerPos.y - 300.0f), 
			FloatRandam(playerPos.z + 1000.0f, playerPos.y + 800.0f)));
		particle->SetMovePos(D3DXVECTOR3(FloatRandam(5.0f, -5.0f), FloatRandam(3.0f, 0.0f), FloatRandam(2.0f, -1.0f)));
		particle->SetMoveSize(D3DXVECTOR3(-0.35f, -0.35f, 0.0f));
		particle->SetMoveRot(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
		particle->SetTexture(CTexture::TEXTURE_SNOW);
	}
}
