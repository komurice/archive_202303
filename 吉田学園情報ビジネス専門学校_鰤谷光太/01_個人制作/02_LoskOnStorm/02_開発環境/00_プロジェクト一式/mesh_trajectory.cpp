//==================================================
// mesh_trajectory.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "mesh_trajectory.h"

#include <crtdbg.h>
#ifdef _DEBUG
//#define DEBUG_PRINT(...) _RPT_BASE(_CRT_WARN, __FILE__, __LINE__, NULL, __VA_ARGS__)
#define DEBUG_PRINT(...) _RPT_BASE(_CRT_WARN, NULL, 0, NULL, __VA_ARGS__)
#else
#define DEBUG_PRINT(...) ((void)0)
#endif

//**************************************************
// マクロ定義
//**************************************************
#define MESH_VTX				(2 * (60 * 10))		// １フレームあたりの頂点数 * （一秒間のフレーム * 秒数）

//**************************************************
// 定数定義
//**************************************************
// 頂点フォーマット
const DWORD FVF_VERTEX_3D = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);		// 座標・法線・カラー・テクスチャ

//**************************************************
// 構造体定義
//**************************************************
// 頂点の情報[3D]の構造体を定義
struct VERTEX_3D
{
	D3DXVECTOR3 pos;	// 頂点座標	
	D3DXVECTOR3 nor;	// 法線ベクトル
	D3DCOLOR col;		// 頂点カラー	
	D3DXVECTOR2 tex;	// テクスチャの座標
};

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CMeshTrajectory::CMeshTrajectory(int nPriority /* =2 */) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CMeshTrajectory::~CMeshTrajectory()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CMeshTrajectory::Init()
{
	// 現在の頂点数
	m_currentVtx = 0;

	// デバイスのポインタ
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * MESH_VTX,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	VERTEX_3D* pVtx = nullptr;			// 頂点情報へのポインタ

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	for (int nCnt = 0; nCnt < MESH_VTX; nCnt++)
	{
		pVtx[nCnt].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		pVtx[nCnt].nor = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
		pVtx[nCnt].col = D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f);
	}

	// 頂点バッファをアンロックする
	m_pVtxBuff->Unlock();

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CMeshTrajectory::Uninit()
{
	if (m_pVtxBuff != nullptr)
	{// 頂点バッファの破棄
		m_pVtxBuff->Release();
		m_pVtxBuff = nullptr;
	}

	Release();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CMeshTrajectory::Update()
{
	
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CMeshTrajectory::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	D3DXMATRIX mtxRot, mtxTrans;								//計算用マトリックス

	//ライトを無効にする
	pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映								↓rotの情報を使って回転行列を作る
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);		//行列掛け算関数		第二引数 * 第三引数 を　第一引数に格納

	// 位置を反映								↓posの情報を使って移動行列を作る
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	// 頂点フォーマットの設定VERTEX_3D
	pDevice->SetFVF(FVF_VERTEX_3D);

	// ポリゴンの描画
	pDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, m_currentVtx - 2);

	//ライトを無効にする
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
}

//--------------------------------------------------
// 頂点を足していく
//--------------------------------------------------
void CMeshTrajectory::AddVtx(const D3DXVECTOR3& pos, const D3DXVECTOR3& move)
{
	// 進行方向の右側
	D3DXVECTOR3 vec = D3DXVECTOR3(move.z, 0.0f, -move.x);

	// 単位ベクトルにする
	D3DXVec3Normalize(&vec, &vec);

	// 弾の位置からの距離をきめる
	vec *= 5.0f;

	VERTEX_3D* pVtx = nullptr;			// 頂点情報へのポインタ

	// 頂点バッファをロックし、頂点情報へのポインタを取得
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	pVtx[m_currentVtx + 0].pos = pos + vec;
	pVtx[m_currentVtx + 1].pos = pos - vec;

	// 頂点バッファをアンロックする
	m_pVtxBuff->Unlock();

	//DEBUG_PRINT("%p 0 x: %f, y: %f, z: %f\n", this, pVtx[m_currentVtx + 0].pos.x, pVtx[m_currentVtx + 0].pos.y, pVtx[m_currentVtx + 0].pos.z);
	//DEBUG_PRINT("%p 1 x: %f, y: %f, z: %f\n", this, pVtx[m_currentVtx + 1].pos.x, pVtx[m_currentVtx + 1].pos.y, pVtx[m_currentVtx + 1].pos.z);

	// 頂点を増やす
	m_currentVtx += 2;
}
