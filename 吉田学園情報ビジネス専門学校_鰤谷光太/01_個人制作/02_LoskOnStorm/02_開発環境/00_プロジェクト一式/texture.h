//**************************************************
// texture.h
// Author  : katsuki mizuki
//**************************************************
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

//==================================================
// インクルード
//==================================================
#include <d3dx9.h>

//==================================================
// 定義
//==================================================
class CTexture
{
public: /* 定義 */
	enum TEXTURE
	{
		TEXTURE_PLAYER = 0,			// プレイヤー
		TEXTURE_ENEMY,				// 敵
		TEXTURE_BULLET,				// 弾
		TEXTURE_EXPLOSION,			// 爆発
		TEXTURE_ROAD,				// 道
		TEXTURE_LOCK_ON_CURSOR,		// ロックオンカーソル
		TEXTURE_LOCK_ON,			// ロックオン
		TEXTURE_NUMBER,				// 数
		TEXTURE_TITLE,				// タイトル
		TEXTURE_BG_TITLE,			// BGタイトル
		TEXTURE_START,				// スタート
		TEXTURE_TUTORIAL,			// チュートリアル
		TEXTURE_RESULT,				// リザルト
		TEXTURE_LIFE,				// ライフ
		TEXTURE_RANKING,			// 順位
		TEXTURE_RANKING_BG,			// ランキングの背景
		TEXTURE_TUTORIAL_CONTROLER,	// ランキングの背景
		TEXTURE_ENEMY_BULLET,		// エネミーの弾
		TEXTURE_PAUSE_RESTART,		// 初めから
		TEXTURE_PAUSE_TITLE,		// タイトルへ
		TEXTURE_PAUSE_CLOSE,		// 閉じる
		TEXTURE_PAUSE,				// ポーズ
		TEXTURE_MAX,
		TEXTURE_NONE,				// 使用しない
	};

	static const char* s_FileName[];	// ファイルパス

public:
	CTexture();		// デフォルトコンストラクタ
	~CTexture();	// デストラクタ

public: /* メンバ関数 */
	void LoadAll();										// 全ての読み込み
	void Load(TEXTURE inTexture);						// 指定の読み込み
	void ReleaseAll();									// 全ての破棄
	void Release(TEXTURE inTexture);					// 指定の破棄
	LPDIRECT3DTEXTURE9 GetTexture(TEXTURE inTexture);	// 情報の取得

private: /* メンバ変数 */
	LPDIRECT3DTEXTURE9 s_pTexture[TEXTURE_MAX];	// テクスチャの情報
};

#endif // !_TEXTURE_H_
