//==================================================
// lock_on_target.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "lock_on_target.h"
#include "enemy3D.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLockOnTarget::CLockOnTarget(int nPriority /* PRIORITY_4 */) : CPolygon3D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLockOnTarget::~CLockOnTarget()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLockOnTarget::Init()
{
	// 初期化
	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_LOCK_ON);

	return E_NOTIMPL;
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CLockOnTarget::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_ALWAYS);

	CPolygon3D::Draw();

	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLockOnTarget* CLockOnTarget::Create(const D3DXVECTOR3 size)
{
	CLockOnTarget *pLockOnTarget;
	pLockOnTarget = new CLockOnTarget;

	if (pLockOnTarget != nullptr)
	{
		pLockOnTarget->Init();
		pLockOnTarget->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pLockOnTarget;
}
