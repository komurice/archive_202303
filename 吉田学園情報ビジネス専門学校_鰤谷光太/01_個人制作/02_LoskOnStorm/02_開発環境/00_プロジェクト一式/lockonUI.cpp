//==================================================
// lockonUI.h
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "lockonUI.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLockOnUI::CLockOnUI(int nPriority /* =4 */) : CObject2D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLockOnUI::~CLockOnUI()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLockOnUI::Init()
{
	CObject2D::Init();

	return S_OK;
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLockOnUI *CLockOnUI::Create(const D3DXVECTOR3 pos, const D3DXVECTOR3 size)
{
	CLockOnUI *pLockOnUI;
	pLockOnUI = new CLockOnUI;

	if (pLockOnUI != nullptr)
	{
		pLockOnUI->Init();
		pLockOnUI->SetPos(pos);
		pLockOnUI->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pLockOnUI;
}
