//==================================================
// back_model.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "back_model.h"

// jsonのinclude
#include "nlohmann/json.hpp"
#include <fstream>

namespace nl = nlohmann;

static nl::json ModelList;		// リストの生成

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CBackModel::CBackModel(int nPriority /* =1 */) : CObject3D(nPriority)
{
	// タイプ設定
	SetType(TYPE_BILL);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CBackModel::~CBackModel()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CBackModel::Update()
{
	D3DXVECTOR3 move = GetMove();

	move.z += -3.0f;

	// posにmoveを加算
	MovePos(move);

	D3DXVECTOR3 pos = GetPos();
	if (pos.z < -2000.0f)
	{
		pos.z += 3000.0f * 2.0f;
	}

	// posを代入
	SetPos(pos);
}

//--------------------------------------------------
// 読み込み
//--------------------------------------------------
void CBackModel::Load(const char *pFdata)
{
	// ファイルオープン
	std::ifstream ifs(pFdata);

	if (ifs)
	{// 開けたら
		ifs >> ModelList;

		// モデルの最大数
		int nIndex = ModelList["INDEX"];
		// 位置
		D3DXVECTOR3 pos(0.0f, 0.0f, 0.0f);
		// 移動量
		D3DXVECTOR3 move(0.0f, 0.0f, 0.0f);
		// 角度
		D3DXVECTOR3 rot(0.0f, 0.0f, 0.0f);
		// モデルの種類
		MODEL_TYPE modelType;

		for (int nCntModel = 0; nCntModel < nIndex; nCntModel++)
		{// タグ付け
			std::string name = "MODEL";
			std::string Number = std::to_string(nCntModel);
			name += Number;

			pos = D3DXVECTOR3(ModelList[name]["POS"]["X"], ModelList[name]["POS"]["Y"], ModelList[name]["POS"]["Z"]);
			move = D3DXVECTOR3(ModelList[name]["MOVE"]["X"], ModelList[name]["MOVE"]["Y"], ModelList[name]["MOVE"]["Z"]);
			rot = D3DXVECTOR3(ModelList[name]["ROT"]["X"], ModelList[name]["ROT"]["Y"], ModelList[name]["ROT"]["Z"]);
			modelType = ModelList[name]["TYPE"];

			CBackModel *pModel = CBackModel::Create(modelType);
			pModel->SetPos(pos);
			pModel->SetMove(move);
			pModel->SetRot(rot);
		}
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CBackModel *CBackModel::Create(MODEL_TYPE type)
{
	CBackModel *pBackModel = nullptr;
	pBackModel = new CBackModel;

	if (pBackModel != nullptr)
	{
		pBackModel->Init();

		//現在の画面(モード)の終了処理
		switch (type)
		{
		case MODEL_TYPE_BILL:
			pBackModel->SetModel("data/MODEL/background_bill.x");

			break;

		case MODEL_TYPE_INTERSECTION:
			pBackModel->SetModel("data/MODEL/background_intersection.x");

			break;

		case MODEL_TYPE_HIGHWAY:
			pBackModel->SetModel("data/MODEL/background_highway.x");

			break;

		default:
			assert(false);
			break;
		}
	}
	else
	{
		assert(false);
	}

	return pBackModel;
}
