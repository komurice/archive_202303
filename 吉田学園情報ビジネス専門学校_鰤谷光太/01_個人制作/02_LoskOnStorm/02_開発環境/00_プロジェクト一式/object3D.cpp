//==================================================
// object3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"
#include "object3D.h"
#include "renderer.h"
#include "camera.h"

//**************************************************
// 静的メンバ変数
//**************************************************
const float CObject3D::BILLBOARD_HEIGHT = 10.0f;
const float CObject3D::BILLBORAD_WIDTH = 10.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CObject3D::CObject3D(int nPriority /* =3 */) : CObject(nPriority)
{
	// 位置
	m_posOrigin = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 向き
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// なめらか
	m_rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 最小値
	m_vtxMinModel = D3DXVECTOR3(FLT_MAX, FLT_MAX, FLT_MAX);
	// 最大値
	m_vtxMaxModel = D3DXVECTOR3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	// 大きさ
	m_size = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	for (int nCnt = 0; nCnt < MAX_TEXTURE; nCnt++)
	{// テクスチャの列挙型
		m_texture[nCnt] = nullptr;
	}
	// 頂点バッファへのポインタ
	m_pVtxBuff = nullptr;
	// メッシュ(頂点の集まり)情報へのポインタ
	m_mesh = nullptr;
	// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
	m_buffMat = nullptr;
	// マテリアル情報の数
	m_numMat = NULL;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CObject3D::~CObject3D()
{
	assert(m_pVtxBuff == nullptr);
	assert(m_mesh == nullptr);
	assert(m_buffMat == nullptr);
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CObject3D::Init()
{
	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CObject3D::Uninit()
{
	for (int i = 0; i < (int)m_numMat; i++)
	{
		if (m_texture[i] != nullptr)
		{// テクスチャの解放
			m_texture[i]->Release();
			m_texture[i] = nullptr;
		}
	}
	// メッシュの破棄
	if (m_mesh != nullptr)
	{
		m_mesh->Release();
		m_mesh = nullptr;
	}
	// マテリアルの破棄
	if (m_buffMat != nullptr)
	{
		m_buffMat->Release();
		m_buffMat = nullptr;
	}

	Release();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CObject3D::Update()
{
	
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CObject3D::Draw()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans, mtxScale;
	// 現在のマテリアル保存用
	D3DMATERIAL9 matDef;
	// マテリアルデータへのポインタ
	D3DXMATERIAL *pMat;
	D3DXVECTOR3 scale(5.0f, 5.0f, 5.0f);

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxWorld);

	// 行列拡縮関数
	D3DXMatrixScaling(&mtxScale, scale.x, scale.y, scale.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxScale);

	// 向きを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 現在のマテリアル保持
	pDevice->GetMaterial(&matDef);

	// マテリアルデータへのポインタを取得
	pMat = (D3DXMATERIAL*)m_buffMat->GetBufferPointer();

	for (int i = 0; i < (int)m_numMat; i++)
	{
		pMat[i].MatD3D.Ambient = pMat[i].MatD3D.Diffuse;
		// マテリアルの設定
		pDevice->SetMaterial(&pMat[i].MatD3D);

		// テクスチャの設定
		pDevice->SetTexture(0, m_texture[i]);

		// モデルパーツの描画
		m_mesh->DrawSubset(i);
	}

	// 保存していたマテリアルを戻す
	pDevice->SetMaterial(&matDef);

	// テクスチャの設定
	pDevice->SetTexture(0, NULL);
}

//--------------------------------------------------
// モデルのセット
//--------------------------------------------------
void CObject3D::SetModel(char *filename)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// 頂点座標の最小値
	m_vtxMinModel = D3DXVECTOR3(100.0f, 100.0f, 100.0f);
	// 頂点座標の最大値
	m_vtxMaxModel = D3DXVECTOR3(-100.0f, -100.0f, -100.0f);

	// メッシュ(頂点の集まり)情報へのポインタ
	m_mesh = nullptr;
	// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
	m_buffMat = nullptr;

	m_numMat = NULL;

	// Xファイルの読み込み
	D3DXLoadMeshFromX(filename,
		D3DXMESH_SYSTEMMEM,
		pDevice,
		NULL,
		&m_buffMat,
		NULL,
		&m_numMat,
		&m_mesh);

	// バッファの先頭ポインタをD3DXMATERIALにキャストして取得
	D3DXMATERIAL *pMat = (D3DXMATERIAL*)m_buffMat->GetBufferPointer();

	// 各メッシュのマテリアル情報を取得する
	for (int i = 0; i < (int)m_numMat; i++)
	{
		m_texture[i] = NULL;

		if (pMat[i].pTextureFilename != NULL)
		{// マテリアルで設定されているテクスチャ読み込み
			D3DXCreateTextureFromFileA(pDevice,
				pMat[i].pTextureFilename,
				&m_texture[i]);
		}
	}
}
