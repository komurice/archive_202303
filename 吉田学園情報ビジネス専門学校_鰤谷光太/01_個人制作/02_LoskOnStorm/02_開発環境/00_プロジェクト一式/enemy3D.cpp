//==================================================
// enemy3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include <memory>
#include <iostream>

#include "manager.h"
#include "game.h"

#include "enemy3D.h"
#include "cobra.h"
#include "tridoron.h"
#include "snake.h"
#include "hornet.h"
#include "sonic.h"
#include "boss.h"
#include "tutorial_enemy.h"

#include "lock_on_target.h"
#include "lock_on_cursor.h"
#include "homing_laser.h"

//**************************************************
// 静的メンバ変数
//**************************************************
CEnemy3D *CEnemy3D::m_pEnemy = nullptr;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemy3D::CEnemy3D() : 
	m_pLockOnTarget(nullptr),
	m_pLockOnCursor(nullptr)
{
	// タイプ設定
	SetType(TYPE_ENEMY);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemy3D::~CEnemy3D()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemy3D::Init()
{
	// 初期化
	CObject3D::Init();

	// LockOnTargetのポインタ
	m_pLockOnTarget = nullptr;
	// LockOnCursorのポインタ
	m_pLockOnCursor = nullptr;
	// HomingLaserのポインタ
	m_pHomingLaser = nullptr;
	// 当たり判定でとる大きさ
	CObject3D::SetSize(D3DXVECTOR3(50.0f, 50.0f, 50.0f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEnemy3D::Uninit()
{
	if (m_pLockOnCursor != nullptr)
	{// エネミーが死んだときにロックオン数を元に戻す
		m_pLockOnCursor->DeadEnemy(this);
	}

	if (m_pHomingLaser != nullptr)
	{// エネミーが死んだときにロックオン数を元に戻す
		m_pHomingLaser->DeadEnemy(this);
	}

	if (m_pLockOnTarget != nullptr)
	{// エネミーについているターゲットの破棄
		m_pLockOnTarget->Uninit();
		m_pLockOnTarget = nullptr;
	}

	CObject3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemy3D::Update()
{
	CObject3D::Update();

	if (m_pLockOnTarget != nullptr)
	{// エネミーのところにロックオンを設置する
		m_pLockOnTarget->SetPos(GetPos());
	}
}

//--------------------------------------------------
// ロックオンカーソルをセットする
//--------------------------------------------------
bool CEnemy3D::SetLockOn(CLockOnCursor* pLockOnCursor)
{
	if (!IsLockOn())
	{
		assert(m_pLockOnTarget == nullptr);
		// エネミーのところにロックオンを設置する
		m_pLockOnTarget = CLockOnTarget::Create(D3DXVECTOR3(100.0f, 0.0f, 100.0f));
		m_pLockOnTarget->SetPos(GetPos());

		assert(m_pLockOnCursor == nullptr);
		m_pLockOnCursor = pLockOnCursor;

		return true;
	}
	else
	{
		assert(false);
	}

	return false;
}

//--------------------------------------------------
// それぞれのエネミーの生成
//--------------------------------------------------
CEnemy3D *CEnemy3D::Create(ENEMY_TYPE type)
{
	// 現在の画面(モード)の終了処理
	switch (type)
	{
	case ENEMY_TYPE_COBRA:
		m_pEnemy = CCobra::Create();
		m_pEnemy->SetModel("data/MODEL/cobra.x");

		break;

	case ENEMY_TYPE_SNAKE:
		m_pEnemy = CSnake::Create();
		m_pEnemy->SetModel("data/MODEL/cobra.x");

		break;

	case ENEMY_TYPE_TRIDORON:
		m_pEnemy = CTridoron::Create();
		m_pEnemy->SetModel("data/MODEL/tridoron.x");

		break;

	case ENEMY_TYPE_HORNET:
		m_pEnemy = CHornet::Create();
		m_pEnemy->SetModel("data/MODEL/hornet.x");

		break;

	case ENEMY_TYPE_SONIC:
		m_pEnemy = CSonic::Create();
		m_pEnemy->SetModel("data/MODEL/sonic.x");

		break;

	case ENEMY_TYPE_BOSS:
		m_pEnemy = CBoss::Create();
		m_pEnemy->SetModel("data/MODEL/boss.x");

		break;

	case ENEMY_TYPE_TUTORIAL:
		m_pEnemy = CTutorialEnemy::Create();
		m_pEnemy->SetModel("data/MODEL/tridoron.x");

		break;

	default:
		assert(false);
		break;
	}

	return m_pEnemy;
}
