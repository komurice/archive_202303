//==================================================
// effect.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "effect.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEffect::CEffect()
{
	// タイプ設定
	SetType(TYPE_EFFECT);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEffect::~CEffect()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEffect::Init()
{
	// 初期化
	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_BULLET);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CEffect::Uninit()
{
	// 終了
	CPolygon3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEffect::Update()
{
	// 更新
	CPolygon3D::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CEffect::Draw()
{
	// 描画
	CPolygon3D::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEffect* CEffect::Create(const D3DXVECTOR3 size)
{
	CEffect *pEffect = nullptr;
	pEffect = new CEffect;

	if (pEffect != nullptr)
	{
		pEffect->Init();
		pEffect->SetSize(size);
		pEffect->SetBillboard(true);
	}
	else
	{
		assert(false);
	}

	return pEffect;
}
