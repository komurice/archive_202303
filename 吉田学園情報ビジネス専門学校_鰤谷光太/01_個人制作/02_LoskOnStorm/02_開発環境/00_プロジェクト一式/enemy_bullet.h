//==================================================
// enemy_bullet.h
// Author: Buriya Kota
//==================================================
#ifndef _ENEMY_BULLET_H_
#define _ENEMY_BULLET_H_

//**************************************************
// インクルード
//**************************************************
#include "polygon3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CEnemyBullet : public CPolygon3D
{
public:
	explicit CEnemyBullet(int nPriority = PRIORITY_BULLET);
	~CEnemyBullet() override;

	virtual HRESULT Init() override;
	virtual void Update() override;

	static CEnemyBullet *Create(const D3DXVECTOR3 move, const D3DXVECTOR3 size);

private:
	void EnemyBulletMove_();
	void CollisionPlayer_();

private:
	// 終了までの時間
	int m_time;

};

#endif	// _ENEMY_BULLET_H_