//==================================================
// camera.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include "manager.h"
#include "input_keyboard.h"
#include "input.h"

#include "camera.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCamera::CCamera()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCamera::~CCamera()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
void CCamera::Init(void)
{
	// 視点・注視点・上方向を設定する
	m_posV = D3DXVECTOR3(0.0f, 1300.0f, -1000.0f);		// 視点
	m_posR = D3DXVECTOR3(0.0f, 0.0f, 0.0f);				// 注視点
	m_posVDest = D3DXVECTOR3(0.0f, 100.0f, -100.0f);	// 目的の視点
	m_posRDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);			// 目的の注視点
	m_vecU = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	float fDisX = m_posR.x - m_posV.x;
	float fDisY = m_posR.y - m_posV.y;
	float fDisZ = m_posR.z - m_posV.z;

	// 角度の初期化
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	// 初期の角度の計算
	m_rot.x = atan2f(fDisZ, fDisY);

	// 視点から注視点までの距離を算出
	m_fDis = sqrtf((fDisX * fDisX) + (fDisZ * fDisZ));
	m_fDis = sqrtf((m_fDis * m_fDis) + (fDisY * fDisY));
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCamera::Uninit(void)
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCamera::Update(void)
{
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();

	//視点の旋回
	if (pInputKeyoard->GetPress(DIK_Z))
	{
		m_rot.y += 0.01f;
	}
	if (pInputKeyoard->GetPress(DIK_C))
	{
		m_rot.y -= 0.01f;
	}

	//角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}
	if (m_rot.x > D3DX_PI)
	{
		m_rot.x -= D3DX_PI * 2.0f;
	}
	else if (m_rot.x < -D3DX_PI)
	{
		m_rot.x += D3DX_PI * 2.0f;
	}

	// 注視点の算出
	m_posV.z = m_posR.z - sinf(m_rot.x) * cosf(m_rot.y) * m_fDis;
	m_posV.x = m_posR.x - sinf(m_rot.x) * sinf(m_rot.y) * m_fDis;
	m_posV.y = m_posR.y - cosf(m_rot.x) * m_fDis;

	Set();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCamera::Set()
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// ビューマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxView);

	// ビューマトリックスの作成
	D3DXMatrixLookAtLH(&m_mtxView,
		&m_posV,
		&m_posR,
		&m_vecU);

	// ビューマトリックスの設定
	pDevice->SetTransform(D3DTS_VIEW, &m_mtxView);

	// プロジェクションマトリックスの初期化
	D3DXMatrixIdentity(&m_mtxProjection);

	// プロジェクションマトリックスの作成
	D3DXMatrixPerspectiveFovLH(&m_mtxProjection,
		D3DXToRadian(45.0f),								// 視野角
		(float)CManager::SCREEN_WIDTH / (float)CManager::SCREEN_HEIGHT,			// アスペクト比
		10.0f,												// ニア
		10000.0f);											// ファー

	// プロジェクションマトリックスの設定
	pDevice->SetTransform(D3DTS_PROJECTION, &m_mtxProjection);
}
