//==================================================
// player3D.h
// Author: Buriya Kota
//==================================================
#ifndef _PLAYER3D_H_
#define _PLAYER3D_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CPlayer3D : public CObject3D
{
public:
	static const float PLAYER_SPEED;

public:
	CPlayer3D();
	~CPlayer3D() override;

	HRESULT Init() override;
	void Update() override;

	void InScreen_();

	static CPlayer3D *Create();

private:
	void Control_();

	void CollisionEnemy_();
	void CollisionItem_();

private:
	CLockOnCursor *m_pLockOnCursor;
	int m_time;

};

#endif	// _PLAYER3D_H_