//==================================================
// mesh_trajectory.h
// Author: Buriya Kota
//==================================================
#ifndef _MESH_TRAJECTORY_H_
#define _MESH_TRAJECTORY_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"
#include "texture.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// クラス
//**************************************************
class CMeshTrajectory : public CObject
{
public:
	explicit CMeshTrajectory(int nPriority = PRIORITY_BULLET);
	~CMeshTrajectory() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetTexture(CTexture::TEXTURE texture) { m_texture = texture; }	// テクスチャの設定

	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetSize() const { return m_size; }

	// pVtxを足していく関数
	void AddVtx(const D3DXVECTOR3& pos, const D3DXVECTOR3& move);

private:
	// 位置
	D3DXVECTOR3 m_pos;
	// 移動量
	D3DXVECTOR3 m_move;
	// 回転
	D3DXVECTOR3 m_rot;
	// サイズ
	D3DXVECTOR3 m_size;
	int m_maxVtx;
	int m_maxPolygon;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// テクスチャの列挙型
	CTexture::TEXTURE m_texture;
	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;
	// 現在使っている頂点数を保存する場所
	int m_currentVtx;

};

#endif	// _MESH_TRAJECTORY_H_