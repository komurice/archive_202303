//==================================================
// enemy.h
// Author: Buriya Kota
//==================================================
#ifndef _ENEMY_H_
#define _ENEMY_H_

//**************************************************
// インクルード
//**************************************************
#include "object2D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CEnemy : public CObject2D
{
public:
	const int ENEMY_ANIM = 15;
	const int ANIM_SPEED = 10;

public:
	CEnemy();
	virtual ~CEnemy() override;

	virtual HRESULT Init() override;
	virtual void Update() override;

	static CEnemy *Create(const D3DXVECTOR3 size);

private:
	void Control_();

private:
	// 移動量
	D3DXVECTOR3 m_move;
	// アニメーションカウンター
	int m_nCounterAnim;
	// アニメーションパターンNo.
	int m_nPatternAnim;

};


#endif	// _ENEMY_H_