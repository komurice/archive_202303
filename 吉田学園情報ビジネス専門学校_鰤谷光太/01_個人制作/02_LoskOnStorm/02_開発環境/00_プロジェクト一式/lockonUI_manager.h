//==================================================
// lockonUI_manager.h
// Author: Buriya Kota
//==================================================
#ifndef _LOCKONUI_MANAGER_H_
#define _LOCKONUI_MANAGER_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnUI;

//**************************************************
// クラス
//**************************************************
class CLockOnUIManager : public CObject
{
public:
	static const int MAX_UI = 8;

public:
	explicit CLockOnUIManager(int nPriority = PRIORITY_UI);
	~CLockOnUIManager() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override {}
	void Draw() override {}

	void SetLockOnNum(int num) { m_nMaxLockOn = num; }
	int GetLockOnNum() { return m_nMaxLockOn; }


	static void AddUI(int nValue);
	void LockOnNum(int nValue);
	void ClearLockOn(int nValue);

	static CLockOnUIManager *Create();

private:
	// Number型の配列
	static CLockOnUI *m_pLockOnUI[MAX_UI];
	// 現在のロックオン数
	static int m_nMaxLockOn;
	static int m_nNowLockOn;

};

#endif	// _LOCKONUI_MANAGER_H_