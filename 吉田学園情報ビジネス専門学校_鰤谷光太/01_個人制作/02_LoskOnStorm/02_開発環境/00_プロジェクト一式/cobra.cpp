//==================================================
// cobra.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>
#include "manager.h"
#include "cobra.h"

#include "utility.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCobra::CCobra()
{
	SetEnemyType(ENEMY_TYPE_COBRA);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCobra::~CCobra()
{
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCobra::Update()
{
	CEnemy3D::Update();

	MoveCobra_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CCobra * CCobra::Create()
{
	CCobra *pCobra;
	pCobra = new CCobra;

	if (pCobra != nullptr)
	{
		pCobra->Init();
	}
	else
	{
		assert(false);
	}

	return pCobra;
}

//--------------------------------------------------
// コブラの動き
//--------------------------------------------------
void CCobra::MoveCobra_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	m_nCntFrame++;

	// 左から着て一定フレームで止まり高さ上昇して上に消えていく
	if (m_nCntFrame > 260)
	{
		move.z += 0.5f;
		SetMove(move);
	}

	MovePos(move);

	D3DXVECTOR3 pos = GetPos();

	if (pos.z > 1000.0f)
	{
		Uninit();
	}
}
