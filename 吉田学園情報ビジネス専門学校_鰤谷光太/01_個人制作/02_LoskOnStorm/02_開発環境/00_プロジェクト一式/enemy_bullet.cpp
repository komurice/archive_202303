//==================================================
// boss_bullet.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "manager.h"

#include "enemy_bullet.h"
#include "player3D.h"
#include "explosion.h"
#include "life_manager.h"
#include "fade.h"
#include "sound.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CEnemyBullet::CEnemyBullet(int nPriority) : CPolygon3D(nPriority)
{
	// タイプ設定
	SetType(TYPE_BOSS_BULLET);
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CEnemyBullet::~CEnemyBullet()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CEnemyBullet::Init()
{
	m_time = 0;

	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_ENEMY_BULLET);

	return S_OK;
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CEnemyBullet::Update()
{
	m_time++;

	CPolygon3D::Update();

	CollisionPlayer_();

	EnemyBulletMove_();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CEnemyBullet *CEnemyBullet::Create(const D3DXVECTOR3 move, const D3DXVECTOR3 size)
{
	CEnemyBullet *pEnemyBullet;
	pEnemyBullet = new CEnemyBullet;

	if (pEnemyBullet != nullptr)
	{
		pEnemyBullet->Init();
		pEnemyBullet->SetMove(move);
		pEnemyBullet->SetSize(size);
		pEnemyBullet->SetBillboard(true);
	}
	else
	{
		assert(false);
	}

	return pEnemyBullet;
}

//--------------------------------------------------
// 弾の挙動
//--------------------------------------------------
void CEnemyBullet::EnemyBulletMove_()
{
	D3DXVECTOR3 move = GetMove();
	SetMove(move);

	MovePos(move);

	if (GetPos().z >= 1000.0f)
	{
		Uninit();
	}
}

//--------------------------------------------------
// プレイヤーとの当たり判定
//--------------------------------------------------
void CEnemyBullet::CollisionPlayer_()
{
	D3DXVECTOR3 pos = GetPos();
	D3DXVECTOR3 size = GetSize();

	CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);

	for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
	{
		if (pObject[nCnt] == nullptr || pObject[nCnt]->IsDeleted())
		{
			continue;
		}

		CObject::TYPE_3D objType = pObject[nCnt]->GetType();
		if (objType != CObject::TYPE_PLAYER)
		{// 球の当たり判定
			continue;
		}

		// ダイナミックキャスト
		CPlayer3D* pPlayer = dynamic_cast<CPlayer3D*>(pObject[nCnt]);
		if ((pPlayer == nullptr))
		{
			assert(false);
			continue;
		}

		D3DXVECTOR3 targetPos = pPlayer->GetPos();
		D3DXVECTOR3 targetSize = pPlayer->GetSize();

		bool isHit = IsCollisionSphere(targetPos, pos, targetSize, size);
		if (isHit)
		{// 当たった時
			// サウンド
			CManager::GetSound()->Play(CSound::LABEL_SE_HIT);

			CExplosion::Create(D3DXVECTOR3(60.0f, 60.0f, 0.0f))->SetPos(pPlayer->GetPos());

			CLifeManager::SubLife(1);
			if (CLifeManager::GetLife() <= 0)
			{
				pPlayer->Uninit();

				CFade::GetInstance()->SetFade(CManager::MODE_RANKING);
			}

			Uninit();
		}
	}
}
