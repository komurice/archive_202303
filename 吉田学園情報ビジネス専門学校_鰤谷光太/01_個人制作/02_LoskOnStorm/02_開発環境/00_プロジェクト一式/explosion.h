//==================================================
// explosion.h
// Author: Buriya Kota
//==================================================
#ifndef _EXPLOSION_H_
#define _EXPLOSION_H_

//**************************************************
// インクルード
//**************************************************
#include "polygon3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CExplosion : public CPolygon3D
{
public:
	const int EXPLOSION_ANIM = 7;

public:
	CExplosion();
	~CExplosion() override;

	HRESULT Init() override;
	void Update() override;

	static CExplosion *Create(const D3DXVECTOR3 size);

private:
	// 移動量
	D3DXVECTOR3 m_move;
	// テクスチャへのポインタ
	CTexture::TEXTURE m_pTexture;
	// アニメーションカウンター
	int m_nCounterAnim;
	// アニメーションパターンNo.
	int m_nPatternAnim;

};

#endif	// _EXPLOSION_H_