//==================================================
// enemy3D.h
// Author: Buriya Kota
//==================================================
#ifndef _ENEMY3D_H_
#define _ENEMY3D_H_

//**************************************************
// インクルード
//**************************************************
#include "object3D.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnTarget;
class CLockOnCursor;
class CHomingLaser;

//**************************************************
// 定数定義
//**************************************************

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CEnemy3D : public CObject3D
{
public:
	// 画面(モード)の種類
	enum ENEMY_TYPE
	{
		ENEMY_TYPE_COBRA = 0,	// コブラ
		ENEMY_TYPE_SNAKE,		// スネーク
		ENEMY_TYPE_TRIDORON,	// トライドロン
		ENEMY_TYPE_HORNET,		// ホーネット
		ENEMY_TYPE_SONIC,		// ソニック
		ENEMY_TYPE_BOSS,		// ボス
		ENEMY_TYPE_TUTORIAL,	// チュートリアル
		ENEMY_TYPE_MAX
	};

	CEnemy3D();
	virtual ~CEnemy3D() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;

	// ロックオンの設定
	virtual bool SetLockOn(CLockOnCursor* pLockOnCursor);
	virtual bool IsLockOn() { return (m_pLockOnTarget != nullptr); }
	void SetEnemyType(ENEMY_TYPE enemyType) { m_enemyType = enemyType; }

	void SetHomingLaser(CHomingLaser* pHomingLaser) { assert(pHomingLaser == nullptr || m_pHomingLaser == nullptr); m_pHomingLaser = pHomingLaser; }

	// ライフ
	void SetLife(int nLife) { m_nLife = nLife; }
	int GetLife() { return m_nLife; }
	ENEMY_TYPE GetEnemyType() { return m_enemyType; }

	void SubLife(int nDamage) { m_nLife -= nDamage; }

	static CEnemy3D *Create(ENEMY_TYPE type);

private:
	// 
	static CEnemy3D *m_pEnemy;
	// ロックオンのポインタ
	CLockOnTarget* m_pLockOnTarget;
	// ロックオンしているカーソルのポインタ
	CLockOnCursor* m_pLockOnCursor;
	// 今自分が狙われているレーザー
	CHomingLaser* m_pHomingLaser;
	// エネミーの種類
	ENEMY_TYPE m_enemyType;
	// エネミーのライフ
	int m_nLife;
};

#endif	// _ENEMY3D_H_