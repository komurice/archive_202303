//==================================================
// lock_on_cursor.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "object.h"
#include "game.h"
#include "tutorial.h"
#include "manager.h"
#include "input_keyboard.h"
#include "utility.h"

#include "score.h"
#include "homing_laser.h"
#include "enemy3D.h"
#include "lock_on_cursor.h"
#include "lock_on_target.h"
#include "lockonUI_manager.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CLockOnCursor::CLockOnCursor(int nPriority /* =4 */) : CPolygon3D(nPriority)
{
	// ロックオンした数のカウント
	m_nLimitNum = 0;
	// 現在のロックオン数
	m_nCurrentNum = 0;
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CLockOnCursor::~CLockOnCursor()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CLockOnCursor::Init()
{
	// 初期化
	CPolygon3D::Init();
	// テクスチャの設定
	CPolygon3D::SetTexture(CTexture::TEXTURE_LOCK_ON_CURSOR);

	// ロックオンの限界数
	m_nLimitNum = INIT_TARGET;
	// 現在のロックオン数
	m_nCurrentNum = 0;

	return S_OK;
}

void CLockOnCursor::Uninit()
{
	// 初期化
	CPolygon3D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CLockOnCursor::Update()
{
	CPolygon3D::Update();

	D3DXVECTOR3 pos = GetWorldToScreenPos(GetPos());
	D3DXVECTOR3 size = GetSize();

	if (m_nCurrentNum < m_nLimitNum)
	{
		for (int nCnt = 0; nCnt < CObject::MAX_OBjECT; nCnt++)
		{
			CObject **pObject = CObject::GetCObject(PRIORITY_OBJECT);
			if (pObject[nCnt] == nullptr || pObject[nCnt]->IsDeleted())
			{
				continue;
			}

			// タイプの取得
			CObject::TYPE_3D objType = pObject[nCnt]->GetType();
			if (objType != CObject::TYPE_ENEMY)
			{
				continue;
			}

			// ダイナミックキャスト
			CEnemy3D* pEnemy = dynamic_cast<CEnemy3D*>(pObject[nCnt]);
			if ((pEnemy == nullptr) || (pEnemy->IsLockOn()))
			{
				continue;
			}

			// カーソルとエネミーの当たり判定
			D3DXVECTOR3 targetPos = GetWorldToScreenPos(pEnemy->GetPos());
			D3DXVECTOR3 targetSize = pEnemy->GetSize();
			bool isHit = IsCollision(targetPos, pos, targetSize, size);

			if (isHit)
			{// 当たった時
				// ロックオンする
				if (pEnemy->SetLockOn(this))
				{// ロックオンできた
					m_pEnemy[m_nCurrentNum] = pEnemy;
					// ロックオンした数を増やす
					m_nCurrentNum++;

					if (CGame::GetLockOnUIManager() != nullptr)
					{
						CGame::GetLockOnUIManager()->LockOnNum(m_nCurrentNum);
					}

					if (CTutorial::GetLockOnUIManager() != nullptr)
					{
						CTutorial::GetLockOnUIManager()->LockOnNum(m_nCurrentNum);
					}
				}
			}
		}
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CLockOnCursor::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_ALWAYS);

	CPolygon3D::Draw();

	pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CLockOnCursor* CLockOnCursor::Create(const D3DXVECTOR3 size, int nPriority)
{
	CLockOnCursor *pLockOnCursor;
	pLockOnCursor = new CLockOnCursor(nPriority);

	if (pLockOnCursor != nullptr)
	{
		pLockOnCursor->Init();
		pLockOnCursor->SetSize(size);
	}
	else
	{
		assert(false);
	}

	return pLockOnCursor;
}

//--------------------------------------------------
// エネミーが死んだときにロックオン数を元に戻す
//--------------------------------------------------
void CLockOnCursor::DeadEnemy(CEnemy3D* enemy)
{
	for (int nCnt = 0; nCnt < MAX_TARGET; nCnt++)
	{
		if ((m_pEnemy[nCnt] != nullptr) && (m_pEnemy[nCnt] == enemy))
		{
			m_pEnemy[nCnt] = nullptr;
			m_nCurrentNum--;
			CGame::GetLockOnUIManager()->ClearLockOn(m_nCurrentNum);
			return;
		}
	}

}

//--------------------------------------------------
// アイテムを取った時
//--------------------------------------------------
void CLockOnCursor::AddLockOn()
{
	if (m_nLimitNum < 8)
	{
		m_nLimitNum += 1;
	}
	else
	{
		CGame::GetScore()->AddScore(2000);
	}
}
