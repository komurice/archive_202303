//==================================================
// object.h
// Author: Buriya Kota
//==================================================
#ifndef _OBJECT_H_
#define _OBJECT_H_

//**************************************************
// インクルード
//**************************************************
#include <functional>

#include "renderer.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// クラス
//**************************************************
class CObject
{
public:
	// オブジェクトの最大数
	static const int MAX_OBjECT = 384;
	// プライオリティの最大数
	static const int MAX_PRIO = 6;

	enum TYPE_3D
	{
		TYPE_NONE = 0,
		TYPE_PLAYER,
		TYPE_ENEMY,
		TYPE_BULLET,
		TYPE_BOSS_BULLET,
		TYPE_HOMING_LASER,
		TYPE_EXPLOSION,
		TYPE_EFFECT,
		TYPE_BILL,
		TYPE_ROAD,
		TYPE_SCORE,
		TYPE_NUMBER,
		TYPE_LIFE,
		TYPE_MODE,
		TYPE_BOSS,
		TYPE_ITEM,
		TYPE_LOCKON_UI,
		TYPE_MAX
	};

	enum PRIORITY
	{
		PRIORITY_BG = 0,
		PRIORITY_BULLET,
		PRIORITY_OBJECT,
		PRIORITY_EFFECT,
		PRIORITY_UI,
		PRIORITY_FADE,
		PRIORITY_MAX
	};

public:
	explicit CObject(int nPriority = PRIORITY_OBJECT);
	virtual ~CObject();

	virtual HRESULT Init() = 0;
	virtual void Uninit() = 0;
	virtual void Update() = 0;
	virtual void Draw() = 0;

	// すべての破棄、更新、描画
	static void ReleaseAll();
	static void UpdateAll();
	static void DrawAll();

	static void ReleaseWithoutMode();

	static CObject **GetCObject(int nPriority) { return &ms_pObject[nPriority][0]; }
	//static void ExecCObjectAll(std::function<void(CObject*)> func);
	/*{
		for (int nCnt = 0; nCnt < MAX; nCnt++)
		{
			if (ms_pObject[nCnt] != nullptr && !ms_pObject[nCnt]->IsDeleted()) {
				func(ms_pObject[nCnt]);
			}
		}
	}*/

	void SetType(TYPE_3D type) { m_type = type; }

	TYPE_3D GetType() { return m_type; }

	bool IsDeleted() { return m_bDeleted; }

protected:
	void Release();

private:
	// タイプ
	TYPE_3D m_type;
	// 自分の実体
	static CObject *ms_pObject[MAX_PRIO][MAX_OBjECT];
	// 総数
	static int m_nNumAll;
	// 番号
	int m_Index;
	// プライオリティ番号
	int m_Priority;
	// 削除されたかどうか
	bool m_bDeleted;
};

// 球の当たり判定
bool IsCollisionSphere(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size);
bool IsCollision(D3DXVECTOR3 targetPos, D3DXVECTOR3 pos, D3DXVECTOR3 targetSize, D3DXVECTOR3 size);

#endif	// _OBJECT_H_