//==================================================
// object3D.h
// Author: Buriya Kota
//==================================================
#ifndef _OBJECT3D_H_
#define _OBJECT3D_H_

//**************************************************
// インクルード
//**************************************************
#include "main.h"
#include "texture.h"
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************

//**************************************************
// クラス
//**************************************************
class CObject3D : public CObject
{
public:
	static const int MAX_TEXTURE = 16;
	static const int MAX_BILLBOARD = 5;
	static const float BILLBORAD_WIDTH;
	static const float BILLBOARD_HEIGHT;

public:
	explicit CObject3D(int nPriority = PRIORITY_OBJECT);
	virtual ~CObject3D() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;
	virtual void Draw() override;

	// セッター
	void SetModel(char *filename);
	void SetPos(const D3DXVECTOR3& pos) { m_posOrigin = pos; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	void SetLife(const int &life) { m_nLife = life; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtxWorld = mtx; }

	// ゲッター
	D3DXVECTOR3 GetMove() { return m_move; }
	const D3DXVECTOR3& GetPos() const { return m_posOrigin; }
	const D3DXVECTOR3& GetSize() const { return m_size; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXMATRIX& GetWorldMtx() const { return m_mtxWorld; }

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_posOrigin += move; }

private:
	// 位置
	D3DXVECTOR3 m_posOrigin;
	// 位置
	D3DXVECTOR3 m_move;
	// 向き
	D3DXVECTOR3 m_rot;
	// 体力
	int m_nLife;
	// なめらか
	D3DXVECTOR3 m_rotDest;
	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// テクスチャの列挙型
	LPDIRECT3DTEXTURE9 m_texture[MAX_TEXTURE];
	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;
	// メッシュ(頂点の集まり)情報へのポインタ
	LPD3DXMESH m_mesh;
	// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
	LPD3DXBUFFER m_buffMat;
	// マテリアル情報の数
	DWORD m_numMat;
	// 大きさ
	D3DXVECTOR3 m_size;

};

#endif	// _OBJECT2D_H_