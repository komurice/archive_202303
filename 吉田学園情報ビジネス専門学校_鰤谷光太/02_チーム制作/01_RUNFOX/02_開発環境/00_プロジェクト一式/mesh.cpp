//============================
//
// メッシュ設定(まっすぐ)
// Author:hamada ryuuga
//
//============================


#include "mesh.h"
#include "manager.h"

#include "input.h"


#include <iostream>
#include <fstream>
#include <windows.h>
#include <nlohmann/json.hpp>
#include <string>

#include <stdio.h>
#include <tchar.h>
#include <locale.h>
#include <windows.h>
#include <sstream>

#include "utility.h"

namespace nl = nlohmann;

nl::json JMesh;//リストの生成

CMesh::CMesh(int nPriority /* =1 */) : CObject(nPriority)
{

}
CMesh::~CMesh()
{

}

//=========================================
// 初期化処理
//=========================================
HRESULT CMesh::Init(void)
{

	// 初期化処理
	m_pos = D3DXVECTOR3(-580.0f, 590.0f, 10.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);	// 回転座標
	

	return S_OK;
}

//=========================================
// 終了処理
//=========================================
void CMesh::Uninit(void)
{
	// 頂点バッファーの解放
	if (m_pVtxBuff != NULL)
	{
		m_pVtxBuff->Release();
		m_pVtxBuff = NULL;
	}
	if (m_pTextureEmesh != NULL)
	{
		m_pTextureEmesh->Release();
		m_pTextureEmesh = NULL;
	}
	if (m_pIdxBuff != NULL)
	{
		m_pIdxBuff->Release();
		m_pIdxBuff = NULL;
	}

	DeletedObj();
}

//=========================================
// 更新処理
//=========================================
void CMesh::Update(void)
{

}

//=========================================
// 描画処理
//=========================================
void CMesh::Draw(void)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス

	//ライト設定falseにするとライトと食らわない
	//pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);
	// ワールドマトリックスの初期化
	// 行列初期化関数(第1引数の行列を単位行列に初期化)
	D3DXMatrixIdentity(&m_mtxWorld);

	// 向きを反映
	// 行列回転関数(第1引数にヨー(y)ピッチ(x)ロール(z)方向の回転行列を作成)
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxRot);

	// 位置を反映
	// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
	D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
	// 行列掛け算関数(第2引数×第3引数第を１引数に格納)
	D3DXMatrixMultiply(&m_mtxWorld, &m_mtxWorld, &mtxTrans);

	// ワールド座標行列の設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtxWorld);

	// 頂点バッファをデバイスのデータストリームに設定
	pDevice->SetStreamSource(0, m_pVtxBuff, 0, sizeof(VERTEX_3D));

	//インデックスバッファ設定
	pDevice->SetIndices(m_pIdxBuff);
	// 頂点フォーマットの設定
	pDevice->SetFVF(FVF_VERTEX_3D);
	//テクスチャの設定
	pDevice->SetTexture(0, m_pTextureEmesh);

	// ポリゴンの描画
  	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_nVtx, 0, m_por);

	// ステンシルバッファ -> 有効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

	// ステンシルバッファと比較する参照値設定 -> ref
	pDevice->SetRenderState(D3DRS_STENCILREF, 0x01);

	// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
	pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

	// ステンシルテストの比較方法 ->
	// （参照値 >= ステンシルバッファの参照値）なら合格
	pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_EQUAL);

	// ステンシルテストの結果に対しての反映設定
	pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_INCR);			// Zとステンシル成功
	pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
	pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);			// Zのみ失敗

	// ポリゴンの描画
	pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLESTRIP, 0, 0, m_nVtx, 0, m_por);

	//テクスチャの設定
	pDevice->SetTexture(0, NULL);

	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	// ステンシルバッファ -> 無効
	pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}



//=============================================================================
// GetPos関数
//=============================================================================
const D3DXVECTOR3 *CMesh::GetPos() const
{
	return &m_posOrigin;
}

//=============================================================================
// SetPos関数
//=============================================================================
void CMesh::SetPos(const D3DXVECTOR3 &pos)
{
	m_pos = pos;
	m_posOrigin = pos;
}
//=============================================================================
// Create関数
//=============================================================================
CMesh* CMesh::Create()
{
	CMesh * pObject = nullptr;
	pObject = new CMesh;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}


//========================================
// 当たり判定
// Author: hamada ryuuga
//=========================================
bool CMesh::CollisionMesh(D3DXVECTOR3 *pPos,float *move, bool jump)
{
	bool bIsLanding = false;
	const int nTri = 3;
		// 頂点座標をロック
		VERTEX_3D* pVtx = NULL;
		m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

		//インデックスバッファのロック
		WORD* pIdx;
		m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

		D3DXMATRIX mtxRot, mtxTrans;	// 計算用マトリックス
		D3DXMATRIX mtxWorld;

		// ワールドマトリックスの初期化
		// 行列初期化関数(第1引数の行列を単位行列に初期化)
		D3DXMatrixIdentity(&mtxWorld);

		// 行列移動関数(第１引数にX,Y,Z方向の移動行列を作成)
		D3DXMatrixTranslation(&mtxTrans, m_posOrigin.x, m_posOrigin.y, m_posOrigin.z);
		// 行列掛け算関数(第2引数×第3引数を第１引数に格納)
		D3DXMatrixMultiply(&mtxWorld, &mtxWorld, &mtxTrans);

		for (int nCnt = 0; nCnt < m_por; nCnt++)
		{
			D3DXVECTOR3 posLineVec[nTri];

			posLineVec[0] = pVtx[pIdx[nCnt + 0]].pos;
			posLineVec[1] = pVtx[pIdx[nCnt + 1]].pos;
			posLineVec[2] = pVtx[pIdx[nCnt + 2]].pos;

			if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
				(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
				(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
			{//縮退ポリゴンを省き
				continue;
			}

			for (int i = 0; i < nTri; i++)
			{//ベクトル３座標をマトリックスで変換する（乗算）
				D3DXVec3TransformCoord(&posLineVec[i], &posLineVec[i], &mtxWorld);
			}

			int  LineCout = 0;

			for (int i = 0; i < nTri; i++)
			{
				//ベクトルS2 V2												
				D3DXVECTOR3 vecWall = posLineVec[(i + 1) % nTri] - posLineVec[i];

				//ベクトル現在のPOSと始点までの距離
				D3DXVECTOR3 vecPos = *pPos - posLineVec[i];

				//外積計算//辺１
				float vecLine = Vec2Cross(&vecPos, &vecWall);

				//三角の中に入ってるときの判定
				if ((nCnt % 2 == 0 && vecLine >= 0.0f) ||
					(nCnt % 2 != 0 && vecLine <= 0.0f))
				{
					LineCout++;
				}
				else
				{
					break;
				}
			}
			if (LineCout == nTri)
			{
				D3DXVECTOR3 V1 = posLineVec[1] - posLineVec[0];
				D3DXVECTOR3 V2 = posLineVec[2] - posLineVec[0];

				D3DXVECTOR3 Normal;
				//AとBの法線を求めるやつ
				D3DXVec3Cross(&Normal, &V1, &V2);

				//vecB をノーマライズして、長さ 1にする。
				D3DXVec3Normalize(&Normal, &Normal);

				D3DXVECTOR3 VecA = *pPos - posLineVec[0];
				//プレイヤーの位置補正
				
				if (pPos->y <= posLineVec[0].y - (Normal.x*(pPos->x - posLineVec[0].x) + Normal.z*(pPos->z - posLineVec[0].z)) / Normal.y &&!jump)
				{
					
					//meshと当たり判定当たった時
						OnHit();
						pPos->y = (posLineVec[0].y - (Normal.x*(pPos->x - posLineVec[0].x) + Normal.z*(pPos->z - posLineVec[0].z)) / Normal.y) - 0.001f;
						*move = 0.0f;
						//jump = false;

				}
			}
		}
		// 頂点座標をアンロック
		m_pVtxBuff->Unlock();
		// 頂点インデックスをアンロック
		m_pIdxBuff->Unlock();
	
	return bIsLanding;
}
 

//------------------------------------
//ファイルの読み込み
//------------------------------------
void CMesh::Loadfile(const char * pFileName)
{

	std::ifstream ifs(pFileName);

	int nIndex = 0;
	VERTEX_3D* pVtx = NULL;
	

	std::string str;
	if (ifs)
	{
		ifs >> JMesh;
		nIndex = JMesh["INDEX"];
		m_move = JMesh["MOVE"];
		D3DXVECTOR3 pos;
		D3DXVECTOR3 size;
		D3DXVECTOR3 rot;
		std::string Type;

		//初期化
		if (JMesh["MESHSIZE"] == NULL)
		{
			CMesh::SetVtxMeshSize(0);
		}
		else
		{
			CMesh::SetVtxMeshSize(JMesh["MESHSIZE"]);
		}

		str = JMesh["TEXPASS"];
		CMesh::SetTexture(str.c_str());

		//座標読み込み
		m_posOrigin = D3DXVECTOR3(JMesh["POSORIGIN"]["X"], JMesh["POSORIGIN"]["Y"], JMesh["POSORIGIN"]["Z"]);

		//サイズ読み込み
		m_MeshSize = D3DXVECTOR3(JMesh["MESHDATASIZE"]["X"], JMesh["MESHDATASIZE"]["Y"], JMesh["MESHDATASIZE"]["Z"]);

		// 頂点座標をロック	
		m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
		for (int nCnt = 0; nCnt < m_nVtx; nCnt++)
		{

			//めっしゅを真ん中にする補正
			//m_pos = D3DXVECTOR3(-(posx - 1)*MAX_SIZEMESH / 2, 0.0f, -posz * MAX_SIZEMESH / 2) + m_posOrigin;

			//JSONファイルタグ設定
			std::string name = "MESH";
			std::string Number = std::to_string(nCnt);
			name += Number;

			pos = D3DXVECTOR3(JMesh[name]["POS"]["X"], JMesh[name]["POS"]["Y"], JMesh[name]["POS"]["Z"]);

			//座標の補正
			pVtx[nCnt].pos = D3DXVECTOR3(pos.x, pos.y, pos.z);


			// 各頂点の法線の設定(※ベクトルの大きさは1にする必要がある)
			pVtx[nCnt].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

			// 頂点カラーの設定
			pVtx[nCnt].col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		}
		// 頂点座標をアンロック
		m_pVtxBuff->Unlock();
		CMesh::SetVtxMeshLight();

	}
}


//------------------------------------
//サイズ初期化
//------------------------------------
void CMesh::SetVtxMeshSize(int Size)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	//辺の頂点数

	m_xsiz = Size;
	m_zsiz = Size;
	m_X = m_xsiz + 1;//1多い数字
	m_Z = m_zsiz + 1;//1多い数字

	 //頂点数
	m_nVtx = m_X* m_Z;//頂点数を使ってるよ

	//インデックス数
	m_Index = (2 * m_X * m_zsiz + 2 * (m_zsiz - 1));

	m_por = m_Index - 2;

	// 頂点バッファの生成
	pDevice->CreateVertexBuffer(sizeof(VERTEX_3D) * m_nVtx,
		D3DUSAGE_WRITEONLY,
		FVF_VERTEX_3D,
		D3DPOOL_MANAGED,
		&m_pVtxBuff,
		NULL);

	//インデックスバッファ生成
	pDevice->CreateIndexBuffer(sizeof(WORD) * m_Index,
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&m_pIdxBuff,
		NULL);

	VERTEX_3D* pVtx = NULL;

	// 頂点座標をロック
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);

	// 頂点座標の設定
	for (int i = 0; i < m_nVtx; i++)
	{
		pVtx[i].pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		float posx = ((i % m_X) - 1.0f);
		float posz = ((i / m_Z) - 1.0f)*-1.0f;

		float texU = 1.0f / m_xsiz*(i % m_X);
		float texV = 1.0f / m_zsiz*(i / m_Z);

		//めっしゅを真ん中にする補正
		m_pos = (D3DXVECTOR3(-(posx - 1)*MAX_SIZEMESH / 2, 0.0f, -posz * MAX_SIZEMESH / 2))+ m_posOrigin;

		//座標の補正
		pVtx[i].pos += D3DXVECTOR3(posx*MAX_SIZEMESH, 0.0f, posz * MAX_SIZEMESH);


		// 各頂点の法線の設定(※ベクトルの大きさは1にする必要がある)
		pVtx[i].nor = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		// 頂点カラーの設定
		pVtx[i].col = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);

		// テクスチャ座標の設定
		pVtx[i].tex = D3DXVECTOR2(texU, texV);
	}

	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
}

//------------------------------------
//法線とIndexの成立
//------------------------------------
void CMesh::SetVtxMeshLight()
{
	VERTEX_3D* pVtx = NULL;

	WORD* pIdx;
	//インデックスバッファ＆頂点バッファのロック
	m_pVtxBuff->Lock(0, 0, (void**)&pVtx, 0);
	m_pIdxBuff->Lock(0, 0, (void**)&pIdx, 0);

	for (int z = 0; z < m_zsiz; z++)
	{
		int linetop = z * (m_X * 2 + 2);
		for (int x = 0; x < m_X; x++)
		{
			int nIdxData = x * 2 + linetop;
			pIdx[nIdxData + 1] = (WORD)(x + (z * m_X));
			pIdx[nIdxData] = (WORD)(pIdx[nIdxData + 1] + m_X);
		}
		//縮退ポリゴン設定
		if (z < m_xsiz - 1)
		{
			pIdx[m_X * 2 + 0 + linetop] = (WORD)(m_xsiz + m_X*z);
			pIdx[m_X * 2 + 1 + linetop] = (WORD)(m_X * 2 + m_X * z);
		}
	}

	//---------------------------------------
	//ここから法線
	//---------------------------------------

	//三角の頂点数
	const int nTri = 3;

	D3DXVECTOR3 posLineVec[nTri];//ベクトル

	for (int nCnt = 0; nCnt < m_por; nCnt++) // プリミティブの数だけまわす。
	{
		//ベクトルを求める
		posLineVec[0] = pVtx[pIdx[nCnt + 0]].pos;
		posLineVec[1] = pVtx[pIdx[nCnt + 1]].pos;
		posLineVec[2] = pVtx[pIdx[nCnt + 2]].pos;

		if ((pIdx[nCnt + 0] == pIdx[nCnt + 1]) ||
			(pIdx[nCnt + 0] == pIdx[nCnt + 2]) ||
			(pIdx[nCnt + 2] == pIdx[nCnt + 1]))
		{
			continue;
		}

		D3DXVECTOR3 V1 = posLineVec[1] - posLineVec[0];
		D3DXVECTOR3 V2 = posLineVec[2] - posLineVec[0];

		D3DXVECTOR3 Normal;

		if (nCnt % 2 == 0)
		{
			//AとBの法線を求めるやつ
			D3DXVec3Cross(&Normal, &V1, &V2);
		}
		else
		{
			//BとAの法線を求めるやつ
			D3DXVec3Cross(&Normal, &V2, &V1);
		}

		//Normalをノーマライズして、長さ 1にする。
		D3DXVec3Normalize(&Normal, &Normal);

		for (int i = 0; i < nTri; i++)
		{//法線計算
			pVtx[pIdx[nCnt + i]].nor += Normal;
		}
	}

	for (int nCnt = 0; nCnt < m_nVtx; nCnt++)
	{
		//norをノーマライズして、長さ 1にする。
		D3DXVec3Normalize(&pVtx[nCnt].nor, &pVtx[nCnt].nor);
	}

	// 頂点座標をアンロック
	m_pVtxBuff->Unlock();
	m_pIdxBuff->Unlock();
}

//--------------------------------------------------
// メッシュのサイズ設定
//--------------------------------------------------
void CMesh::SetMesh(int Size)
{
	SetVtxMeshSize(Size);
	SetVtxMeshLight();
}


//--------------------------------------------------
// テクスチャの設定
//--------------------------------------------------
void CMesh::SetTexture(const char * pFileName)
{
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();
	if (m_pTextureEmesh != NULL)
	{
		m_pTextureEmesh->Release();
		m_pTextureEmesh = NULL;
	}

	//テクスチャの読み込み
	D3DXCreateTextureFromFile(pDevice,
		pFileName,
		&m_pTextureEmesh);

	m_pFileName = pFileName;
}

