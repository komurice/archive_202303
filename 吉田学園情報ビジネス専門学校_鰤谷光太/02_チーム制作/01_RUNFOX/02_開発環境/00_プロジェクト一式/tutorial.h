//--------------------------------------------------------------------------
//
// 制作 ( チュートリアル )
// Author::TAKANO MINTO()->hamada ryuuga();
//
//--------------------------------------------------------------------------

#ifndef _TUTORIAL_H_
#define _TUTORIAL_H_

//-----------------------------
//インクルードファイル
//-----------------------------
#include"main.h"
#include "object2d.h"

//-----------------------------
//前方宣言
//-----------------------------
class CTexture;
class CSound;

//-----------------------------
//クラス定義
//　チュートリアルのクラス
//-----------------------------
class CTutorial :public CObject
{
public:
	CTutorial(int nPriority = 1);		// コンストラクタ
	~CTutorial();						// デストラクタ
	HRESULT Init() override;			// 初期化
	void Uninit() override;				// 終了
	void Update() override;				// 更新
	void Draw() override;				// 描画
	static CTutorial*Create();			// チュートリアルの生成

private:
	void SetPoligon();						// 白いポリゴンの配置
	void SetLogo();							// ロゴの配置
	void SwitchTutorial();					// チュートリアル画面の変更(不必要にだったら消してヨシ！)

private:
	CObject2D* m_pObj2D = {};				// オブジェクトツーデーのポインタ
	int m_nSwitch;								// スイッチ制御用の変数。もろちん、不必要なら消してヨシ！
};

#endif