//============================
//
// ゲーム画面のヘッダー
// Author:hamada ryuuga
//
//============================
#ifndef _GAME_H_		//このマクロが定義されてなかったら
#define _GAME_H_		//2重インクルード防止のマクロ定義

#include "object.h"

class CMeshField;
class CPlayer3D;
class CStage;
class CSound;
class CTimer;
class CCountdown;

class CGame :public CObject
{
public:
	explicit CGame(int nPriority  = 1);
	~CGame();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CGame*Create();

	// プレイヤーの情報の取得
	static CPlayer3D* GetPlayer() { return m_pPlayer; }
	static CMeshField* GetMeshField() { return m_pMeshField; }
	static CStage* GetStage() { return m_pStage; }
	static CTimer* GetTimer() { return m_pTimer; }
	static CCountdown* GetCountdown() { return m_pCountdown; }

private:
	static CPlayer3D *m_pPlayer;
	static CMeshField *m_pMeshField;
	static CStage*m_pStage;
	static CTimer* m_pTimer;
	static CCountdown *m_pCountdown;
};
#endif