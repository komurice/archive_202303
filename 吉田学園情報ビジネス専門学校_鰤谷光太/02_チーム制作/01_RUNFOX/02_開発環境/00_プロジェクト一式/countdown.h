//==================================================
// countdown.h
// Author: tutida ryousei
//==================================================
#ifndef _COUNTDOWN_H_
#define	_COUNTDOWN_H_

#include"object.h"

class CNumber;
class CObject2D;

class CCountdown : public CObject
{
public:
	explicit CCountdown(int nPriority = PRIORITY_COUNTDOWN);
	~CCountdown();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CCountdown *Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int time);

	void SetCountSize();		// サイズの設定
	void SetCountCol();			// 色の設定
	void SetPolygonAlpha();		// ポリゴンのアルファ値の設定

	// セッター
	void SetPos(D3DXVECTOR3 pos) { m_Pos = pos; }
	void SetInitSize(D3DXVECTOR3 initsize) { m_InitSize = initsize; }
	void SetSize(D3DXVECTOR3 size) { m_Size = size; }
	void SetTimer(int time) { m_nTime = time; }

private:
	int m_nInitTime;			// 最初の時間
	int m_nTime;				// 時間
	int m_nTimeOld;				// 過去の時間
	int m_nFrameCount;			// フレームカウント
	int m_nCountPolygon;		// ポリゴンの数
	float m_fAlpha;				// アルファ値
	bool m_bStart;				// スタートしたか
	D3DXVECTOR3 m_Pos;			// 数字の位置
	D3DXVECTOR3 m_PolygonPos;	// ポリゴンの位置
	D3DXVECTOR3 m_Size;			// サイズ
	D3DXVECTOR3 m_InitSize;		// 最初のサイズ
	D3DXCOLOR m_Col;			// 色
	CNumber *m_pNumber;			// 数字の情報

	// ポリゴンの配列
	static const int m_nNumPolygon = 15;	// ポリゴンの数
	float m_fPolygonAlpha[m_nNumPolygon];	// アルファ値
	bool m_bAlpha[m_nNumPolygon];			// アルファ値を変更するか
	CObject2D *m_pObject2D[m_nNumPolygon];	// オブジェクト2Dの情報
};

#endif // !_COUNTDOWN_H_