//============================
//
// ゲーム画面
// Author:hamada ryuuga
//
//============================

//------------------------
// インクルード
//------------------------
#include "game.h"
#include "input.h"
#include "manager.h"
#include "light.h"
#include "stage.h"
#include "model.h"
#include "ball.h"
#include "sound.h"

#include "object.h"
#include "object2D.h"
#include "object3D.h"
#include "model.h"

#include "player3D.h"
#include "meshfield.h"

#include "wood_spawn.h"
#include "stage.h"
#include "pendulum.h"
#include "ball.h"
#include "manager.h"
#include "input_keyboard.h"
#include "font.h"
#include "words.h"
#include "fade.h"
#include "timer.h"
#include"countdown.h"
#include "silhouette.h"

CMeshField *CGame::m_pMeshField = nullptr;
CPlayer3D *CGame::m_pPlayer = nullptr;
CStage* CGame::m_pStage;
CTimer* CGame::m_pTimer = nullptr;
CCountdown* CGame::m_pCountdown = nullptr;



//========================
// コンストラクター
//========================
CGame::CGame(int nPriority /* =1 */) : CObject(nPriority)
{
}
//========================
// デストラクト
//========================
CGame::~CGame()
{
}

//========================
// ゲームの初期化処理
//========================
HRESULT CGame::Init(void)
{
	m_pStage = CStage::Create();

	CSilhoette::Create(
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH * 0.5f, (float)CManager::SCREEN_HEIGHT * 0.5f, 0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH, (float)CManager::SCREEN_HEIGHT, 0.0f),
		D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));

	m_pTimer = CTimer::Create({ 1140.0f,100.0f,0.0f }, { 40.0f,40.0f,0.0f }, 60);

	m_pCountdown = CCountdown::Create({ 640, 360, 0.0f }, { 30.0f,30.0f,0.0f }, 3);

	CManager::GetSound()->Play(CSound::SOUND_BGM_GAME);
	//音量の設定/*第一引数にサウンドの種類、第二引数に設定したい音量を小数点の値で入れる*/
	CManager::GetSound()->SetVolume(CSound::SOUND_BGM_GAME,0.4f);

	return S_OK;
}

//========================
// ゲームの終了処理
//========================
void CGame::Uninit(void)
{
	CManager::GetSound()->Stop(CSound::SOUND_BGM_GAME);

	DeletedObj();
}

//========================
// ゲームの更新処理
//========================
void CGame::Update(void)
{
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	/*if (pInputKeyoard->GetTrigger(DIK_BACKSPACE))
	{
		CManager::GetFade()->NextMode(CManager::MODE_RESULT);
	}*/

	if (m_pTimer->GetTime() == 10)
	{
		CManager::GetSound()->Play(CSound::SOUND_SE_WARNING);
		CManager::GetSound()->UpVolume(CSound::SOUND_SE_WARNING,0.7f);
	}
}

//========================
// ゲームの描画処理
//========================
void CGame::Draw(void)
{
	
}

//==================
//クリエイト
//==================
CGame*CGame::Create()
{
	CGame *pGame = nullptr;

	pGame = new CGame;

	if (pGame != nullptr)
	{
		pGame->Init();
		pGame->SetType(TYPE_MODE);
	}

	return pGame;
}