//**************************************************
// texture.h
// Author  : katsuki mizuki
//**************************************************
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

//==================================================
// インクルード
//==================================================
#include <d3dx9.h>

//==================================================
// 定義
//==================================================
class CTexture
{
public: /* 定義 */
	enum TEXTURE
	{
		TEXTURE_PLAYER = 0,							// プレイヤー
		TEXTURE_GROUND,								// 地面
		TEXTURE_SKY,								// 空
		TEXTURE_SHADOW,								// 影
		TEXTURE_NUMBER,								// 数字
		TEXTURE_TIME,								// 時計
		TEXTURE_START,								// スタート
		TEXTURE_PAUSE,								// ポーズ
		TEXTURE_PAUSE1,								// ポーズ１
		TEXTURE_TITLE,								// タイトルで使用するテクスチャ
		TEXTURE_TITLE_F,							// F
		TEXTURE_TITLE_O,							// O
		TEXTURE_TITLE_X,							// X
		TEXTURE_TITLE_R,							// R
		TEXTURE_TITLE_U,							// U
		TEXTURE_TITLE_N,							// N
		TEXTURE_RESULT,								// リザルト
		TEXTURE_GOAL,								// ゴール
		TEXTURE_TIMEUP,								// タイムアップ
		TEXTURE_TITLELOGO,							// タイトルで使用するロゴ
		TEXTURE_TITLEFOX,							// タイトルにいるゴン
		TEXTURE_TITLE_GAME,							// タイトルで使用するゲームスタート選択バーテクスチャ
		TEXTURE_TITLE_TUTORIAL,						// タイトルで使用するチュートリアル選択バーテクスチャ
		TEXTURE_TITLE_RANKING,						// タイトルで使用するランキング選択バーテクスチャ
		TEXTURE_TITLE_EXIT,							// タイトルで使用する終了選択バーテクスチャ
		TEXTURE_TITLE_FRAME,						// タイトルで使用する選択バーフレーム
		TEXTURE_RESULT_TITLE,						// リザルトで使用するロゴ	
		TEXTURE_RESULT_RANKING,						// リザルトで使用するランキングのダミーor枠線(になるかも？)	
		TEXTURE_TUTORIAL_LOGO,						// チュートリアルで使用するロゴ	
		TEXTURE_TUTORIAL_RETURN,					// チュートリアルで使用するロゴ	
		TEXTURE_TUTORIAL_DESCRIPTION,				// チュートリアルで使用する説明tutorialland
		TEXTURE_TUTORIAL_LAND,						// チュートリアル最初
		TEXTURE_TUTORIAL_LANDMk2,					// チュートリアル本編１
		TEXTURE_TUTORIAL_LANDMk3,					// チュートリアル本編２
		TEXTURE_TUTORIAL_LANDMk4,					// チュートリアル本編３
		TEXTURE_NAMESET_BG,							// ネームセット背景
		TEXTURE_CHECKPOINT,							// チェックポイント
		TEXTURE_ALPHABET,
		TEXTURE_MAX,
		TEXTURE_NONE,								// 使用しない
	};

	static const char* s_FileName[];	// ファイルパス

public:
	CTexture();		// デフォルトコンストラクタ
	~CTexture();	// デストラクタ

public: /* メンバ関数 */
	void LoadAll();										// 全ての読み込み
	void Load(TEXTURE inTexture);						// 指定の読み込み
	void ReleaseAll();									// 全ての破棄
	void Release(TEXTURE inTexture);					// 指定の破棄
	LPDIRECT3DTEXTURE9 GetTexture(TEXTURE inTexture);	// 情報の取得

private: /* メンバ変数 */
	LPDIRECT3DTEXTURE9 s_pTexture[TEXTURE_MAX];	// テクスチャの情報
};

#endif // !_TEXTURE_H_
