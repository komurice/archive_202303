//============================
//
// フェード設定
// Author:hamada ryuuga
//
//============================
#include <stdio.h>
#include <assert.h>
#include "fade.h"
#include "font.h"
#include "words.h"
#include "fade_model.h"

//=============================================================================
// コンストラクタ
//=============================================================================
CFade::CFade(int nPriority) : CObject2D(nPriority)
{
}

//=============================================================================
// デストラクタ
//=============================================================================
CFade::~CFade()
{
}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CFade::Init(void)
{
	// 現在のモーション番号の保管
	CObject2D::Init();
	m_FadeSp = 0.04f;
	m_FadeSet = 0.0f;

	//CFadeModel::Create();

	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CFade::Uninit(void)
{
	// 現在のモーション番号の保管
	CObject2D::Uninit();
	Release();
}

//=============================================================================
// 更新
//=============================================================================
void CFade::Update(void)
{
	if (fade != FADENON)
	{
		// 現在のモーション番号の保管
		CObject2D::Update();
		if (fade == FADEOUT)
		{
			m_FadeSet -= m_FadeSp;
		}
		if (fade == FADEIN)
		{
			m_FadeSet += m_FadeSp;
		}

		if (m_FadeSet >= 1.0f)
		{
			fade = FADEOUT;
			m_FadeSet = 1.0f;
			CManager::SetMode(m_NextMode);

		}
		if (m_FadeSet <= 0.0f)
		{
			fade = FADENON;

			m_FadeSet = 0.0f;

			return;
		}
		SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, m_FadeSet));
	}
}

//=============================================================================
// 描画
//=============================================================================
void CFade::Draw(void)
{	
	//// デバイスの取得
	//LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	//// ステンシルバッファ -> 有効
	//pDevice->SetRenderState(D3DRS_STENCILENABLE, TRUE);

	//// ステンシルバッファと比較する参照値設定 -> ref
	//pDevice->SetRenderState(D3DRS_STENCILREF, 0x10);

	//// ステンシルバッファの値に対してのマスク設定 -> 0xff(全て真)
	//pDevice->SetRenderState(D3DRS_STENCILMASK, 0xff);

	//// ステンシルテストの比較方法 ->
	//// （参照値 >= ステンシルバッファの参照値）なら合格
	//pDevice->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_GREATER);

	//// ステンシルテストの結果に対しての反映設定
	//pDevice->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_REPLACE);		// Zとステンシル成功
	//pDevice->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);			// Zとステンシル失敗
	//pDevice->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);			// Zのみ失敗

	CObject2D::Draw();

	//// ステンシルバッファ -> 無効
	//pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);
}

//=============================================================================
// create
//=============================================================================
CFade* CFade::Create()
{
	CFade * pObject = nullptr;
	pObject = new CFade;

	if (pObject != nullptr)
	{
		pObject->Init();
		pObject->m_NextMode = CManager::MODE_TITLE;
		pObject->SetSize(D3DXVECTOR3(1280.0f, 720.0f, 0.0f));
		pObject->SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
		pObject->SetType(CObject::TYPE_MODE);
		pObject->fade = FADENON;
	}
	return pObject;
}


//=============================================================================
// 次のモードにいくね
//=============================================================================
void CFade::NextMode(CManager::MODE nextMode)
{
	if (fade == FADENON)
	{
		Init();
		m_NextMode = nextMode;
		SetPos(D3DXVECTOR3(640.0f, 360.0f, 0.0f));
		SetSize(D3DXVECTOR3(1280.0f, 720.0f, 0.0f));
		SetCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f));
		m_FadeSet = 0.0f;
		fade = FADEIN;
	}
}
