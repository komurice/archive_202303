//==================================================
// player3D.cpp
// Author: Buriya Kota
//==================================================

//**************************************************
// include
//**************************************************
#include <assert.h>

#include "utility.h"

#include "manager.h"
#include "camera.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "debug_proc.h"

#include "object3D.h"
#include "player3D.h"
#include "shadow.h"
#include "meshfield.h"
#include "model.h"
#include "game.h"
#include "stage.h"
#include "building.h"

//**************************************************
// 静的メンバ変数
//**************************************************
const float CPlayer3D::PLAYER_SPEED = 15.0f;

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPlayer3D::CPlayer3D()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPlayer3D::~CPlayer3D()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CPlayer3D::Init()
{
	LoadSetFile("data/TEXT/motion.txt");

	m_nCurrentKey = 0;

	m_pos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(50.0f, 50.0f, 50.0f);

	// モデルの生成
	m_pModel[0] = CModel::Create(CModel::MODEL_TYPE_PLAYER);
	m_pModel[1] = CModel::Create(CModel::MODEL_TYPE_PLAYER);
	m_pModel[2] = CModel::Create(CModel::MODEL_TYPE_PLAYER);
	m_pModel[3] = CModel::Create(CModel::MODEL_TYPE_PLAYER);

	// 親の設定
	m_pModel[0]->SetParent(nullptr);
	m_pModel[1]->SetParent(m_pModel[0]);
	m_pModel[2]->SetParent(m_pModel[0]);
	m_pModel[3]->SetParent(m_pModel[0]);

	m_pModel[0]->SetPos(D3DXVECTOR3(  0.0f,  0.0f, 0.0f));
	m_pModel[1]->SetPos(D3DXVECTOR3(  0.0f, 50.0f, 0.0f));
	m_pModel[2]->SetPos(D3DXVECTOR3( 50.0f,  0.0f, 0.0f));
	m_pModel[3]->SetPos(D3DXVECTOR3(-50.0f,  0.0f, 0.0f));

	m_pModel[1]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[2]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));
	m_pModel[3]->SetScale(D3DXVECTOR3(1.0f, 1.0f, 1.0f));

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CPlayer3D::Uninit()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Uninit();
		delete m_pModel[nCnt];
		m_pModel[nCnt] = nullptr;
	}

	// deleteフラグを立てる
	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPlayer3D::Update()
{
	m_posOld = m_pos;
	SetPosOld(m_posOld);

	// プレイヤーの動き
	Control_();
	// モーション
	Motion_();

	// メッシュとの当たり判定
	CMeshField *pMeshField = CGame::GetMeshField();
	pMeshField->CMeshField::CollisionMesh(&m_pos);

	SetPos(m_pos);

#ifdef _DEBUG
	// デバッグ表示
	CDebugProc::Print("プレイヤーの現在の角度 : %f\n", m_rot.y);
	CDebugProc::Print("プレイヤーの現在の高さ : %f\n", m_pos.y);
#endif // DEBUG

}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CPlayer3D::Draw()
{
	// デバイスの取得
	LPDIRECT3DDEVICE9 pDevice = CManager::GetRenderer()->GetDevice();

	// 計算用マトリックス
	D3DXMATRIX mtxRot, mtxTrans;

	// ワールドマトリックスの初期化
	D3DXMatrixIdentity(&m_mtx);

	// 向きを反映
	D3DXMatrixRotationYawPitchRoll(&mtxRot, m_rot.y, m_rot.x, m_rot.z);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxRot);

	// 位置を反映
	D3DXMatrixTranslation(&mtxTrans, m_pos.x, m_pos.y, m_pos.z);
	D3DXMatrixMultiply(&m_mtx, &m_mtx, &mtxTrans);

	// ワールドマトリックスの設定
	pDevice->SetTransform(D3DTS_WORLD, &m_mtx);

	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		m_pModel[nCnt]->Draw();
	}
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPlayer3D* CPlayer3D::Create(D3DXVECTOR3 pos)
{
	CPlayer3D *pPlayer3D = nullptr;
	pPlayer3D = new CPlayer3D;

	if (pPlayer3D != nullptr)
	{
		pPlayer3D->Init();
		pPlayer3D->SetPos(pos);
	}
	else
	{
		assert(false);
	}

	return pPlayer3D;
}

//--------------------------------------------------
// 現在の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer3D::RotNormalization(D3DXVECTOR3 rot)
{
	m_rot = rot;

	// 現在の角度の正規化
	if (m_rot.y > D3DX_PI)
	{
		m_rot.y -= D3DX_PI * 2.0f;
	}
	else if (m_rot.y < -D3DX_PI)
	{
		m_rot.y += D3DX_PI * 2.0f;
	}

	return m_rot;
}

//--------------------------------------------------
// 目的の角度の正規化
//--------------------------------------------------
D3DXVECTOR3 CPlayer3D::RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest)
{
	m_rot = rot;
	m_rotDest = rotDest;

	// 目的の角度の正規化
	if (m_rotDest.y - m_rot.y > D3DX_PI)
	{
		m_rotDest.y -= D3DX_PI * 2.0f;
	}
	else if (m_rotDest.y - m_rot.y < -D3DX_PI)
	{
		m_rotDest.y += D3DX_PI * 2.0f;
	}

	return m_rotDest;
}

//--------------------------------------------------
// 操作
//--------------------------------------------------
void CPlayer3D::Control_()
{
	// インプット
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	// ジョイパッドの入力
	vec.x = pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).x;
	vec.z = -pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).y;

	D3DXVECTOR3 rotDest = GetRotDest();

	if ((vec.x == 0.0f) && (vec.z == 0.0f))
	{
		if (pInputKeyoard->GetPress(DIK_A))
		{// 左
			vec.x += -1.0f;
			rotDest.y = (D3DX_PI * 0.5f);
		}
		if (pInputKeyoard->GetPress(DIK_D))
		{// 右
			vec.x += 1.0f;
			rotDest.y = (-D3DX_PI * 0.5f);
		}
		if (pInputKeyoard->GetPress(DIK_W))
		{// 上
			vec.z += 1.0f;
			rotDest.y = D3DX_PI;
		}
		if (pInputKeyoard->GetPress(DIK_S))
		{// 下
			vec.z += -1.0f;
			rotDest.y = 0.0f;
		}

		SetRotDest(rotDest);
	}

	D3DXVECTOR3 rot = GetRot();

	// 目的の角度の正規化
	rotDest = RotDestNormalization(rot, rotDest);

	// 目的の角度を現在の角度に近づける
	rot.y += (rotDest.y - rot.y) * 0.2f;

	// 現在の角度の正規化
	rot = RotNormalization(rot);

	SetRot(rot);

	D3DXVECTOR3 pos = GetPos();

	if (vec.x != 0.0f || vec.z != 0.0f)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		pos += vec * PLAYER_SPEED;

		SetPos(pos);
	}
}

//--------------------------------------------------
// モーション
//--------------------------------------------------
void CPlayer3D::Motion_()
{
	for (int nCnt = 0; nCnt < MAX_PARTS; nCnt++)
	{
		if (m_pModel[nCnt] == nullptr)
		{
			continue;
		}

		int nNextNumber = m_nCurrentKey + 1;

		// 差分
		D3DXVECTOR3 difPos = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].pos - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos;
		D3DXVECTOR3 difRot = m_ModelData[0].KeySet[nNextNumber].aKey[nCnt].rot - m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot;

		// 相対値
		float nRelativeValue = (float)m_nCountMotion / (float)m_ModelData[0].KeySet[m_nCurrentKey].nFrame;

		// 現在地
		D3DXVECTOR3 currentPos = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].pos + (difPos * nRelativeValue);
		D3DXVECTOR3 currentRot = m_ModelData[0].KeySet[m_nCurrentKey].aKey[nCnt].rot + (difRot * nRelativeValue);

		m_pModel[nCnt]->SetPos(currentPos);
		m_pModel[nCnt]->SetRot(currentRot);
	}

	// モーションカウンターを進める
	m_nCountMotion++;

	if (m_nCountMotion >= m_ModelData[0].KeySet[m_nCurrentKey].nFrame)
	{
		m_nCurrentKey++;
		m_nCountMotion = 0;

		if (m_nCurrentKey >= m_nNumKey)
		{
			m_nCurrentKey = 0;
		}
	}
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer3D::LoadSetFile(char *Filename)
{
	m_nSetCurrentMotion = 0;
	char modelFile[256];
	char String[256];
	// ファイルポインタの宣言
	FILE* pFile;	

	//ファイルを開く
	pFile = fopen(Filename, "r");

	if (pFile != NULL)
	{//ファイルが開いた場合
		fscanf(pFile, "%s", &String);

		while (strncmp(&String[0], "SCRIPT", 6) != 0)
		{//スタート来るまで空白読み込む
			String[0] = {};
			fscanf(pFile, "%s", &String[0]);
		}
		D3DXVECTOR3	s_modelMainpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		while (strncmp(&String[0], "END_SCRIPT", 10) != 0)
		{// 文字列の初期化と読み込み// 文字列の初期化と読み込み
			fscanf(pFile, "%s", &String[0]);

			if (strcmp(&String[0], "MODEL_FILENAME") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &modelFile);
			}
			if (strcmp(&String[0], "MAINPOS") == 0)
			{// 文字列が一致した場合
				fscanf(pFile, "%s", &String[0]);//＝読み込むやつ
				fscanf(pFile, "%f", &s_modelMainpos.x);
				fscanf(pFile, "%f", &s_modelMainpos.y);
				fscanf(pFile, "%f", &s_modelMainpos.z);
			}
			else if ((strcmp(&String[0], "MODELSET") == 0) || (strcmp(&String[0], "MOTIONSET") == 0))
			{// 文字列が一致した場合
				D3DXVECTOR3	s_modelpos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
				D3DXVECTOR3	s_modelrot = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

				while (1)
				{
					// 文字列の初期化と読み込み
					String[0] = {};
					fscanf(pFile, "%s", &String[0]);

					if (strncmp(&String[0], "#", 1) == 0)
					{//これのあとコメント
						fgets(&String[0], sizeof(String), pFile);
						continue;
					}

					if (strcmp(&String[0], "POS") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);//＝読み込むやつ
						fscanf(pFile, "%f", &s_modelpos.x);
						fscanf(pFile, "%f", &s_modelpos.y);
						fscanf(pFile, "%f", &s_modelpos.z);
					}

					if (strcmp(&String[0], "ROT") == 0)
					{// 文字列が一致した場合
						fscanf(pFile, "%s", &String[0]);//＝読み込むやつ
						fscanf(pFile, "%f", &s_modelrot.x);
						fscanf(pFile, "%f", &s_modelrot.y);
						fscanf(pFile, "%f", &s_modelrot.z);
					}
				
					if (strcmp(&String[0], "LOOP") == 0)
					{// 文字列が一致した場合//ループするかしないか1する０しない
						fscanf(pFile, "%s", &String[0]);//＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].LOOP);
					}

					if (strcmp(&String[0], "NUM_KEY") == 0)
					{// 文字列が一致した場合//キーの最大数
						fscanf(pFile, "%s", &String[0]);//＝読み込むやつ
						fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].NUM_KEY);
					}

					if (strcmp(&String[0], "KEYSET") == 0)
					{// 文字列が一致した場合//アニメーションのファイルだったとき
						LoadKeySetFile(pFile);
					}

					if (strcmp(&String[0], "END_MOTIONSET") == 0)
					{//一回のmotion読み込み切ったら次のmotionのセットに行くためにカウント初期化してデータを加算する
						m_nSetModel = 0;
						m_nSetCurrentMotion++;
					}
	
					if (strcmp(&String[0], "END_SCRIPT") == 0)
					{// 文字列が一致した場合
						break;
					}
				}
			}
		}
	}

	//ファイルを閉じる
	fclose(pFile);
}

//---------------------------------------------------------------------------
// ファイル読み込み処理
//---------------------------------------------------------------------------
void CPlayer3D::LoadKeySetFile(FILE * pFile)
{
	char String[256];
	int nSetKey = 0;

	while (1)
	{
		// 文字列の初期化と読み込み
		fscanf(pFile, "%s", &String[0]);

		if (strncmp(&String[0], "#", 1) == 0)
		{//コメント対策
			fgets(&String[0], sizeof(String), pFile);
			continue;
		}

		if (strcmp(&String[0], "FRAME") == 0)
		{// 文字列が一致した場合
			fscanf(pFile, "%s", &String[0]);//イコール読み込む
			fscanf(pFile, "%d", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].nFrame);
		}

		if (strcmp(&String[0], "KEY") == 0)
		{
			while (1)
			{// 文字列の初期化と読み込み
				String[0] = {};
				fscanf(pFile, "%s", &String[0]);
				if (strncmp(&String[0], "#", 1) == 0)
				{
					fgets(&String[0], sizeof(String), pFile);//コメント読み込む
					continue;
				}

				if (strcmp(&String[0], "POS") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].pos.z);
				}

				if (strcmp(&String[0], "ROT") == 0)
				{// 文字列が一致した場合
					fscanf(pFile, "%s", &String[0]);//イコール読み込む
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.x);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.y);
					fscanf(pFile, "%f", &m_ModelData[m_nSetCurrentMotion].KeySet[m_nSetModel].aKey[nSetKey].rot.z);
				}

				if (strcmp(&String[0], "END_KEY") == 0)
				{
					nSetKey++;//パーツの数がどんどん増える
					break;
				}
			}
		}

		if (strcmp(&String[0], "END_KEYSET") == 0)
		{// 文字列が一致した場合
			m_nSetModel++;//現在のセットしてる番号の更新
			nSetKey = 0;//パーツの数初期化
			break;
		}
	}
}
