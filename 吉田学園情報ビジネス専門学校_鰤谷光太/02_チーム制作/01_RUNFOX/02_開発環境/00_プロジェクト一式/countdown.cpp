//==================================================
// countdown.cpp
// Author: tutida ryousei
//==================================================
#include"countdown.h"
#include"number.h"
#include"timer.h"
#include"debug_proc.h"
#include"manager.h"
#include"fade.h"
#include"sound.h"
#include"stage.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CCountdown::CCountdown(int nPriority /* =6 */) : CObject(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CCountdown::~CCountdown()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CCountdown::Init()
{
	m_pNumber = CNumber::Create({ m_Pos.x, m_Pos.y, 0.0f }, m_Size);
	m_Size = m_InitSize;
	m_nInitTime = m_nTime;
	m_fAlpha = 0.0f;
	m_PolygonPos = m_Pos;

	float one = 1.0f;

	for (int nCnt = 0; nCnt < m_nNumPolygon; nCnt++)
	{
		one -= 1 / (float)m_nNumPolygon;

		// ポリゴンの位置
		m_PolygonPos.x -= sinf(D3DX_PI * 2 * one) * 70;
		m_PolygonPos.y -= cosf(D3DX_PI * 2 * one) * 70;

		// ポリゴンの配置
		m_pObject2D[nCnt] = CObject2D::Create({ m_PolygonPos.x,m_PolygonPos.y,0.0f }, { 10.0f,10.0f,0.0f });
		m_PolygonPos = m_Pos;

		// ポリゴンのアルファ値の設定
		m_fPolygonAlpha[nCnt] = 1.0f;
		m_pObject2D[nCnt]->SetCol({ 1.0f,0.0f,0.5f,m_fPolygonAlpha[nCnt] });
	}

	m_bAlpha[m_nCountPolygon] = true;
	CStage::SetCountdown(false);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CCountdown::Uninit()
{
	if (m_pNumber != nullptr)
	{
		m_pNumber->Uninit();
		m_pNumber = nullptr;
	}

	CObject::DeletedObj();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CCountdown::Update()
{
	CFade::FADE *pFade = CManager::GetFade()->GetFade();
	
	// フェードが終わってからカウントダウンを始める
	if (*pFade == CFade::FADENON)
	{
		if (m_pNumber != nullptr)
		{
			m_pNumber->Update();
		}

		m_nFrameCount++;
		m_nTimeOld = m_nTime;

		if (m_nFrameCount % 60 == 0)
		{
			m_nTime--;
			CManager::GetSound()->Play(CSound::SOUND_SE_DECISION);
		}

		SetCountSize();			// サイズの設定
		SetCountCol();			// 数字の色の設定
		SetPolygonAlpha();		// ポリゴンのアルファ値の設定

		if (m_nTime == 0)
		{
			// テクスチャの変更
			m_pNumber->AnimTexture(1, 1);
			const CTexture::TEXTURE Texture = CTexture::TEXTURE_START;
			m_pNumber->SetNumTexture(Texture);

			CManager::GetSound()->Play(CSound::SOUND_SE_CANCEL);
			if (!m_bStart)
			{
				m_Size = m_InitSize = { 250.0f,40.0f,0.0f };
				m_bStart = true;
			}

			CStage::SetCountdown(true);
		}
		else if (m_nTime < 0)
		{
			for (int nCnt = 0; nCnt < m_nNumPolygon; nCnt++)
			{
				m_pObject2D[nCnt]->Uninit();
			}
			Uninit();
		}
		else
		{
			// テクスチャ座標の設定
			m_pNumber->AnimTexture(m_nTime, 10);
		}
	}
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CCountdown::Draw()
{
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CCountdown *CCountdown::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int time)
{
	CCountdown *pCountdown = nullptr;

	pCountdown = new CCountdown;

	if (pCountdown != nullptr)
	{
		pCountdown->SetPos(pos);
		pCountdown->SetInitSize(size);
		pCountdown->SetTimer(time);
		pCountdown->Init();
	}

	return pCountdown;
}

//--------------------------------------------------
// サイズの設定
//--------------------------------------------------
void CCountdown::SetCountSize()
{
	if (m_Size.x <= m_InitSize.x + 30 && m_Size.y <= m_InitSize.x + 30)
	{// 目的のサイズまで大きくする
		m_Size.x++;
		m_Size.y++;
	}
	else if (m_nTimeOld > m_nTime)
	{
		m_Size = m_InitSize;
	}

	m_pNumber->SetNumSize(m_Size);
}

//--------------------------------------------------
// 色の設定
//--------------------------------------------------
void CCountdown::SetCountCol()
{
	if (m_Size.x <= m_InitSize.x + 30 && m_Size.y <= m_InitSize.x + 30)
	{// アルファ値の加算
		m_fAlpha += 0.05f;
	}
	else
	{// アルファ値の減算
		m_fAlpha -= 0.05f;
	}

	if (m_nTimeOld > m_nTime)
	{
		m_fAlpha = 0.0f;
	}

	// アルファ値の設定
	if (m_nTime != 0)
	{
		m_pNumber->SetNumCol(&D3DXCOLOR(1.0f, 0.0f, 0.5f, m_fAlpha));
	}
	else
	{
		m_pNumber->SetNumCol(&D3DXCOLOR(0.0f, 1.0f, 1.0f, m_fAlpha));
	}
}

//--------------------------------------------------
// ポリゴンの設定
//--------------------------------------------------
void CCountdown::SetPolygonAlpha()
{
	// アルファ値の減少量
	float sub = (float)m_nNumPolygon / 60.0f / m_nInitTime;

	if (m_bAlpha[m_nCountPolygon])
	{
		m_fPolygonAlpha[m_nCountPolygon] -= sub;

		if (m_fPolygonAlpha[m_nCountPolygon] <= 0.0f)
		{
			m_bAlpha[m_nCountPolygon] = false;

			m_nCountPolygon++;
			m_bAlpha[m_nCountPolygon] = true;
		}
	}

	if (m_nCountPolygon >= m_nNumPolygon)
	{
		m_nCountPolygon = 0;
	}

	// 色の設定
	m_pObject2D[m_nCountPolygon]->SetCol({ 1.0f,0.0f,0.5f,m_fPolygonAlpha[m_nCountPolygon] });
}