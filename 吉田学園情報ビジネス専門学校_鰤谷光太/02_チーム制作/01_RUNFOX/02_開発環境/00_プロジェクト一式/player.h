//============================
//
// プレイヤー設定ヘッター
// Author:hamada ryuuga
//
//============================
#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "main.h"
#include "motion.h"
#include "renderer.h"
#include "motion_model3D.h"

class CPlayer : public CMotionModel3D
{
public:

	
	//--------------------------------------------------------------------
	// プレイヤーのアクションの列挙型
	//--------------------------------------------------------------------
	enum ACTION_TYPE
	{
		TYPE_NEUTRAL = 0,		// ニュートラル
		TYPE_MOVE,			// 移動
		TYPE_ATTACK,			// 攻撃
		TYPE_MAX,				// 最大数
	};

	//modelデータの構造体//
	struct MODELDATAPLAYER
	{
		int key;		// 時間管理
		int nowKey;		// 今のキー
		int loop;		// ループするかどうか[0:ループしない / 1 : ループする]
		int num_key;  	// キー数
		/*MyKeySet KeySet[MAX_KEY];*/
	};

public:
	static const int MAXLIFE = 300;
	static const float ATTENUATION;		// 減衰係数
	static const float SPEED;			// スピード
	static const float WIDTH;			// モデルの半径
	static const int MAX_PRAYER;		// 最大数
	static const int MAX_MOVE;			// アニメーションの最大数
	static const int INVINCIBLE;		// 無敵時間
	static const int MAX_MODELPARTS = 9;
	static const float MAX_HITMOVESPEED;
	
public:
	explicit CPlayer(int nPriority = PRIORITY_PLAYER);
	~CPlayer();

	HRESULT Init()override;	// 初期化
	void Uninit()override;	// 破棄
	void Update()override;	// 更新
	void Draw()override;	// 描画

	static CPlayer *Create();

	void SetFriction(float Infriction) { m_Friction = Infriction; };
	void breakHit();		// 移動
	void SetChangePoptime(const D3DXVECTOR3 pPos) { ChangePoptime = pPos; }		// 移動

	D3DXVECTOR3 GetMove() { return m_move; }
	void SetMove(float ismove) { m_move.x = ismove; }
	void SetMaxMove(float ismove) { m_MaxSpeed = ismove; }
private:
	
	void Move();		// 移動

	int m_Timer;
	int m_Speed;
	int m_Pow;
	float m_MoveSpeed;
	float m_Friction;
	bool m_jump;
	ACTION_TYPE m_Action;
	float m_MaxSpeed;
	float m_JunpSpd;				//ジャンプスピード
private:
	D3DXVECTOR3 m_rotDest;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_size;
	D3DXVECTOR3 ChangePoptime;
	D3DXVECTOR3 m_move;
	//int m_nCounter;					//SEのカウント用変数
};
#endif
