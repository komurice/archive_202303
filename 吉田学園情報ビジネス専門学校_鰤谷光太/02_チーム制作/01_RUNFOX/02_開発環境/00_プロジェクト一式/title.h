//**************************************************
//
// 制作 ( タイトル )
// Author : hamada ryuuga
//
//**************************************************
#pragma once
#ifndef _TITLE_H_
#define _TITLE_H_

#include"main.h"
#include "object2d.h"

//-----------------------------
//前方宣言
//-----------------------------
class CTexture;

//**************************************************
// 定数定義
//**************************************************
#define MAX_LOGO		(7)

class CTitle :public CObject
{
public:
	//画面(モード)の種類
	enum MODE
	{
		MODE_GAME = 0,			//ゲーム画面	
		MODE_TUTORIAL,
		MODE_NAMESET,		//ランキング画面
		MODE_END,
		MODE_MAX
	};

	CTitle(int nPriority = 1);
	~CTitle();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CTitle*Create();

private:
	void SetBG();
	void SetLogo();
	void SetSelectBar();
	void SetFrame();

private:
	CObject2D *m_Logpobj2d[4];
	CObject2D *m_pTitleLogp[MAX_LOGO];
	int m_NextMode;
	int m_nTime;
	int m_nNum;
	int m_direction;
	int m_nFrame;
};

#endif