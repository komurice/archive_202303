//==================================================
// ball.h
// Author: tutida ryousei
//==================================================
#ifndef _BALL_H_
#define	_BALL_H_

#include"building.h"

class CBuilding;

class CBall : public CBuilding
{
public:
	CBall();
	~CBall();
	
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	void Hit() override;

	static CBall *Create(D3DXVECTOR3 pos, float radius, float rotspeed, int rottype);
	void Move();	// 移動処理

	// セッター
	void SetCenterPos(D3DXVECTOR3 centerpos) { m_CenterPos = centerpos; }
	void SetRadius(float radius) { m_fRadius = radius; }
	void SetRotSpeed(float rotspeed) { m_fRotSpeed = rotspeed; }
	void SetRotType(int type) { m_nRotType = type; }

	// ゲッター
	D3DXVECTOR3 GetDestRot() { return m_Rot; }
	float GetCoefficient() { return m_fRotSpeed; }
	int GetType() { return m_nRotType; }

private:
	D3DXVECTOR3 m_CenterPos;	// 中心の位置
	D3DXVECTOR3 m_Pos;			// オブジェクトの位置
	D3DXVECTOR3 m_Rot;			// 角度
	float m_fRotSpeed;			// 回転速度
	float m_fMoveSpeed;			// 移動速度
	float m_fRadius;			// 半径
	int m_nRotType;				// 右回りか左回りか
};

#endif // !_BALL_H_
