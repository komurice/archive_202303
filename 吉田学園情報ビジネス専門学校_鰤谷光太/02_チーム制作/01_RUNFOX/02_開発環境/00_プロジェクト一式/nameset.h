//**************************************************
//
// 制作 ( 名まえ )
// Author : hamada ryuuga
//
//**************************************************

#ifndef _NAMESET_H_
#define _NAMESET_H_

#include"main.h"
#include "object2d.h"
#include "name.h"

class CNameSet :public CObject
{
public:
	explicit CNameSet(int nPriority = 1);
	~CNameSet();
	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	static CNameSet*Create();

	static std::string GetName();
private:

	void RankingNeme();

	CObject2D *m_object2d[2];

	static std::string m_PlayName;

	CName*m_ListName[3];
	CName*m_PlayNameSet[10];

	D3DXVECTOR3 Collar;
	D3DXVECTOR3 m_NemePos;

	int m_NowPlay;
};

#endif