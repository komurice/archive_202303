//=============================================================================
//
// 建物
// Author : 浜田琉雅
//
//=============================================================================


#ifndef _BUILDING_H_			// このマクロ定義がされてなかったら
#define _BUILDING_H_			// 二重インクルード防止のマクロ定義

#include "renderer.h"
#include "objectX.h"
#include "texture.h"
#include "object2D.h"



class CBuilding : public CObjectX
{
public:

	static CBuilding *CBuilding::Create(const char * pFileName,  D3DXVECTOR3 *pPos);

	explicit CBuilding(int nPriority = PRIORITY_OBJECT);
	~CBuilding() override;

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void SetFileName(const char * pFileName) { m_pFileName = pFileName;}
	const char* GetFileName() { return m_pFileName; }

	void SetGold(bool IsGo) { m_Gold = IsGo; }
	bool GetGold() { return m_Gold; }
	void SetPosOld() { m_PosOld = GetPos(); }

	virtual void Hit();
	bool CollisionModel(D3DXVECTOR3 *pPos, D3DXVECTOR3 *pPosOld, D3DXVECTOR3 *pSize);
	//bool IsCollisionSphere(D3DXVECTOR3 pos, D3DXVECTOR3 size);
	void SetModelNumber(int IsNumber) { m_Number = IsNumber; }
	int GetModelNumber() { return m_Number; }
	int GetPopModelNumber() { return m_NowNumber; }

	bool GetChangePoptime() { return m_ChangePoptime; }
	void SetChangePoptime(bool ChangePoptime) { m_ChangePoptime = ChangePoptime; }
private:
	void move();
	D3DXVECTOR3 m_PosOld;
	D3DXVECTOR3 m_Move;
	const char * m_pFileName;
	bool m_Gold;
	bool m_ChangePoptime;
	int m_Number;
	static int m_NowNumber;
	int m_nCheckCounter;
	CObject2D *m_pObj2D;				// オブジェクトツーデーのポインタ
};

#endif

