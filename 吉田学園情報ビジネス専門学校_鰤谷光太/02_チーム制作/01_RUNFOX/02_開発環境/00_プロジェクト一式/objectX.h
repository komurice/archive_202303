//==================================================
// objectX->h
// Author: Buriya Kota
//==================================================
#ifndef _OBJECTX_H_
#define _OBJECTX_H_

//**************************************************
// インクルード
//**************************************************
#include "main.h"
#include "texture.h"
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CModelX;

//**************************************************
// クラス
//**************************************************
class CObjectX : public CObject
{
public:
	static const int MAX_BILLBOARD = 5;
	static const float BILLBORAD_WIDTH;
	static const float BILLBOARD_HEIGHT;

public:
	explicit CObjectX(int nPriority = 0);
	virtual ~CObjectX() override;

	virtual HRESULT Init() override;
	virtual void Uninit() override;
	virtual void Update() override;
	virtual void Draw() override;

	// セッター
	void SetModel(const char *filename);
	void SetPos(const D3DXVECTOR3& pos) { m_posOrigin = pos; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetMove(const D3DXVECTOR3& move) { m_move = move; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetLife(const int &life) { m_nLife = life; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtxWorld = mtx; }

	// ゲッター
	D3DXVECTOR3 GetMove() { return m_move; }
	//D3DXVECTOR3 *GetPosPointer() { return &m_posOrigin; }
	const D3DXVECTOR3& GetPos() const { return m_posOrigin; }
	const D3DXVECTOR3& GetSize() const { return m_size; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXMATRIX& GetMtxWorld() const { return m_mtxWorld; }

	// 移動量代入
	void MovePos(const D3DXVECTOR3& move) { m_posOrigin += move; }

	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

	D3DXVECTOR3 GetVtxMax() { return m_vtxMaxModel; }
	D3DXVECTOR3 GetVtxMin() { return m_vtxMinModel; }
private:
	
	 CModelX* m_modelXData;

private:
	// 位置
	D3DXVECTOR3 m_posOrigin;
	// 位置
	D3DXVECTOR3 m_move;
	// 向き
	D3DXVECTOR3 m_rot;
	// 向き
	D3DXVECTOR3 m_rotDest;
	// 大きさ
	D3DXVECTOR3 m_size;
	// ワールドマトリックス
	D3DXMATRIX m_mtxWorld;
	// テクスチャの列挙型
	// 頂点バッファへのポインタ
	LPDIRECT3DVERTEXBUFFER9 m_pVtxBuff;

	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;

	// 体力
	int m_nLife;

};


//=============================================================================
// モデルマネージャー　
// Author : 浜田琉雅
// 概要 : モデルのデータを保存しておく
//=============================================================================
class CObjectXManager
{
public: /* 定義 */

	static const int MODEL_MAX = 100;

private:
	static CObjectXManager * ms_ObjectXManager;

public:
	CObjectXManager();		// デフォルトコンストラクタ
	~CObjectXManager();	// デストラクタ

public: /* メンバ関数 */
	static CObjectXManager *GetManager();
	CModelX* LoadXfile(const char *pXFileName);	// 指定の読み込み
	static void ReleaseAll();
public: /* メンバ変数 */
	CModelX* m_modelXList[MODEL_MAX];

};

//=============================================================================
// モデルクラスX
// Author : 浜田琉雅
// 概要 : メッシュなどの設定
//=============================================================================
class CModelX
{
public:
	static const int MAX_TEXTUREMAX = 16;
	// デフォルトコンストラクタ
	CModelX()
	{
		for (int i = 0; i < MAX_TEXTUREMAX; i++)
		{
			m_texture[i] = nullptr;
		}
	}		

	~CModelX() {}	// デストラクタ

	void Release()
	{
		for (int i = 0; i < (int)m_numMat; i++)
		{
			if (m_texture[i] != nullptr)
			{// テクスチャの解放
				m_texture[i]->Release();
				m_texture[i] = nullptr;
			}
		}
		// メッシュの破棄
		if (m_mesh != nullptr)
		{
			m_mesh->Release();
			m_mesh = nullptr;
		}
		// マテリアルの破棄
		if (m_buffMat != nullptr)
		{
			m_buffMat->Release();
			m_buffMat = nullptr;
		}
	
	}

	D3DXVECTOR3 GetVtxMax() { return m_vtxMaxModel; }
	D3DXVECTOR3 GetVtxMin() { return m_vtxMinModel; }
	
	// メッシュ(頂点の集まり)情報へのポインタ
	LPD3DXMESH m_mesh;
	// マテリアル情報へのポインタ	←　1のXファイルに複数のマテリアルが入っている
	LPD3DXBUFFER m_buffMat;
	// マテリアル情報の数
	DWORD m_numMat;
	// 最小値
	D3DXVECTOR3 m_vtxMinModel;
	// 最大値
	D3DXVECTOR3 m_vtxMaxModel;
	LPDIRECT3DTEXTURE9 m_texture[MAX_TEXTUREMAX];
	
	int m_nType;
	char  m_pXFileName[256];

};




#endif	// _OBJECTX_H_