//==================================================
// timeup.cpp
// Author: tutida ryousei
//==================================================
#include"game_end.h"
#include"manager.h"
#include"fade.h"
#include"debug_proc.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CGameEnd::CGameEnd(int nPriority) : CObject2D(nPriority)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CGameEnd::~CGameEnd()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CGameEnd::Init()
{
	CObject2D::Init();

	SetTexture(m_Texture);

	m_fAlpha = 1.0f;
	m_bEnd = true;

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CGameEnd::Uninit()
{
	CObject2D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CGameEnd::Update()
{
	CObject2D::Update();

	m_nDisplayTime--;

	if (m_nDisplayTime <= 0)
	{
		// 少しずつ透明にする
		m_fAlpha -= m_fAlphaSpeed;

		if (m_fAlpha <= 0)
		{// 消えたらリザルトへ
			CManager::GetFade()->NextMode(CManager::MODE_RESULT);
		}
	}

	// 色の設定
	SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, m_fAlpha));
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CGameEnd::Draw()
{
	CObject2D::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CGameEnd *CGameEnd::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size, int displaytime, float alphaspeed, CTexture::TEXTURE texture)
{
	CGameEnd *pGameEnd = nullptr;
	
	pGameEnd = new CGameEnd;
	
	if (pGameEnd != nullptr)
	{
		pGameEnd->SetPos(pos);
		pGameEnd->SetSize(size);
		pGameEnd->SetDisplayTime(displaytime);
		pGameEnd->SetAlphaSpeed(alphaspeed);
		pGameEnd->SetTex(texture);
		pGameEnd->Init();
	}
	
	return pGameEnd;
}