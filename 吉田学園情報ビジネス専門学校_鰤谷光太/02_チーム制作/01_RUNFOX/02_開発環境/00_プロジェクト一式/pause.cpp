//==================================================
// pause.cpp
// Author: tutida ryousei
//==================================================
#include"pause.h"
#include"input_keyboard.h"
#include"manager.h"
#include"fade.h"
#include"object2D.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CPause::CPause()
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CPause::~CPause()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
void CPause::Init()
{
	m_fAlpha = 1.0f;
	m_bPause = false;

	memset(m_pObject2D, 0, sizeof(m_pObject2D));

	// ポリゴンの設定
	m_pObject2D[PAUSE_BG] = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2, CManager::SCREEN_HEIGHT / 2, 0.0f),
		D3DXVECTOR3(600.0f, (float)CManager::SCREEN_HEIGHT, 0.0f),
		3);

	m_pObject2D[PAUSE_CONTINUE] = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2, 300.0f, 0.0f),
		D3DXVECTOR3(300.0f, 100.0f, 0.0f),
		3);

	m_pObject2D[PAUSE_TITLE] = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2, 450.0f, 0.0f),
		D3DXVECTOR3(500.0f, 100.0f, 0.0f),
		3);

	// テクスチャの設定
	m_pObject2D[PAUSE_CONTINUE]->SetTexture(CTexture::TEXTURE_PAUSE);
	m_pObject2D[PAUSE_TITLE]->SetTexture(CTexture::TEXTURE_PAUSE1);

	for (int nCnt = 0; nCnt < PAUSE_MAX; nCnt++)
	{
		m_pObject2D[nCnt]->SetCol({ 1.0f,1.0f,1.0f,0.0f });
		m_pObject2D[nCnt]->SetType(CObject::TYPE_MODE);
	}
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CPause::Update()
{
	// インプット
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();

	CManager::MODE mode = CManager::GetGameMode();			// モードの取得
	CFade::FADE *pFade = CManager::GetFade()->GetFade();	// フェード状態の取得

	if (pInputKeyoard->GetTrigger(DIK_P)
		&& mode == CManager::MODE_GAME
		&& *pFade == CFade::FADENON
		&& !m_bPause)
	{// ポーズする
		m_bPause = true;
		m_nSelect = PAUSE_CONTINUE;
	}
	
	// ポーズ中の処理
	In_Pause();
	// 選択
	Select();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CPause *CPause::Create()
{
	CPause *pPause = nullptr;

	pPause = new CPause;

	if (pPause != nullptr)
	{
		pPause->Init();
	}

	return pPause;
}

//--------------------------------------------------
// ポーズ中の処理
//--------------------------------------------------
void CPause::In_Pause()
{
	if (m_bPause)
	{// ポーズ中
		m_fAlpha = 0.2f;
		m_pObject2D[PAUSE_BG]->SetCol({ 1.0f,1.0f,1.0f,1.0f });

		// インプット
		CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();

		// W,Sで選択
		if (pInputKeyoard->GetTrigger(DIK_W) && m_nSelect > PAUSE_CONTINUE)
		{
			m_nSelect--;
		}
		else if (pInputKeyoard->GetTrigger(DIK_S) && m_nSelect < PAUSE_TITLE)
		{
			m_nSelect++;
		}
	}
	else
	{
		m_fAlpha = 0.0f;
		m_pObject2D[PAUSE_BG]->SetCol({ 1.0f,1.0f,1.0f,0.0f });
	}

	for (int nCnt = 0; nCnt < PAUSE_MAX; nCnt++)
	{
		if (nCnt != PAUSE_BG && m_pObject2D[nCnt] != nullptr)
		{// 色の設定
			m_pObject2D[nCnt]->SetCol({ 1.0f,1.0f,1.0f,m_fAlpha });
		}
	}
}

//--------------------------------------------------
// 選択処理
//--------------------------------------------------
void CPause::Select()
{
	// インプット
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();

	if (m_bPause)
	{
		switch (m_nSelect)
		{
		case PAUSE_CONTINUE:
			if (pInputKeyoard->GetTrigger(DIK_RETURN))
			{// ポーズ解除
				m_bPause = false;
			}
			break;

		case PAUSE_TITLE:
			if (pInputKeyoard->GetTrigger(DIK_RETURN))
			{// タイトルに戻る
				m_bPause = false;
				CManager::GetFade()->NextMode(CManager::MODE_TITLE);
			}
			break;

		default:
			break;
		}

		if (m_pObject2D[m_nSelect] != nullptr)
		{
			m_pObject2D[m_nSelect]->SetCol({ 1.0f,1.0f,1.0f,1.0f });
		}
	}
}