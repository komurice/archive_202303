//============================
//
// ２Dpolygon設定
// Author:hamada ryuuga
//
//============================

#include "stage.h"
#include "object.h"
#include "utility.h"

#include "manager.h"

#include <iostream>
#include <fstream>
#include <windows.h>
#include <nlohmann/json.hpp>
#include <string>

#include <stdio.h>
#include <tchar.h>
#include <locale.h>
#include <windows.h>
#include <sstream>

#include "building.h"
#include "mesh.h"
#include "skyfield.h"
#include "movemesh.h"
#include "meshroom.h"
#include "runmesh.h"
#include "player3D.h"

#include "input.h"
#include "objectX.h"


#include "ball.h"
#include "wood_spawn.h"
#include "pendulum.h"
#include "meshfield.h"

namespace nl = nlohmann;
CMesh * CStage::m_mesh[100];
nl::json JBuilding;//リストの生成
nl::json JlistMesh;//リストの生成
bool CStage::m_bCountdown = false;

//------------------------------------
// コンストラクタ
//------------------------------------
CStage::CStage()
{

}

//------------------------------------
// デストラクタ
//------------------------------------
CStage::~CStage()
{
}

//------------------------------------
// 初期化
//------------------------------------
HRESULT  CStage::Init()
{

	m_pPlayer = nullptr;

	for (int i = 0; i < 100; i++)
	{
		if (m_mesh[i] != nullptr)
		{
			m_mesh[i] = nullptr;
		}
		
	}


	m_numAllBuilding = 0;
	//ステージのゆか情報読み込み
	CStage::LoadfileMesh("data\\testmesh.json");

	if (CManager::GetGameMode() == CManager::MODE_GAME)
	{	//ゲームのオブジェクト情報読み込み
		CStage::Loadfile("data\\testBil.json");
	}

	//Playerの読み込み
	m_pPlayer = CPlayer::Create();
	m_pPlayer->SetMotion("data/SYSTEM/Gon/Fox.txt");

	//ゆかのポリゴン読み込み
	CObject3D* Data = CObject3D::Create({100000.0f,-0.0f,100000.0f});
	Data->SetPos({ 0.0f,-1000.0f,0.0f });
	Data->SetTexture(CTexture::TEXTURE_SKY);

	//スカイメッシュ読み込み
	CSkyField::Create();

	return S_OK;
}


//------------------------------------
//オブジェクトの生成
//------------------------------------
void CStage::SetObject()
{

	
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::Loadfile(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	int nIndex = 0;

	if (ifs)
	{
		ifs >> JBuilding;

		//設置したいオブジェクトの総数
		nIndex = JBuilding["INDEX"];
		m_numAllBuilding = JBuilding["INDEX"];

		D3DXVECTOR3 pos;
		D3DXVECTOR3 size;
		D3DXVECTOR3 rot;
		std::string Type;
		bool gold;
		bool Check;
		CBuilding* Buil = nullptr;
		int ModelType = 0;
		
		for (int nCntEnemy = 0; nCntEnemy < nIndex; nCntEnemy++)
		{
			//CBuilding* Build = nullptr;
			std::string name = "BUILDING";
			std::string Number = std::to_string(nCntEnemy);
			name += Number;

			if (JBuilding.count(name) == 0)
			{
				//int a = 0;
			}
			pos = D3DXVECTOR3(JBuilding[name]["POS"]["X"], JBuilding[name]["POS"]["Y"], JBuilding[name]["POS"]["Z"]);
			rot = D3DXVECTOR3(JBuilding[name]["ROT"]["X"], JBuilding[name]["ROT"]["Y"], JBuilding[name]["ROT"]["Z"]);
			Type = JBuilding[name]["TYPE"];
			ModelType = JBuilding[name]["MODELTYPE"];
			gold = JBuilding[name]["GOLD"];
			Check = JBuilding[name]["CHECK"];

			//設置モデルごとに個別設定
			switch ((CStage::MODELTYPE)ModelType)
			{
			case CStage::NORMAL:
			{
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			}
			case CStage::BALL:
			{
				float radius;
				float Rotspeed;
				int Rottype;
				D3DXVECTOR3 centerpos;
				name += "BALL";
				centerpos = D3DXVECTOR3(JBuilding[name]["CENTERPOS"]["X"], JBuilding[name]["CENTERPOS"]["Y"], JBuilding[name]["CENTERPOS"]["Z"]);
				radius = JBuilding[name]["RADIUS"];
				Rotspeed = JBuilding[name]["ROTSPEED"];
				Rottype = JBuilding[name]["ROTTYPE"];

				Buil = CBall::Create(centerpos, radius, Rotspeed, Rottype);

				break;
			}
			case CStage::PENDULUM:
			{
				D3DXVECTOR3 Ballrot;
				float Coefficient;
				int Movetype;

				std::string BallType;
				name += "PENDULUM";
				Ballrot = D3DXVECTOR3(JBuilding[name]["DESTROT"]["X"], JBuilding[name]["DESTROT"]["Y"], JBuilding[name]["DESTROT"]["Z"]);
				Coefficient = JBuilding[name]["COEFFICIENT"];
				Movetype = JBuilding[name]["MOVETYPE"];
				Buil = CPendulum::Create(pos, Ballrot, Coefficient, Movetype);

				break;
			}
			case CStage::WOOD:
			{
				D3DXVECTOR3 Woodpos;
				float Poprot;
				float PoprotSpeed;
				float PopMove;
				int Poptime;
				std::string WoodType;

				name += "WOOD";

				Woodpos = D3DXVECTOR3(JBuilding[name]["POPPOS"]["X"], JBuilding[name]["POPPOS"]["Y"], JBuilding[name]["POPPOS"]["Z"]);
				Poprot = JBuilding[name]["POPROT"];
				Poptime = JBuilding[name]["POPTIME"];
				PoprotSpeed = JBuilding[name]["ROTSPEED"];
				PopMove = JBuilding[name]["MOVE"];

				Buil = CWood_Spawn::Create(Woodpos, Poprot, PopMove, PoprotSpeed, Poptime);
				break;
			}
			case CStage::WOOL:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				break;
			case CStage::CHANGE:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetChangePoptime(Check);
				break;
			case CStage::GOAL:
				Buil = CBuilding::Create((char*)Type.c_str(), &pos);
				Buil->SetRot(rot);
				Buil->SetGold(gold);
				break;
			default:
				break;
			}

		
		
		}
	}
}

//------------------------------------
//今あるオブジェクトの読み込み
//------------------------------------
void CStage::LoadfileMesh(const char * pFileName)
{
	std::ifstream ifs(pFileName);

	if (ifs)
	{
		ifs >> JlistMesh;

		int nIndex = 0;

		//メッシュの総数取得
		nIndex = JlistMesh["INDEX"];

		int Type;

		for (int meshCount = 0; meshCount < nIndex; meshCount++)
		{

			std::string name = "Mesh";
			std::string Number = std::to_string(meshCount);
			name += Number;
			std::string filepass;

			filepass = JlistMesh[name]["FILEPASS"];
			Type = JlistMesh[name]["TYPE"];
			
			//タイプの読み込み
			switch (Type)
			{
			case MESH:
				m_mesh[meshCount] = CMesh::Create();
				break;
			case MESHMOVE:
				m_mesh[meshCount] = CMoveMesh::Create();
				break;
			case FLOORINGMOVE:
				m_mesh[meshCount] = CMeshRoom::Create();
				break;
			case RUNMESH:
				m_mesh[meshCount] = CRunMesh::Create();
				break;
			default:
				m_mesh[meshCount] = CMesh::Create();
				break;
			}

			m_mesh[meshCount]->Loadfile(filepass.c_str());
			m_mesh[meshCount]->SetType(Type);
			m_mesh[meshCount]->SetNumber(meshCount);


		}
	}
}
//------------------------------------
// 終了
//------------------------------------
void CStage::Uninit()
{
	CObjectXManager::ReleaseAll();
	for (int meshCount = 0; meshCount < 100; meshCount++)
	{
		if (m_mesh[meshCount] != nullptr)
		{
			m_mesh[meshCount]->Uninit();
			m_mesh[meshCount]->DeletedObj();
		}
		
	}
	CObject::DeletedObj();
}

//------------------------------------
// 更新
//------------------------------------
void CStage::Update()
{
}


//------------------------------------
// create
//------------------------------------
CStage *CStage::Create()
{
	CStage * pObject = new CStage;

	if (pObject != nullptr)
	{
		pObject->Init();
	}
	return pObject;
}

//------------------------------------
// 当たってるメッシュをとる関数
//------------------------------------
CMesh* CStage::GetPlayerHitMesh(int IsNumber)
{
	if (m_mesh[IsNumber] != nullptr)
	{
	
		D3DXVECTOR3 Size(m_mesh[IsNumber]->GetMeshSize());
		D3DXVECTOR3 oneboxSize(m_mesh[IsNumber]->GetOneMeshSize());
		if (((m_mesh[IsNumber]->GetPos()->z - Size.z) <= (m_pPlayer->GetPos().z)) &&
			((m_mesh[IsNumber]->GetPos()->z + oneboxSize.z )>= (m_pPlayer->GetPos().z)) &&
			((m_mesh[IsNumber]->GetPos()->x - oneboxSize.x) <= (m_pPlayer->GetPos().x)) &&
			((m_mesh[IsNumber]->GetPos()->x + Size.x) >= (m_pPlayer->GetPos().x)))
		{
			return	m_mesh[IsNumber];
		}
	}
	return	nullptr;
}


