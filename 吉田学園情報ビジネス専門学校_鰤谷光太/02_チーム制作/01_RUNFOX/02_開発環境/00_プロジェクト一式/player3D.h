//==================================================
// player3D.h
// Author: Buriya Kota
//==================================================
#ifndef _PLAYER3D_H_
#define _PLAYER3D_H_

//**************************************************
// インクルード
//**************************************************
#include "object.h"

//**************************************************
// 前方前言　実態はNG　ポインタだけならOK
//**************************************************
class CLockOnCursor;
class CShadow;
class CModel;

//**************************************************
// 定数定義
//**************************************************
#define MAX_PARTS		(4)
#define MAX_MOTION		(5)
#define MAX_BUILDING	(4096)

//**************************************************
// 構造体定義
//**************************************************

//**************************************************
// クラス
//**************************************************
class CPlayer3D : public CObject
{
public:
	struct KEY
	{
		D3DXVECTOR3 pos;
		D3DXVECTOR3 rot;
	};

	struct KEY_SET
	{
		int nFrame;
		KEY aKey[MAX_PARTS];
	};

	//modelデータの構造体//
	struct MODELDATA
	{
		int  LOOP;		// ループするかどうか[0:ループしない / 1 : ループする]
		int NUM_KEY;  	// キー数
		KEY_SET KeySet[MAX_MOTION];
	};

public:
	static const float PLAYER_SPEED;

public:
	CPlayer3D();
	~CPlayer3D();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	static CPlayer3D *Create(D3DXVECTOR3 pos);

	// セッター
	void SetPos(const D3DXVECTOR3& pos) { m_pos = pos; }
	void SetPosOld(const D3DXVECTOR3& posOld) { m_posOld = posOld; }
	void SetRot(const D3DXVECTOR3& rot) { m_rot = rot; }
	void SetRotDest(const D3DXVECTOR3& rotDest) { m_rotDest = rotDest; }
	void SetSize(const D3DXVECTOR3& size) { m_size = size; }
	void SetWorldMtx(const D3DXMATRIX& mtx) { m_mtx = mtx; }

	// ゲッター
	const D3DXVECTOR3& GetPos() const { return m_pos; }
	const D3DXVECTOR3& GetPosOld() const { return m_posOld; }
	const D3DXVECTOR3& GetRot() const { return m_rot; }
	const D3DXVECTOR3& GetRotDest() const { return m_rotDest; }
	const D3DXVECTOR3& GetSize() const { return m_size; }

	// 角度の正規化
	D3DXVECTOR3 RotNormalization(D3DXVECTOR3 rot);
	D3DXVECTOR3 RotDestNormalization(D3DXVECTOR3 rot, D3DXVECTOR3 rotDest);

private:
	void Control_();
	void Motion_();

private:
	// モデルの情報
	CModel *m_pModel[MAX_PARTS];
	D3DXMATRIX m_mtx;
	D3DXVECTOR3 m_pos;
	D3DXVECTOR3 m_posOld;
	D3DXVECTOR3 m_rot;
	D3DXVECTOR3 m_rotDest;
	D3DXVECTOR3 m_size;
	CShadow *m_pShadow;
	// キーの総数
	int m_nNumKey;
	// 現在のキー
	int m_nCurrentKey;
	// モーションカウンター
	int m_nCountMotion;
	// キーの構造体//


	MODELDATA m_ModelData[MAX_MOTION];

	int m_nSetModel;
	int m_nSetCurrentMotion;
	//=====================================
	// プロトタイプ宣言
	//=====================================
	// 制御関数
	void LoadSetFile(char *Filename);
	void LoadKeySetFile(FILE *pFile);

};

#endif	// _PLAYER3D_H_