//**************************************************
//
// 制作 ( タイトル )
// Author : hamada ryuuga
// Author : buriya kota
//
//**************************************************
#include "title.h"
#include "input.h"
#include "manager.h"
#include "object2d.h"
#include "object3d.h"
#include "sound.h"
#include "input.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "object.h"
#include "manager.h"
#include "fade.h"
#include "text.h"
#include "stage.h"
#include "manager.h"
#include "camera.h"
#include "debug_proc.h"

//**************************************************
// 定数定義
//**************************************************
#define LETTER_SPACING		(120.0f)
#define LOGO_HIGHT			(130.0f)
#define LOGO_WIDTH			(2000.0f)

//========================
// コンストラクター
//========================
CTitle::CTitle(int nPriority /* =1 */) : CObject(nPriority)
{
}
//========================
// デストラクト
//========================
CTitle::~CTitle()
{
}

//================
//初期化処理
//================
HRESULT CTitle::Init(void)
{
	m_NextMode = 0;
	m_nTime = 0;
	m_nNum = 0;

	//SetBG();
	CStage::Create();

	SetLogo();

	SetSelectBar();

	SetFrame();


	CManager::GetCamera()->Init();

	CManager::GetSound()->Play(CSound::SOUND_BGM_TITLE);
	
	return S_OK;
}

//================
//破棄
//================
void CTitle::Uninit(void)
{	
	CManager::GetSound()->Stop(CSound::SOUND_BGM_TITLE);

	DeletedObj();
}

//==================
//更新処理
//==================
void CTitle::Update(void)
{
	CSound *pSound = CManager::GetSound();
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	if (pInputKeyoard->GetTrigger(DIK_RETURN) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_A, 0) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_B, 0))
	{
		pSound->Play(CSound::SOUND_SE_DECISION);
		switch (m_NextMode)
		{
		case MODE::MODE_GAME:
			//モードの設定
			CManager::GetFade()->NextMode(CManager::MODE_GAME);
			break;
		case MODE::MODE_TUTORIAL:
			//モードの設定
			CManager::GetFade()->NextMode(CManager::MODE_TUTORIAL);
			break;
		case MODE::MODE_NAMESET:
			//モードの設定
			CManager::GetFade()->NextMode(CManager::MODE_NAMESET);
			break;
		case MODE::MODE_END:
			//ゲームの終了
			PostQuitMessage(0);
			break;
		default:
			break;
		}
	}

	if (pInputKeyoard->GetTrigger(DIK_W) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_UP, 0))
	{
		pSound->Play(CSound::SOUND_SE_SELECT);

		//モード選択
		m_Logpobj2d[m_NextMode]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
		m_Logpobj2d[m_NextMode]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
		m_NextMode = (MODE)(m_NextMode - 1);


		if (m_NextMode < MODE::MODE_GAME)
		{
			m_NextMode = MODE::MODE_END;
		}


		m_Logpobj2d[m_NextMode]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_Logpobj2d[m_NextMode]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
	}
	if (pInputKeyoard->GetTrigger(DIK_S) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_DOWN, 0))
	{
		pSound->Play(CSound::SOUND_SE_SELECT);

		//モード選択
		m_Logpobj2d[m_NextMode]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
		m_Logpobj2d[m_NextMode]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));

		m_NextMode = (MODE)(m_NextMode + 1);

		if (m_NextMode >= MODE::MODE_MAX)
		{
			m_NextMode = MODE::MODE_GAME;
		}

		m_Logpobj2d[m_NextMode]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
		m_Logpobj2d[m_NextMode]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
	}

	D3DXVECTOR3 pos[MAX_LOGO];

	m_nTime++;

	// タイトルの動き
	for (int nCnt = 0; nCnt < MAX_LOGO; nCnt++)
	{
		pos[nCnt] = m_pTitleLogp[nCnt]->GetPos();
		pos[nCnt].x += -10.0f;

		if (pos[nCnt].x <= 100.0f + LETTER_SPACING * nCnt)
		{// 横にずれる処理
			pos[nCnt].x = 100.0f + LETTER_SPACING * nCnt;
		}

		m_pTitleLogp[nCnt]->SetPos(pos[nCnt]);
	}

	if (270 <= m_nTime)
	{
		for (int i = 0; i < MAX_LOGO; i++)
		{
			float sinCurve = sinf(((m_nTime + (i * 5)) * 0.01f) * (D3DX_PI * 2.0f)) * 15.0f;
			pos[i].y = sinCurve;
			pos[i].y += LOGO_HIGHT;
			m_pTitleLogp[i]->SetPos(pos[i]);
		}
	}
	
#ifdef _DEBUG
	// デバッグ表示
	CDebugProc::Print("フレーム数 : %d\n", m_nTime);
#endif // DEBUG

}

//==================
//描画処理
//==================
void CTitle::Draw(void)
{
}

//==================
//クリエイト
//==================
CTitle*CTitle::Create()
{
	CTitle *pTitle = nullptr;

	pTitle = new CTitle;

	if (pTitle != nullptr)
	{
		pTitle->Init();
		pTitle->SetType(TYPE_MODE);
	}

	return pTitle;
}

//==================
//タイトルの背景配置
//==================
void CTitle::SetBG()
{
	CObject2D *pobj2d = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2,
		CManager::SCREEN_HEIGHT / 2,
		0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH,
		(float)CManager::SCREEN_HEIGHT,
			0.0f),
		PRIORITY_UI);

	pobj2d->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE);
}

//==================
//タイトルのロゴ配置
//==================
void CTitle::SetLogo()
{
	for (int nCnt = 0; nCnt < MAX_LOGO; nCnt++)
	{// FOXRUN
		m_pTitleLogp[nCnt] = CObject2D::Create(D3DXVECTOR3(LOGO_WIDTH + (LETTER_SPACING + 120.0f) * nCnt, LOGO_HIGHT, 0.0f), D3DXVECTOR3(140.0f, 140.0f, 0.0f), PRIORITY_UI);
	}

	// それぞれのテクスチャ
	m_pTitleLogp[0]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_F);
	m_pTitleLogp[1]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_O);
	m_pTitleLogp[2]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_X);
	m_pTitleLogp[3]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_R);
	m_pTitleLogp[4]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_U);
	m_pTitleLogp[5]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_N);
	m_pTitleLogp[6]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLEFOX);
}

//==================
//タイトルのバー配置
//==================
void CTitle::SetSelectBar()
{
	for (int i = 0; i < 4; i++)
	{
		m_Logpobj2d[i] = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT - 250.0f + 50.0f * i, 0.0f), D3DXVECTOR3(500.0f, 100.0f, 0.0f), PRIORITY_UI);
		m_Logpobj2d[i]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 0.5f));
	}

	// 初期の大きさ
	m_Logpobj2d[0]->SetCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f));
	m_Logpobj2d[0]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
	m_Logpobj2d[1]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
	m_Logpobj2d[2]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));
	m_Logpobj2d[3]->SetSize(D3DXVECTOR3(350.0f, 130.0f, 0.0f));

	// それぞれンのテクスチャ
	m_Logpobj2d[0]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_GAME);
	m_Logpobj2d[1]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_TUTORIAL);
	m_Logpobj2d[2]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_RANKING);
	m_Logpobj2d[3]->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_EXIT);
}

//==================
//タイトルの枠配置
//==================
void CTitle::SetFrame()
{
	CObject2D *pobj2d = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH * 0.5f, CManager::SCREEN_HEIGHT - 170.0f, 0.0f), D3DXVECTOR3(700.0f, 230.0f, 0.0f), PRIORITY_UI);

	pobj2d->SetTexture(CTexture::TEXTURE::TEXTURE_TITLE_FRAME);
}
