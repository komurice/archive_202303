//==================================================
// number.cpp
// Author: tutida ryousei
//==================================================
#include"number.h"

//--------------------------------------------------
// コンストラクタ
//--------------------------------------------------
CNumber::CNumber() : CObject2D(2)
{
}

//--------------------------------------------------
// デストラクタ
//--------------------------------------------------
CNumber::~CNumber()
{
}

//--------------------------------------------------
// 初期化
//--------------------------------------------------
HRESULT CNumber::Init()
{
	CObject2D::Init();

	// テクスチャの設定
	const CTexture::TEXTURE Texture = CTexture::TEXTURE_NUMBER;
	SetTexture(Texture);

	return S_OK;
}

//--------------------------------------------------
// 終了
//--------------------------------------------------
void CNumber::Uninit()
{
	CObject2D::Uninit();
}

//--------------------------------------------------
// 更新
//--------------------------------------------------
void CNumber::Update()
{
	CObject2D::Update();
}

//--------------------------------------------------
// 描画
//--------------------------------------------------
void CNumber::Draw()
{
	CObject2D::Draw();
}

//--------------------------------------------------
// 生成
//--------------------------------------------------
CNumber *CNumber::Create(D3DXVECTOR3 pos, D3DXVECTOR3 size)
{
	CNumber *pNumber = nullptr;

	pNumber = new CNumber;

	if (pNumber != nullptr)
	{
		pNumber->Init();
		pNumber->SetPos(pos);
		pNumber->SetSize(size);
	}

	return pNumber;
}

//--------------------------------------------------
// テクスチャを変える
//--------------------------------------------------
void CNumber::SetNumTexture(CTexture::TEXTURE pTexture)
{
	SetTexture(pTexture);
}