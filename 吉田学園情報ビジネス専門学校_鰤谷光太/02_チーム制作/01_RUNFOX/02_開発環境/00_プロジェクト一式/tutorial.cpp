//--------------------------------------------------------------------------
//
// 制作 ( チュートリアル )
// Author::TAKANO MINTO();
//
//--------------------------------------------------------------------------

//-----------------------------
//インクルードファイル
//-----------------------------
#include "tutorial.h"
#include "input.h"
#include "manager.h"
#include "object2d.h"
#include "sound.h"
#include "input.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "object.h"
#include "manager.h"
#include "fade.h"
#include "text.h"

//-----------------------------
//コンストラクタ
//-----------------------------
CTutorial::CTutorial(int nPriority /* =1 */) : CObject(nPriority)
{	
	//スイッチ制御変数のクリア
	m_nSwitch = 0;
}

//-----------------------------
//デストラクタ
//-----------------------------
CTutorial::~CTutorial()
{
}

//-----------------------------
//初期化
//-----------------------------
HRESULT CTutorial::Init()
{
	//スイッチ制御変数の初期化
	m_nSwitch = -1;

	//ポリゴンの配置
	SetPoligon();

	//ロゴの配置
	SetLogo();

	//サウンドの再生
	CManager::GetSound()->Play(CSound::SOUND_BGM_TUTORIAL);

	return S_OK;
}

//-----------------------------
//終了
//-----------------------------
void CTutorial::Uninit()
{
	//サウンドの停止
	CManager::GetSound()->Stop(CSound::SOUND_BGM_TUTORIAL);

	//生成されたオブジェクトの破棄
	DeletedObj();
}

//-----------------------------
//更新
//-----------------------------
void CTutorial::Update()
{
	//チュートリアルの画像変更
	SwitchTutorial();

	//インプットのポインタに値を代入
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	if (CManager::GetInputKeyboard()->GetTrigger(DIK_RETURN) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_A, 0) )
	{//指定されたキーが押されたら
		CManager::GetSound()->Play(CSound::SOUND_SE_CANCEL);	//SEの再生
		CManager::GetFade()->NextMode(CManager::MODE_TITLE);	//モードの設定
	}
}

//-----------------------------
//描画
//-----------------------------
void CTutorial::Draw()
{

}

//-----------------------------
//生成
//-----------------------------
CTutorial*CTutorial::Create()
{
	CTutorial *pTitle = nullptr;		//ポインタにヌルを代入

	pTitle = new CTutorial;				//チュートリアルの動的確保

	if (pTitle != nullptr)
	{//ポインタに値が入っていなかったら
		pTitle->Init();					//初期化
		pTitle->SetType(TYPE_MODE);		//モード設定
	}

	return pTitle;						//返り値
}

//-----------------------------
//ポリゴンの生成
//-----------------------------
void CTutorial::SetPoligon()
{
	//背景の生成
	m_pObj2D = CObject2D::Create(D3DXVECTOR3(CManager::SCREEN_WIDTH / 2,
		CManager::SCREEN_HEIGHT / 2,
		0.0f),
		D3DXVECTOR3((float)CManager::SCREEN_WIDTH,
		(float)CManager::SCREEN_HEIGHT,
			0.0f),
		0);

	//テクスチャの設定
	m_pObj2D->SetTexture(CTexture::TEXTURE_TUTORIAL_LAND);
}

//-----------------------------
//ロゴ配置
//-----------------------------
void CTutorial::SetLogo()
{
	CObject2D *pobj2d[2];			//2Dオブジェクトのポインタを宣言

	//オブジェクト生成
	pobj2d[0] = CObject2D::Create(D3DXVECTOR3(240.0f, 80.0f, 0.0f), D3DXVECTOR3(500.0f, 200.0f, 0.0f), 0);

	//テクスチャを設定
	pobj2d[0]->SetTexture(CTexture::TEXTURE::TEXTURE_TUTORIAL_LOGO);

	//2つ目のオブジェクト生成
	pobj2d[1] = CObject2D::Create(D3DXVECTOR3(1100.0f, 640.0f, 0.0f), D3DXVECTOR3(300.0f, 100.0f, 0.0f), 0);

	//2つ目のオブジェクトにテクスチャを設定
	pobj2d[1]->SetTexture(CTexture::TEXTURE::TEXTURE_TUTORIAL_RETURN);
}

//-----------------------------
//チュートリアル画面の変更
//-----------------------------
void CTutorial::SwitchTutorial()
{
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();
	if (CManager::GetInputKeyboard()->GetTrigger(DIK_SPACE)||pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_B, 0))
	{//スペースが押されたら
		CManager::GetSound()->Play(CSound::SOUND_SE_SELECT);		//SEを再生

		//スイッチ制御変数に値を入れる
		m_nSwitch = (m_nSwitch + 1 )% 3;

		if (m_nSwitch >= 3)
		{//スイッチ制御変数が3以上だったら
			m_nSwitch = 0;			//スイッチ制御変数を0にする
		}

		switch (m_nSwitch)
		{
		case 0:
			//テクスチャを変更
			m_pObj2D->SetTexture(CTexture::TEXTURE_TUTORIAL_LANDMk2);

			break;

		case 1:
			//テクスチャを変更
			m_pObj2D->SetTexture(CTexture::TEXTURE_TUTORIAL_LANDMk3);

			break;

		case 2:
			//テクスチャを変更
			m_pObj2D->SetTexture(CTexture::TEXTURE_TUTORIAL_LANDMk4);

			break;

		default:
			break;
		}
	}
}
