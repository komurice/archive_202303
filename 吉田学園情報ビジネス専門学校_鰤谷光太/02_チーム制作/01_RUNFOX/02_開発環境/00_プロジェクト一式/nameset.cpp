//**************************************************
//
// 制作 ( 名まえ )
// Author : hamada ryuuga
//
//**************************************************
#include "nameset.h"
#include "input.h"
#include "manager.h"
#include "object2d.h"
#include "fade.h"
#include "playfab.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "result.h"
#include "utility.h"
#include "sound.h"

std::string  CNameSet::m_PlayName;

//==================================
// コンストラクター
//==================================
CNameSet::CNameSet(int nPriority):CObject(nPriority)
{

	if (m_object2d[0] != nullptr)
	{
		m_object2d[0] = nullptr;
	}

	for (int i = 0; i < 3; i++)
	{
		if (m_ListName[i] != nullptr)
		{
			m_ListName[i] = nullptr;
		}
	}
	for (int i = 0; i < 10; i++)
	{
		if (m_PlayNameSet[i] != nullptr)
		{
			m_PlayNameSet[i] = nullptr;
		}
	}
}
//==================================
// デストラクタ
//==================================
CNameSet::~CNameSet()
{
}

//================
//初期化処理
//================
HRESULT CNameSet::Init(void)
{
	//ポリゴンの生成
	CObject2D *pObject2D = CObject2D::Create({ 1280 / 2,720 / 2,0.0f }, { 1280.0f,720.0f,0.0f }, 1);
	
	//テクスチャの設定
	pObject2D->SetTexture(CTexture::TEXTURE_NAMESET_BG);

	m_PlayName = "";
	m_NowPlay = 0;
	m_NemePos = D3DXVECTOR3(640.0f-300.0f, 100.0f, 0.0f);

	m_ListName[0] = CName::Create();
	m_ListName[0]->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
	m_ListName[0]->SetPos(D3DXVECTOR3(640.0f -125.0f, 375.0f, 0.0f));
	m_ListName[0]->SetAlphabet(CName::MAX);

	m_ListName[1] = CName::Create();
	m_ListName[1]->SetSize(D3DXVECTOR3(150.0f, 150.0f, 0.0f));
	m_ListName[1]->SetPos(D3DXVECTOR3(640.0f, 350.0f, 0.0f));

	m_ListName[2] = CName::Create();
	m_ListName[2]->SetSize(D3DXVECTOR3(100.0f, 100.0f, 0.0f));
	m_ListName[2]->SetPos(D3DXVECTOR3(640.0f + 125.0f, 375.0f, 0.0f));
	m_ListName[2]->SetAlphabet(CName::B);

	//サウンドの再生
	CManager::GetSound()->Play(CSound::SOUND_BGM_RANKING);

	return S_OK;
}

//================
//破棄
//================
void CNameSet::Uninit(void)
{
	//サウンドの停止
	CManager::GetSound()->Stop(CSound::SOUND_BGM_RANKING);

	Release();
}

//============================
//更新処理
//============================
void CNameSet::Update(void)
{
	//サウンドの情報取得
	CSound *pSound = CManager::GetSound();
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();
	if (pInputKeyoard->GetTrigger(DIK_BACKSPACE)||pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_B, 0))
	{
		//SEの再生
		pSound->Play(CSound::SOUND_SE_CANCEL);
		if (m_NowPlay > 0)
		{
			if (m_PlayNameSet[m_NowPlay-1] != nullptr)
			{
				m_NowPlay--;
				m_PlayNameSet[m_NowPlay]->Uninit();
				m_PlayName = m_PlayName.substr(0, m_PlayName.length() - 1);
				m_NemePos.x -= 120.0f;
			}
		}
	}

	if (pInputKeyoard->GetTrigger(DIK_SPACE) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_A, 0))
	{
		//SEの再生
		pSound->Play(CSound::SOUND_SE_DECISION);
		if (m_NowPlay >= 7)
		{
			//モードの設定
			CPlayfab::SetScore(m_PlayName, CResult::GetMyScore());

			CManager::GetFade()->NextMode(CManager::MODE_TITLE);
			return;
		}

		CNameSet::RankingNeme();
	}	
}
//============================
//描画処理
//============================
void CNameSet::Draw(void)
{
}

//==================================
//rankingに名まえをねじ込む
//==================================
void CNameSet::RankingNeme()
{
	CName::ALPHABET Type = m_ListName[1]->GetAlphabet();

	if (Type >= CName::A && Type <= CName::Z)
	{
		m_PlayName += ('A' + (char)Type);
	}
	else if (Type == CName::MAX)
	{
		//モードの設定
		if (m_PlayName != "")
		{
			CManager::GetFade()->NextMode(CManager::MODE_TITLE);
			return;
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}

	m_PlayNameSet[m_NowPlay] = CName::Create();
	m_PlayNameSet[m_NowPlay]->SetSize(D3DXVECTOR3(60.0f, 60.0f, 0.0f));
	m_PlayNameSet[m_NowPlay]->SetPos(m_NemePos);
	m_PlayNameSet[m_NowPlay]->SetAlphabet(Type);
	m_PlayNameSet[m_NowPlay]->SetMoveSwitch(false);

	m_NemePos.x += 120.0f;
	m_NowPlay++;
}


//==================
//クリエイト
//==================
CNameSet*CNameSet::Create()
{
	CNameSet *pResult = nullptr;

	pResult = new CNameSet;

	if (pResult != nullptr)
	{
		pResult->Init();
		pResult->SetType(TYPE_MODE);
	}

	return pResult;
}

//==================================
//名まえのゲット
//==================================
std::string  CNameSet::GetName()
{ 
	if (m_PlayName == "")
	{
		m_PlayName = "guest";
	}
	return m_PlayName; 
}