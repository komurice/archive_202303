//==================================================
// number.h
// Author: tutida ryousei
//==================================================
#ifndef _NUMBER_H_
#define	_NUMBER_H_

#include"object2D.h"

class CNumber : public CObject2D
{
public:
	CNumber();
	~CNumber();

	HRESULT Init() override;
	void Uninit() override;
	void Update() override;
	void Draw()  override;

	static CNumber *Create(D3DXVECTOR3 pos, D3DXVECTOR3 size);

	// セッター
	void SetNumCol(D3DXCOLOR *col) { SetCol(*col); }
	void SetNumSize(D3DXVECTOR3 size) { SetSize(size); }
	void SetNumTexture(CTexture::TEXTURE pTexture);

private:
	CTexture::TEXTURE *m_pTexture;		// テクスチャの情報
};

#endif // !_NUMBER_H_
