//============================
//
// プレイヤー設定
// Author:hamada ryuuga
//
//============================

#include <stdio.h>
#include <assert.h>
#include "player.h"
#include "input.h"
#include "camera.h"
#include "motion.h"
#include "manager.h"
#include "motion.h"

#include "utility.h"
#include "game.h"
#include "title.h"
#include "mesh.h"
#include "building.h"
#include "stage.h"
#include "ball.h"
#include "wood_spawn.h"
#include "input_keyboard.h"
#include "input_joypad.h"
#include "motion.h"
#include "sound.h"
#include "countdown.h"
#include "timer.h"

//=============================================================================
// static変数
//=============================================================================
const float CPlayer::ATTENUATION = 0.5f;	// 
const float CPlayer::SPEED = 1.0f;			// 移動量
const float CPlayer::WIDTH = 10.0f;			// モデルの半径
const float CPlayer::MAX_HITMOVESPEED = 50.0f;
const int CPlayer::MAX_PRAYER = 16;			// 最大数
const int CPlayer::MAX_MOVE = 9;			// アニメーションの最大数
const int CPlayer::INVINCIBLE = 30;		// 無敵時間
//=============================================================================
// コンストラクタ
//=============================================================================
CPlayer::CPlayer(int nPriority) : CMotionModel3D(nPriority)
{
	//m_nCounter = 0;				//SEカウントメンバ変数のクリア
}

//=============================================================================
// デストラクタ
//=============================================================================
CPlayer::~CPlayer()
{

}

//=============================================================================
// 初期化
//=============================================================================
HRESULT CPlayer::Init()
{
	m_Action = TYPE_NEUTRAL;
	m_MaxSpeed = 15.0f;
	m_move = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotDest = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_size = D3DXVECTOR3(300.0f, 100.0f, 300.0f);
	m_pos = D3DXVECTOR3(360.0f, 0.0f, -120.0f);
	//m_nCounter = 0;		//カウンターの初期化
	m_Pow = 0;
	m_Timer = 0;
	m_Speed = 1;
	D3DXVECTOR3	Size(2.0f, 2.0f, 2.0f);
	m_MoveSpeed = 7.0f;
	SetPos(D3DXVECTOR3(360.0f, 0.0f, -120.0f));
	SetChangePoptime(D3DXVECTOR3(360.0f, 0.0f, -120.0f));
	m_jump = false;
	SetStencil(true);
	m_JunpSpd = 1.0f;
	return S_OK;
}

//=============================================================================
// 終了
//=============================================================================
void CPlayer::Uninit()
{
	//CModel3D::UninitModel();										// モデルの終了
	// 現在のモーション番号の保管
	CMotionModel3D::Uninit();
}

//=============================================================================
// 更新
//=============================================================================
void CPlayer::Update()
{
	bool bCountdown = false;
	bool bTimeUp = false;

	CManager::MODE mode = CManager::GetGameMode();

	if (mode == CManager::MODE_GAME)
	{
		bCountdown = CStage::GetCountdown();
		bTimeUp = CGame::GetTimer()->GetTimeUp();
	}

	if (bCountdown && !bTimeUp)
	{
		m_posOld = GetPos();
		// モーション情報の取得
		CMotion *pMotion = CMotionModel3D::GetMotion();

		SetMaxMove(15.0f);

		// ニュートラルモーションの設定
		if (pMotion != nullptr
			&& !pMotion->GetMotion())
		{
			m_Action = TYPE_NEUTRAL;
			pMotion->SetNumMotion(m_Action);
		}
		
		if (CManager::GetGameMode() == CManager::MODE_GAME)
		{
			Move();	//動きセット	
		}

		// 現在のモーション番号の保管
		CMotionModel3D::Update();
	}
}

//=============================================================================
// 描画
//=============================================================================
void CPlayer::Draw()
{
	CMotionModel3D::Draw();
}

//=============================================================================
// create
//=============================================================================
CPlayer *CPlayer::Create()
{
	CPlayer * pObject = nullptr;
	pObject = new CPlayer;

	if (pObject != nullptr)
	{
		pObject->Init();
	}

	return pObject;
}

//=============================================================================
// Move
//=============================================================================
void CPlayer::Move()	//動きセット
{
	//サウンド
	CSound *pSound = CManager::GetSound();

	// インプット
	CInputKeyboard *pInputKeyoard = CManager::GetInputKeyboard();
	CInputJoyPad *pInputJoyPad = CManager::GetInputJoyPad();

	// 移動用の箱の用意
	D3DXVECTOR3 vec(0.0f, 0.0f, 0.0f);

	// ジョイパッドの入力
	vec.x = pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).x;
	vec.z = -pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).y;

	// 角度の取得
	m_rotDest = GetRot();
	if (pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).x != 0.0f ||
		pInputJoyPad->GetJoypadStick(pInputJoyPad->JOYKEY_LEFT_STICK, 0).y != 0.0f)
	{
		m_Timer++;
		if (m_Timer >= 5)
		{
			m_Timer = 0;
			m_Speed++;
		}
		if (m_Speed >= 5)
		{
			m_Speed = 5;
		}
	}
	
	if (((vec.x == 0.0f) && (vec.z == 0.0f))&& !m_jump)
	{// 移動
		if (pInputKeyoard->GetPress(DIK_A)|| 
			pInputKeyoard->GetPress(DIK_D)|| 
			pInputKeyoard->GetPress(DIK_W)||
			pInputKeyoard->GetPress(DIK_S))
		{//移動の速度段々加速させるやつ
			m_Timer++;
			if (m_Timer >= 5)
			{
				m_Timer = 0;
				m_Speed++;
			}
			if (m_Speed >= 5)
			{
				m_Speed = 5;
			}
		}
		else
		{
			m_Speed = 1;
		}

		if (pInputKeyoard->GetPress(DIK_A))
		{// 左
			vec.x += -1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_D))
		{// 右
			vec.x += 1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_W))
		{// 上
			vec.z += 1.0f;
		}
		if (pInputKeyoard->GetPress(DIK_S))
		{// 下
			vec.z += -1.0f;
		}
		
	}
	if (pInputKeyoard->GetPress(DIK_LSHIFT) || pInputJoyPad->GetJoypadPress(pInputJoyPad->JOYKEY_B, 0))
	{//ダッシュ
		m_Speed = 14;
	}
	
	if (pInputJoyPad->Stick(pInputJoyPad->JOYKEY_LEFT_STICK, 0, 0.5f) && !m_jump)
	{// プレイヤーが移動した方向に向きを合わせる
		m_rotDest.y = pInputJoyPad->GetStickAngle(pInputJoyPad->JOYKEY_LEFT_STICK, 0);
		m_rotDest.y += D3DX_PI;
	}

	D3DXVECTOR3 rot = GetRot();

	// 目的の角度の正規化
	NormalizeAngleDest(&rot.y, &m_rotDest.y);

	// 目的の角度を現在の角度に近づける
	rot.y += (m_rotDest.y - rot.y) * 0.2f;

	// 現在の角度の正規化
	NormalizeAngle(&rot.y);

	//D3DXVECTOR3 pos = GetPos();

	if ((vec.x != 0.0f || vec.z != 0.0f) &&!m_jump)
	{// 移動とそれに合わせた回転
	 // ベクトルの正規化
		D3DXVec3Normalize(&vec, &vec);
		// キーボード
		m_move += vec * (float)m_Speed;
	}

	SetRot(rot);

	float consumption = 0.0f;
	m_Friction = 0.0f;

	

	if (m_pos.y < -150.0f)
	{
 		m_move.y += -10.0f;
		m_jump = false;
 		m_Pow = 19;
		//m_nCounter = 0;
	}
	else
	{
		m_move.y += -1.0f;
	}
	
	if ((pInputKeyoard->GetTrigger(DIK_SPACE) || pInputJoyPad->GetJoypadTrigger(pInputJoyPad->JOYKEY_A, 0)) && !m_jump)
	{//SPACEキーが押された

		//ジャンプ量
		m_move.y += 30.0f;
		pSound->Play(CSound::SOUND_SE_JAMP);
		m_jump = true;
	}
		
	
	if (m_pos.y <= 0.0f&&m_move.y <= 0.0f)
	{
		m_jump = false;
		//m_Pow = 0;
		/*m_nCounter = 0;*/
	}
	else if(m_jump)
	{
		m_Friction = -0.49f;
	}
	
	for (int i = 0; i < 100; i++)
	{// メッシュとの当たり判定
		if (CGame::GetStage()->GetPlayerHitMesh(i) != nullptr)
		{
			CGame::GetStage()->GetPlayerHitMesh(i)->CollisionMesh(&m_pos,&m_move.y, m_jump);
		}
	}

	int Size = CGame::GetStage()->GetNumAllBuilding();

	m_move.x += (0.0f - m_move.x)*(ATTENUATION + m_Friction);//（目的の値-現在の値）＊減衰係数
	m_move.z += (0.0f - m_move.z)*(ATTENUATION + m_Friction);
	
	// 移動速度の上限
	if (m_move.x <= -m_MaxSpeed)
	{
		m_move.x = -m_MaxSpeed;
	}
	if (m_move.x >= m_MaxSpeed)
	{
		m_move.x = m_MaxSpeed;
	}
	if (m_move.z <= -m_MaxSpeed)
	{
		m_move.z = -m_MaxSpeed;
	}
	if (m_move.z >= m_MaxSpeed)
	{
		m_move.z = m_MaxSpeed;
	}

	// 移動を加算
	m_pos += m_move;
	
	if (m_pos.y <= -1060.0f / 2 + 20.0f)
	{// 落下の処理
		m_pos.y = (-760.0f / 2) + 20.0f;
		m_pos = ChangePoptime;
		pSound->Play(CSound::SOUND_SE_RESPAWN);
	}
	SetPos(m_pos);


	for (int i = 0; i < Size; i++)
	{// 当たり判定
		if (CObject::SelectModelObject(i, CObject::TYPE_BUILDING) != nullptr)
		{
			CBuilding* Obj = dynamic_cast<CBuilding*>(CObject::SelectModelObject(i, CObject::TYPE_BUILDING));  // ダウンキャスト 
			Obj->CollisionModel(&m_pos, &m_posOld, &m_size);
		}
	}

	// モーション
	if ((vec.x != 0.0f) || (vec.z != 0.0f))
	{
		CMotion *pMotion = CMotionModel3D::GetMotion();
		if (m_Action == TYPE_NEUTRAL)
		{
			if (pMotion != nullptr)
			{
				m_Action = TYPE_MOVE;
				pMotion->SetNumMotion(m_Action);
			}
		}
	}
	else
	{
		CMotion *pMotion = CMotionModel3D::GetMotion();
		if (m_Action != TYPE_NEUTRAL)
		{
			if (pMotion != nullptr)
			{
				m_Action = TYPE_NEUTRAL;
				pMotion->SetNumMotion(m_Action);
			}
		}
	}
}

//=============================================================================
// Move
//=============================================================================
void CPlayer::breakHit()
{

	m_pos.x += MAX_HITMOVESPEED * 100;
	SetPos(m_pos);
	CManager::GetSound()->Play(CSound::SOUND_SE_DAMAGE);
}
